function [] = mat2tifvol(IM,filename)
%IM is a matrix with image data
%filename must have a .tif extension
for i=1:size(IM,3)
    if i==1
        imwrite(IM(:,:,i),filename)
    else
        imwrite(IM(:,:,i),filename,'WriteMode','append')
    end
end
end