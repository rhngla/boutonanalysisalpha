%This script contains tricks to create an undistorted .png image from a
%matlab figure
figure,imshow(rand(1000,1000),[0 1])
set(gcf,'Position',[0 0 1000 1000],'Color',[0 0 0])
set(gca,'Position',[0 0 1 1])
set(gcf, 'PaperUnits', 'points');
set(gcf, 'PaperSize', [1000 1000]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 1000 1000]);
print(gcf, '-dpng', 'FigFile.png');