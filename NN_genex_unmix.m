function [IM,lbl,layers,options]=NN_genex_unmix(L,NL,an_L_ind,an_NL_ind)
%This function generates examples for the network to train on. Each example
%is generated without mixing animals.

k=50;
n_examples=10000;
nfeatures=1;
t_l=1:10;
t_nl=1:10;
IM=zeros(numel(t_l),k,1,n_examples);

ind83=find(an_L_ind==83);
ind85=find(an_L_ind==85);
ind101=find(an_L_ind==101);
ind102=find(an_NL_ind==102);
ind88=find(an_NL_ind==88);

pickL=[numel(ind83),numel(ind85),numel(ind101)];
pickL=cumsum(pickL./sum(pickL));

pickNL=[numel(ind102),numel(ind88)];
pickNL=cumsum(pickNL./sum(pickNL));

%Equal number of examples are chosen from learners and nonlearners
%Within learners, number of examples are hcosen proportional to amount of
%data per animal.

lbl=nan(n_examples,1);
for i=1:n_examples
    if mod(i,2)==0
        %Pick examples from learners
        diceroll=rand(1);
        if diceroll<=pickL(1)
            ind=ind83(randperm(numel(ind83),k));
        elseif diceroll>pickL(1) && diceroll<=pickL(2)
            ind=ind85(randperm(numel(ind85),k));
        elseif diceroll>pickL(2)
            ind=ind101(randperm(numel(ind101),k));
        end
        
        IM(:,:,1,i)=L(t_l,ind);
        lbl(i)=1;
    else
        %Pick examples from non-learners
        diceroll=rand(1);
        if diceroll<=pickNL(1)
            ind=ind102(randperm(numel(ind102),k));
        elseif diceroll>pickNL(1) && diceroll<=pickNL(2)
            ind=ind88(randperm(numel(ind88),k));
        end
        
        IM(:,:,1,i)=NL(t_nl,ind);
        lbl(i)=0;
    end
end
lbl=categorical(lbl);

layers = [
    imageInputLayer([size(IM,1) k 1],'Normalization','none')
    convolution2dLayer([size(IM,1),1],nfeatures,'Stride',1);
    clippedReluLayer(1)
    averagePooling2dLayer([1 k],'Stride',1)
    fullyConnectedLayer(2)
    softmaxLayer()
    classificationLayer];

%Weight initialization
layers(2).Weights=single(0.1*rand(size(IM,1),nfeatures));
layers(2).Bias=single(0.5*ones(nfeatures,1));

options = trainingOptions('sgdm',...
    'Momentum',0,...
    'MaxEpochs',200, ...
    'Verbose',true,...
    'VerboseFrequency',100,...
    'LearnRateSchedule','piecewise',...
    'LearnRateDropFactor',1,...
    'ExecutionEnvironment','auto',...
    'Plots','none',...
    'MiniBatchSize',1);
end