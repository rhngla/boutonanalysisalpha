function [] = nogui_loadfcn(animal,timepoint,section,axon)
%This function assembles trace and images to pass to the optimization
%function. The animal,timepoint,section,axon values point to exising
%directories and files.
%start_time=tic();

nogui_pathlist;
stackid=[animal,timepoint,section];
Gpth=[im_pth,stackid,'G.mat'];
Rpth=[im_pth,stackid,'R.mat'];
AMrpth=[man_pth,stackid,filesep,axon,'.swc'];

if exist(Gpth,'file') && exist(Rpth,'file') && exist(AMrpth,'file')
    %Load G and R images:
    temp=load(Gpth,'Original');
    Im.G=double(temp.Original);
    clear temp;
    temp=load(Rpth,'Original');
    Im.R=double(temp.Original);
    clear temp;
    
    %Load trace:
    [AM,r,~]=swc2AM(AMrpth);
    
    %Create info structure:
    inform.animal=animal;inform.section=section;inform.timepoint=timepoint;inform.axon=axon;
    
    %Optimize and make log entry
    exitcount=nogui_optimization(Im,AM,r,inform);
    msg=([stackid,'-',axon,' - count at exit: ',num2str(exitcount)]);
else
    msg=([stackid,'-',axon,' - File(s) not found']);
    
end
[~,sysname]=system('hostname');
%dt=num2str(toc(start_time));
dt=datestr(datetime('now'),'dd-mm-yy');
fid=fopen(isunixispc([err_pth,'run_optim.txt']),'a');
fprintf(fid,['\n',msg,' Time elapsed: ',dt,' seconds on ',sysname(1:end-1)]);
fclose(fid);
end