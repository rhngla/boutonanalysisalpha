%83,101: Learners with good imaging data
%88,102: Non-learners with good imaging data
%85    : Learner, 'red' animal. Noisy imaging data
%108   : Learner, ~10 timepoints. Image quality is poor for time points where performance improves 
%68, 89: Learners, 4 time points each. 

%Load the full dataset for 5 animals; Data also available for animals 108,68 and 89
A83=analysis_loaddat(83,0,1);
A101=analysis_loaddat(101,0,1);
A102=analysis_loaddat(102,0,1);
A88=analysis_loaddat(88,0,1);

A85=analysis_loaddat(85,0,1);
A108=analysis_loaddat(108,0,1);
A68=analysis_loaddat(68,0,1);
A89=analysis_loaddat(89,0,1);

%Convert data to matrices--------------------------------------------------
D83=analysis_getmat(A83);
D101=analysis_getmat(A101);
D102=analysis_getmat(A102);
D88=analysis_getmat(A88);

D85=analysis_getmat(A85);
D108=analysis_getmat(A108);
D68=analysis_getmat(A68);
D89=analysis_getmat(A89);

%Restrict to cleanest dataset - image/axon quality is evaluated in an excel
%file
d83=analysis_processdat(D83);
d101=analysis_processdat(D101);
d102=analysis_processdat(D102);
d88=analysis_processdat(D88);

d85=analysis_processdat(D85);
d108=analysis_processdat(D108);
d68=analysis_processdat(D68);
d89=analysis_processdat(D89);


%Select one of the below options as input for the analysis_maindataset
%function:
%analysis_type='Axon outliers';
analysis_type='Homeostasis';        %Bouton number/strength density plots
analysis_type='Distributions';      %Weight distributions
analysis_type='Statistics';         %Bouton addition/elimination/oscillations etc
analysis_type='Survival Fraction';  %Regular + size resolved survival curves
analysis_type='New Bouton Survival Fraction';  %Regular + size resolved survival curves
analysis_type='TemporalCorr';       %Shows anticorrelation of weight changes as lag of 4 days
analysis_type='SpatialCorr';        %Spatial correlation of weight or weight changes with distance+cumulative histograms+plotting highly correlated boutons on traces
analysis_type='Maps';              %Plots the maps shown in the thesis for addition/elimination etc
analysis_type='Transition matrix';  

%Perform analysis: --------------------------------------------------------
full_dataset={d83,d101,d102,d88};%Used for equalization within analysis_maindataset.m

Out83=analysis_maindataset(A83,d83,full_dataset,analysis_type);
Out101=analysis_maindataset(A101,d101,full_dataset,analysis_type);
Out102=analysis_maindataset(A102,d102,full_dataset,analysis_type);
Out88=analysis_maindataset(A88,d88,full_dataset,analysis_type);

%Animals with less data:
O85=analysis_maindataset(A85,d85,full_dataset,analysis_type);
O108=analysis_maindataset(A108,d108,full_dataset,analysis_type);
O68=analysis_maindataset(A68,d68,full_dataset,analysis_type);
O89=analysis_maindataset(A89,d89,full_dataset,analysis_type);

%To plot only learning data------------------------------------------------
L83=analysis_loadlearn(83,[],[]);
L101=analysis_loadlearn(101,[],[]);
L102=analysis_loadlearn(102,[],[]);
L88=analysis_loadlearn(88,[],[]);

L85=analysis_loadlearn(85,[],[]);
L108=analysis_loadlearn(108,[],[]);
L68=analysis_loadlearn(68,[],[]); 
L89=analysis_loadlearn(89,[],[]);

% savefig(gcf,[res_pth,'Added.fig'])
% print(gcf,'-dpng',[res_pth,'Added.png']);
