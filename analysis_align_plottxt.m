%This script saves figures for the 'Statistics' analysis in
%analysis_maindataset.m and also for NN_biostatistics;
hf=findobj(0,'Type','Figure');
for i=1:numel(hf)
    ha=findobj(hf(i),'Type','Axes');
    if contains(ha.YLabel.String,'Added Fraction') || contains(ha.YLabel.String,'Eliminated Fraction')
        ha.YLim=[0 0.25];
    elseif contains(ha.YLabel.String,'Turnover Fraction')
        ha.YLim=[0 0.5];
    elseif contains(ha.YLabel.String,'Depressed Fraction') || contains(ha.YLabel.String,'Potentiated Fraction')
        ha.YLim=[0 0.5];
    elseif contains(ha.YLabel.String,'Avg amount')
        ha.YLim=[0 2.6];
    elseif contains(ha.YLabel.String,'Avg added') || contains(ha.YLabel.String,'Avg eliminated')
        ha.YLim=[0 2.6];
    elseif contains(ha.YLabel.String,'Relative')
        ha.YLim=[0 0.08];    
    end
end

cc=lines(5);
cc(2,:)=[];
txtobj=findobj(0,'Type','Text');
for i=1:numel(txtobj)
    txtcol=txtobj(i).Color;
    ord=find(sum(bsxfun(@eq,txtcol,cc),2)==3);
    
    pos=txtobj(i).Position;
    hax=txtobj(i).Parent;
    txtobj(i).HorizontalAlignment='center';
    txtobj(i).Position(2)=ord*(0.08*hax.YLim(2));
end
set(findobj(0,'Type','Line'),'LineWidth',2,'Marker','s');
set(findobj(0,'Type','Axes'),'XLim',[-2 65]);
drawnow;

hf=findobj(0,'Type','Figure');
for i=1:numel(hf)
    ha=findobj(hf(i),'Type','Axes');
    pathlist;
    print(hf(i),'-dpng',[res_pth,'Motility-NN-',ha.YLabel.String,'.png']);
    savefig(hf(i),[res_pth,'Motility-NN-',ha.YLabel.String,'.fig'])
end

