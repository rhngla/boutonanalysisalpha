function C=catnanpad2(A,B,dimcat)
%2D matrices A and B will concatenated with nan padding.
if dimcat==1
    C=nan(size(A,1)+size(B,1),max(size(A,2),size(B,2)));
    C(1:size(A,1),1:size(A,2))=A;
    C((size(A,1)+1):end,1:size(B,2))=B;
elseif dimcat==2
    C=nan(max(size(A,1),size(B,1)),size(A,2)+size(B,2));
    C(1:size(A,1),1:size(A,2))=A;
    C(1:size(B,1),(size(A,2)+1):end)=B;
end
end