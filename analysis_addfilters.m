function [] = analysis_addfilters()
%This function was used to add median333 intensity profile to each of the
%individual profiles in the dataset. Function requires access to the
%imaging dataset.

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
maindir=isunixispc([parentdir,'/dat/Profiles_v3/']);
im_pth='F:\Rohan\DatasetIM\';

profiledir=dir(maindir);
profiledir=[{profiledir.name}'];

%Choose all data from a particular animal
keep=regexp(profiledir,'(DL089)*');
keep=find(~cellfun(@(x) isempty(x),keep));

for k=1:numel(keep)
   filelist=dir([maindir,filesep,profiledir{keep(k)}]);
   filelist=[{filelist.name}'];
   
   keepfile=regexp(filelist,'(A)');
   keepfile=find(~cellfun(@(x) isempty(x),keepfile));
   
   IM=load([im_pth,profiledir{keep(k)},'G.mat'],'Original');
   IM=double(IM.Original);
   for f=1:numel(keepfile)
       fname=[maindir,filesep,profiledir{keep(k)},filesep,filelist{keepfile(f)}];
       Profile=load(fname);
       
       [Profile.I.G.median333.raw, Profile.I.G.median333.caliber] = ...
           profilefilters(Profile.r.optim,IM,'median333',[]);
       Profile.I.G.median333.norm = ...
          Profile.I.G.median333.raw./mean(Profile.I.G.median333.raw(~Profile.annotate.ignore));
       
       save(fname,'-struct','Profile');
       display(['Saved profile: ',fname]);
       clear Profile;
   end
end
end