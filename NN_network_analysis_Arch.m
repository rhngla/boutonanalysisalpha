function [L_ind]=NN_network_analysis_Arch(anid)
%This function was used to networks trained on full dataset. Clipped reLU
%layer was used and the initial bias was set to 0.5.
set(0,'defaultAxesFontSize',18,'defaultAxesFontName','Calibri')

if ismac
    path_exp='/Users/Fruity/Dropbox/Lab/Plasticity/dat/NN/NN_Arch_unmix/';
    load('/Users/Fruity/Dropbox/Lab/Plasticity/BoutonAnalysis/dat/NN_Dat_v2.mat');
else
    path_exp='E:\Rohan\Dropbox\Lab\Plasticity\dat\NN\NN_Arch_2_best\';
    load('E:\Rohan\Dropbox\Lab\Plasticity\BoutonAnalysis\dat\NN_Dat_v2.mat')
end

%Load files
%fnames=dir([path_exp,num2str(anid),'-','*.mat']);
fnames=dir([path_exp,'Unmixed','-',num2str(anid),'-','*.mat']);
fnames={fnames.name}';
clear N
for f=1:numel(fnames)
    N{f}=load([path_exp,fnames{f}]);
end

figure,
finalloss=nan(numel(fnames),1);
finalacc=nan(numel(fnames),1);
cc=0.5*[rand(numel(fnames),3),0.5*ones(numel(fnames),1)]+0.25;
for f=1:numel(fnames)
    if numel(N{f}.traininfo.TrainingLoss)>1
        temp=movmean(N{f}.traininfo.TrainingLoss,[10,0]);
        temp_plot=temp(1:end);
        finalloss(f)=temp_plot(end);
        finalacc(f)=mean(N{f}.traininfo.TrainingAccuracy(end-100:end));
        plot(temp_plot,'-','Color',cc(f,:),'ButtonDownFcn',@analysis_explorelines,'UserData',f);hold on
    end
end
title('Loss');
axis square;box on;drawnow
%close(gcf);


w2=[];
for i=1:numel(N)
    %N{i}.net.Layers(5).Weights
    w2=[w2,N{i}.net.Layers(5).Weights(:)];
end

W=[];B=[];
figure;
[temp,sortind]=sort(finalloss);
temp=temp(end);
%ii=1;
for f=1:numel(fnames)
    if finalloss(f)<=temp
        w1=N{f}.net.Layers(2).Weights(:,1,1,1);
        b1=N{f}.net.Layers(2).Bias;
        W=[W,w1(:)];
        B=[B,b1];
        plot([w1(:)],'Color',cc(f,:),'ButtonDownFcn',@analysis_explorelines,'UserData',f);hold on
        
        %{
        %To save the best networks for more training
        newname=['/Users/Fruity/Dropbox/Lab/Plasticity/dat/NN/NN_Arch_2_best/','Best-',num2str(anid),'-',num2str(ii)];
        A=N{f};
        save(newname,'-struct','A');
        ii=ii+1;
        %}
    end
end
title('Weights');
grid on;drawnow;

%Show loss and weights at the conv layer in the chosen network
if anid==1
    chosen_net=131;
elseif anid==3
    chosen_net=114;
end
set(findobj(0,'UserData',chosen_net),'LineWidth',2,'Color',[1 0 0 1]);

%To plot cloud of points and the approximate decision boundary:
%chosen_net=sortind(round(numel(sortind)./2));
t_prjct=[2,3,4];
[L,~,~,NL] = NN_eqhist(O83,O85,O101,O102,O88,anid);

W=N{chosen_net}.net.Layers(2).Weights(:,1,1,1);
B=N{chosen_net}.net.Layers(2).Bias;

for i=1:numel(chosen_net)
    %Find activation right before thresholding to classify
    Dat=[L,NL];
    is_L=[true(1,size(L,2)),false(1,size(NL,2))];
    %lbl=false(size(Dat,2),1);
    lbl=categorical(nan(size(Dat,2),1));
    act_2=nan(size(Dat,2),1);
    act_3=nan(size(Dat,2),1);
    for j=1:size(Dat,2)
        this_bouton=repmat(Dat(:,j),1,50);
        lbl(j)=classify(N{chosen_net}.net,this_bouton);
        %lbl(j,:)=activations(N{chosen_net}.net,this_bouton,7);
        act_2(j)=unique(activations(N{chosen_net}.net,this_bouton,2)');
        act_3(j)=unique(activations(N{chosen_net}.net,this_bouton,3)');
        %lbl(j)=temp(1)<=0;
    end
    %inds=lbl(:,1)'>0.5;
    is_class_learner=lbl==categorical(1);%1 is Learner
    %is_outlier=true(size(is_outlier));%Pass all
    %is_outlier=lbl;
    disp(sum(is_class_learner));
    
    %{
    %Consider only the boutons in a minority:
    if sum(is_outlier)>sum(~is_outlier)
       is_outlier=~is_outlier; 
    end
    %}
    
    %Finding distances from the decision plane
    dist_measure=((W(:)'*Dat)+B)./(W(:)'*W(:)).^0.5;
    %is_far=dist_measure*(sign(mean(W)))>0;
    is_far=true(size(dist_measure));
    
    
    is_class_learner=is_class_learner(:)';
    is_L=is_L(:)';
    L_ind=find(is_L & is_class_learner & is_far);
    NL_ind=find(~is_L & is_class_learner & is_far);
    
    %L_ind=find(is_L & ~is_class_learner);
    %NL_ind=find(~is_L & ~is_class_learner);
    
    NL_ind=NL_ind-size(L,2);
    ind_102=NL_ind(NL_ind<=size(O102.w,2));
    ind_88=NL_ind(NL_ind>size(O102.w,2))-size(O102.w,2);
    
    %Show statistics using original weights
    %
    if anid==1
        NN_biostats(O83.w(:,L_ind),O83)
    elseif anid==2
        NN_biostats(O85.w(:,L_ind),O85)
    elseif anid==3
        NN_biostats(O101.w(:,L_ind),O101)
    end
    %NN_biostats(O102.w(:,ind_102),O102)
    %NN_biostats(O88.w(:,ind_88),O88)
    %}
    
    %{
    NN_biostats(L(:,L_ind),anid)
    NN_biostats(NL(:,ind_102),4)
    NN_biostats(NL(:,ind_88),5)
    %}

    fprintf('Outliers in Learners: %0.1d out of %0.1d\n',sum(is_L(:) & is_class_learner(:)),sum(is_L(:)))
    fprintf('Outliers in NonLearners: %0.1d out of %0.1d\n',sum(~is_L(:) & is_class_learner(:)),sum(~is_L(:)))
    
    %Draw decision plane
    %For a plane: Aa*x + Bb*y + Cc*z + Dd = 0
    Aa=W(t_prjct(1),i);
    Bb=W(t_prjct(2),i);
    Cc=W(t_prjct(3),i);
    Dd=B(i);
    figure,
    [x, y] = meshgrid(-2:1:40); % Generate x and y data
    z = -1/Cc*(Aa*x + Bb*y + Dd); % Solve for z vertices data
    h=surf(x,y,z);hold on %Plot the surface
    h.FaceColor=[0.2 0.2 0.2];h.EdgeColor='none';h.FaceAlpha=0.5;
    
    %{
    %Distance of points from decision boundary
    dists=(Aa.*Dat(t_prjct(1),is_outlier) + Bb.*Dat(t_prjct(2),is_outlier) + Cc.*Dat(t_prjct(3),is_outlier) + Dd)./...
        ((Aa.^2) + (Bb.^2) + (Cc.^2)).^0.5;
    %}
    
    %{
    %Plot full Learners and Non-learner clouds
    plot_prjct(3)(L(t_prjct(1),:),L(t_prjct(2),:),L(t_prjct(3),:),'.','Color',[0.8 0.3 0.3],'MarkerSize',15);hold on
    plot_prjct(3)(NL(t_prjct(1),:),NL(t_prjct(2),:),NL(t_prjct(3),:),'.','Color',[0.2 0.5 0.8],'MarkerSize',15);
    %}
    
    %Plot boutons colored by how they are classified
    ind1=is_class_learner & is_L;
    ind2=is_class_learner & ~is_L;
    ind3=~is_class_learner & is_L;
    ind4=~is_class_learner & ~is_L;
    
    h1=scatter3(Dat(t_prjct(1),ind1),Dat(t_prjct(2),ind1),Dat(t_prjct(3),ind1));hold on
    h2=scatter3(Dat(t_prjct(1),ind2),Dat(t_prjct(2),ind2),Dat(t_prjct(3),ind2));hold on
    
    h3=scatter3(Dat(t_prjct(1),ind3),Dat(t_prjct(2),ind3),Dat(t_prjct(3),ind3));hold on
    h4=scatter3(Dat(t_prjct(1),ind4),Dat(t_prjct(2),ind4),Dat(t_prjct(3),ind4));hold on
    
    h1.MarkerFaceColor=[0 0.3 0.8];
    h1.MarkerFaceAlpha=0.7;
    h1.MarkerEdgeColor='none';
    h1.SizeData=50;
    
    h2.MarkerFaceColor='none';
    h2.MarkerFaceAlpha=0.2;
    h2.MarkerEdgeColor=[0 0.3 0.8];
    h2.SizeData=50;
    
    h3.MarkerFaceColor=[0.8 0.3 0.3];
    h3.MarkerFaceAlpha=0.7;
    h3.MarkerEdgeColor='none';
    h3.SizeData=50;
    
    h4.MarkerFaceColor='none';
    h4.MarkerFaceAlpha=0.7;
    h4.MarkerEdgeColor=[0.8 0.3 0.3];
    h4.SizeData=50;

    xlabel(['Session ',num2str(t_prjct(1))])
    ylabel(['Session ',num2str(t_prjct(2))])
    zlabel(['Session ',num2str(t_prjct(3))])
    grid on;set(gca,'FontName','Calibri','FontSize',15)
    box on;axis equal;xlim([-2 40]);ylim([-2 40]);zlim([-2 40]);
    drawnow;
    
    legend([h1,h2,h3,h4],{'Learner','Non-learner','Learner','Non-learner'});
    drawnow;
end

%{
%--------------------------------------------------------------------------
%Select a network thta distinguishes based on outliers in Learners
%1. Pass individual boutons through network, and check output after the clipped ReLU
%2. Find the fraction of 0s and 1s for Learner and Non-Learner
%3. Consider the fraction that is less than 50%; these are defined as outliers
%4. Retain the networks which have more outliers in Learners than Non-Learners
[L,~,~,NL] = NN_eqhist(O83,O85,O101,O102,O88,anid);

L_1=nan(numel(fnames),1);
NL_1=nan(numel(fnames),1);
for f=1:numel(fnames)
    D=L;
    after_relu=nan(size(D,2),50);
    for i=1:size(D,2)
        this_bouton=repmat(D(:,i),1,50);
        after_relu(i,:)=activations(N{f}.net,this_bouton,3);
    end
    L_1(f)=sum(after_relu(:,1))./size(D,2);
    
    D=NL;
    after_relu=nan(size(D,2),50);
    for i=1:size(D,2)
        this_bouton=repmat(D(:,i),1,50);
        after_relu(i,:)=activations(N{f}.net,this_bouton,3);
    end
    NL_1(f)=sum(after_relu(:,1))./size(D,2);
end

keep=find((L_1<0.5 & L_1>NL_1) | (L_1>0.5 & L_1<NL_1));
W=[];B=[];
figure;
finalacc=nan(numel(N),1);
for k=1:numel(keep)
    f=keep(k);
    finalacc(f)=mean(N{f}.traininfo.TrainingAccuracy(end-10:end));
    if finalacc(f)>0
        finalacc(f)
        w1=N{f}.net.Layers(2).Weights(:,1,1,1);
        b1=N{f}.net.Layers(2).Bias;
        W=[W,w1(:)];
        B=[B,b1];
        plot(w1);hold on
    end
end
drawnow;axis square;box on;
%}
end
%}