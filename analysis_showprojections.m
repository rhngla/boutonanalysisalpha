function IM=analysis_showprojections(plane)
%This returns the xy yz or zy projections saved within profiles in a 3D
%stack, where the third dimension is different imaging sessions. 
anid=85;
tracer={[]};
section = {'008'};
axon = {[1]};
timepoint=cellstr(char(double('C'):double('T'))')';
pathlist;
animal=sprintf('DL%03d',anid);

Section=cell(numel(section),1);
for se=1:numel(section)
    for ti=1:numel(timepoint)
        for ax=1:numel(axon{se})
            for tr=1:numel(tracer)
                axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
                fname=isunixispc([profile_pth,animal,timepoint{ti},section{se},'\',axonstr,'.mat']);
                if exist(fname,'file')
                    Dat=load(fname,'proj');
                    %Section{se}.Time{ti}.Axon{ax}=struct(Dat);
                    if ti==1
                       IM=zeros(size(Dat.proj.G.(plane).full,1),size(Dat.proj.G.(plane).full,2),numel(timepoint)); 
                    end
                    IM(:,:,ti)=Dat.proj.G.(plane).full./(7*median(Dat.proj.G.(plane).full(:)));
                else
                    disp([animal,timepoint{ti},section{se},'-',axonstr,' not found!']);
                end
            end
        end
    end
end

end