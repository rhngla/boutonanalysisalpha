function [] = convformat()
%Script converts older data format to new format
oldpath='F:\Rohan\Dropbox\Lab\Plasticity\dat\Controls\Conditions\Profiles_v4\';
newpath='F:\Rohan\Dropbox\Lab\Plasticity\dat\Controls\Conditions\Profiles_v5\';
impth='F:\Rohan\DatasetIM\Control-1\';

animal='DL001';
section='002';
timeletter={'A','B','D','E','F','H','I'};
axon=[1,2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20];

for t=1:numel(timeletter)
    timepoint=timeletter{t};
    Im.G=load([impth,animal,timepoint,section,'G.mat'],'Original');Im.G=double(Im.G.Original);
    for ax=1:numel(axon)
        axonstr=sprintf('A%0.3d',axon(ax));
        Old=load([oldpath,animal,timepoint,section,filesep,axonstr,'.mat']);
        
        New.AM=Old.AM;
        New.r=Old.r;
        New.d=Old.d;
        New.d.aligned=Old.d.optim;
        New.I.G.LoGxy=Old.I.G.LoGxy;
        New.I.G.Gauss=Old.I.G.Gauss2;
        New.fit.G.LoGxy=Old.fit.G.LoGxy;
        New.fit.G.Gauss=Old.fit.G.Gauss2;
        New.annotate=Old.annotate;
        New=genproj(New,Old,Im);
        New.id=Old.id;
        save([newpath,animal,timepoint,section,filesep,axonstr,'.mat'],'-struct','New')
        clear Old New
    end
    clear Im;
end
end

function New=genproj(New,Old,Im)
channel={'G'};
sizeIm=size(Im.(channel{1}));%Size of all channels is the same.

pad=20;
minr=min(Old.r.optim,[],1);
maxr=max(Old.r.optim,[],1);
minx=round(max(minr(1)-pad,1));maxx=round(min(maxr(1)+pad,sizeIm(1)));
miny=round(max(minr(2)-pad,1));maxy=round(min(maxr(2)+pad,sizeIm(2)));
minz=round(max(minr(3)-pad,1));maxz=round(min(maxr(3)+pad,sizeIm(3)));

[~,SVr,~]=AdjustPPM(Old.AM.optim,Old.r.optim,zeros(size(Old.r.optim,1),1),1);

%Perform fast marching on restricted volume:
paramset;%Parameters for FastMarching
[KT]=FastMarchingTube([maxx-minx+1,maxy-miny+1,maxz-minz+1],[SVr(:,1)-minx+1,SVr(:,2)-miny+1,SVr(:,3)-minz+1],params.proj.fm_dist,[1 1 1]);
Filter=false(sizeIm);
Filter(minx:maxx,miny:maxy,minz:maxz)=KT;
Filter=double(Filter);

display('Updating projections...');
for ch=1:numel(fieldnames(Im))
    Im_ax=Filter.*Im.(channel{ch});
    New.proj.(channel{ch}).xy.ax=max(permute(Im_ax,[1,2,3]),[],3);
    New.proj.(channel{ch}).zy.ax=max(permute(Im_ax,[3,2,1]),[],3);
    New.proj.(channel{ch}).xz.ax=max(permute(Im_ax,[1,3,2]),[],3);
    
    New.proj.(channel{ch}).xy.full=max(permute(Im.(channel{ch}),[1,2,3]),[],3);
    New.proj.(channel{ch}).zy.full=max(permute(Im.(channel{ch}),[3,2,1]),[],3);
    New.proj.(channel{ch}).xz.full=max(permute(Im.(channel{ch}),[1,3,2]),[],3);
    display(['Completed for channel ', channel{ch}]);
end
display('Updating projections completed.');
end