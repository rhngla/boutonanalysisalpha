function [] = NN_biostats(w,Dat)
%This function calculates statistics like addition, removal, potentiation,
%depression etc. for boutons that are classified Learning boutons by the
%network.

%{
NN_biostats(O83.w,O83)
NN_biostats(O101.w,O101)
NN_biostats(O102.w,O102)
NN_biostats(O88.w,O88)
%}
anim=[83,85,101,102,88];
anid=find(anim==Dat.anid);
cc=lines(anid);
anc=cc(anid,:);
f=fP_handles;

%To threshold probabilities using significance level
sig_level=0; %siglevel~=0 uses thresholded values

if sig_level==0
    P=f.P(w);
    P_add=f.P_add(w(1:end-1,:),w(2:end,:));
    P_elim=f.P_elim(w(1:end-1,:),w(2:end,:));
    pot=f.P_pot(w(1:end-1,:),w(2:end,:));
    dep=f.P_dep(w(1:end-1,:),w(2:end,:));
    changed=f.P_change(w(1:end-1,:),w(2:end,:));
    
else
    P=f.P(w)>sig_level;
    P_add=f.P_add(w(1:end-1,:),w(2:end,:))>sig_level;
    P_elim=f.P_elim(w(1:end-1,:),w(2:end,:))>sig_level;
    pot=f.P_pot(w(1:end-1,:),w(2:end,:))>sig_level;
    dep=f.P_dep(w(1:end-1,:),w(2:end,:))>sig_level;
    changed=f.P_change(w(1:end-1,:),w(2:end,:))>sig_level;
end

%for weight calculations
S_mean=sum(w(1:end-1,:)+w(2:end,:),2);
S_change=abs(w(2:end,:)-w(1:end-1,:));

tt=analysis_aligntime(Dat,[],'start');
tt=(tt(1:end-1)+tt(2:end))./2;
tt_txt=cell(size(w,1)-1,1);
for i=1:numel(tt_txt)
    tt_txt{i}=[Dat.L.im_id{i},Dat.L.im_id{i+1}];
end

hfn=100;
hf=gobjects(20,1);
figind=1;
%Added number-----------
hf(figind)=figure(80+figind);movegui(hf(figind),'northwest');
figure(hf(figind));drawnow;
yy=sum(P_add,2)./sum(P(2:end,:),2);
%yy=sum(P_add,2)./mean(sum(P(1:end,:),2));
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Added Fraction');drawnow;hold on;
figind=figind+1;

%{
%Avg added weight
hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
S_add=S_change.*P_add;
yy=sum(S_add,2)./sum(P_add,2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Avg added weight');drawnow;hold on;
figind=figind+1;
%}

%Elimination------------
hf(figind)=figure(80+figind);movegui(hf(figind),'north');
yy=sum(P_elim,2)./sum(P(1:end-1,:),2);
%yy=sum(P_elim,2)./mean(sum(P(1:end,:),2));
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Eliminated Fraction');drawnow;hold on;
figind=figind+1;

%{
%Avg eliminated weight
hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
S_elim=S_change.*P_elim;
yy=sum(S_elim,2)./sum(P_elim,2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Avg eliminated weight');drawnow;hold on;
figind=figind+1;
%}

%Turnover---------------
hf(figind)=figure(80+figind);movegui(hf(figind),'northeast');
yy=(sum(P_add,2)+sum(P_elim,2))./((sum(P(1:end-1,:),2)+sum(P(2:end,:),2))./2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Turnover Fraction');drawnow;hold on;
figind=figind+1;


%Potentiation-------------
%{
%------------Number
hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
yy=sum(pot,2)./sum(P(1:end-1,:),2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Potentiated Fraction');drawnow;hold on;
figind=figind+1;
%}

%------------Relative amount
hf(figind)=figure(80+figind);movegui(hf(figind),'west');
S_pot=S_change.*pot;
yy=sum(S_pot./S_mean,2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Relative Potentiated Weight');drawnow;hold on;
figind=figind+1;

%------------Avg amount
hf(figind)=figure(80+figind);movegui(hf(figind),'west');
S_pot=S_change.*pot;
yy=sum(S_pot,2)./sum(pot,2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Avg weight change per potentiation');drawnow;hold on;
figind=figind+1;

%Depression-------------
%------------Number
%
hf(figind)=figure(80+figind);movegui(hf(figind),'south');
yy=sum(dep,2)./sum(P(1:end-1,:),2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Depressed Fraction');drawnow;hold on;
figind=figind+1;
%}

%------------Relative amount
hf(figind)=figure(80+figind);movegui(hf(figind),'east');
S_dep=S_change.*dep;
yy=sum(S_dep./S_mean,2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Relative Depressed Weight');drawnow;hold on;
figind=figind+1;

%------------Avg amount
hf(figind)=figure(80+figind);movegui(hf(figind),'east');
S_dep=S_change.*dep;

%TESTING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
S_deptemp=S_dep;
deptemp=dep;
nexamples=round(size(S_deptemp,2)*(5/100));
sorted=sort(abs(S_deptemp(:)));
thr=sorted(end-nexamples);
ind=any(abs(S_deptemp)>=thr,1);
S_deptemp(:,ind)=[];
deptemp(:,ind)=[];
%TESTING END!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

%yy=sum(S_dep,2)./sum(dep,2);
yy=sum(S_deptemp,2)./sum(deptemp,2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Avg weight change per depression');drawnow;hold on;
figind=figind+1;

%{
%Weight change fraction---
hf(figind)=figure(80+figind);movegui(hf(figind),'southeast');
yy=sum(changed,2)./sum(P(1:end-1,:),2);
plot(tt,yy,'o-','Color',anc);
text(tt,1.05*yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Changed Fraction');drawnow;hold on;
figind=figind+1;
%}

%Turnover equivalent for absolute weight change
hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
S_weight_tor=S_change.*(dep+pot);
yy=sum(S_weight_tor,2)./sum((dep+pot),2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Avg weight change');drawnow;hold on;
figind=figind+1;

%Turnover equivalent for relative weight change
hf(figind)=figure(80+figind);movegui(hf(figind),'south');
yy=sum(S_weight_tor./S_mean,2);
plot(tt,yy,'o-','Color',anc);
text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Relative weight change');drawnow;hold on;
figind=figind+1;

%Oscillation---------------
P=f.P(w);
S_d2=nan(size(w(2:end-1,:)));
P_bouton=nan(size(w(2:end-1,:),1),1);
for i=2:(size(w,1)-1)
    S_d2(i-1,:)=sum(([-0.5;1;-0.5]*ones(1,size(w,2))).*w(i-1:i+1,:),1).*(P(i,:));
    P_bouton(i-1)=sum(P(i,:));
end

%TESTING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%thr=4*std(abs(S_d2(:)));
nexamples=round(size(S_d2,2)*(5/100));
sorted=sort(abs(S_d2(:)));
thr=sorted(end-nexamples);
ind=any(S_d2>=thr,1);
S_d2(:,ind)=[];
P_bouton=P_bouton-sum(P(2:end-1,ind),2);
%TESTING END!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ttt=analysis_aligntime(Dat,[],'start');
ttt=ttt(2:end-1);
ttt_txt=cell(size(w,1)-2,1);
for i=1:numel(ttt_txt)
    ttt_txt{i}=[Dat.L.im_id{i},Dat.L.im_id{i+1},Dat.L.im_id{i+2}];
end

yy=sum(abs(S_d2),2)./P_bouton;
hf(figind)=figure(80+figind);movegui(hf(figind),'southeast');
plot(ttt,yy,'o-','Color',anc);
text(ttt,0.05*mean(yy)+yy,ttt_txt,'HorizontalAlignment','center','Color',anc,...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
xlabel('Time (days)');ylabel('Avg Oscillation');drawnow;hold on;

NN_biostats_addlearningcurve(hf,Dat);
NN_biostats_MakePretty(hf);
end


function NN_biostats_MakePretty(h)
h=h(isgraphics(h));
for i=1:numel(h)
    if strcmp(h(i).Type,'figure')
        h(i).Position=[h(i).Position(1) h(i).Position(2) 668 636];
        hax=findobj(h(i).Children,'flat','Type','Axes');
        if ~isempty(hax)
            axes(hax);
            yyaxis left;
            axis(hax,'square');
            hax.Box='on';
            hax.YAxis(1).Limits(1)=0;
            ylimval=0;
            hl=findobj(hax.Children,'Type','Line');
            for ii=1:numel(hl)
                ylimval=max([ylimval,max(hl(ii).YData(:))]);
            end
            ylimval(ylimval==0)=0.1;
            hax.YAxis(1).Limits(2)=ylimval*1.1;
            grid(hax,'on');
        end
    end
end
end


function NN_biostats_saveplot()
%This sfunction contains a directly pastable script to saves figures for
%analysis in NN_biostatistics.m

hf=findobj(0,'Type','Figure');
for i=1:numel(hf)
    ha=findobj(hf(i),'Type','Axes');
    if contains(ha.YLabel.String,'Added Fraction') || contains(ha.YLabel.String,'Eliminated Fraction')
        %ha.YLim=[0 0.24];
        ha.YAxis(1).Limits=[0 0.24];
    elseif contains(ha.YLabel.String,'Turnover Fraction')
        %ha.YLim=[0 0.45];
        ha.YAxis(1).Limits=[0 0.45];
    elseif contains(ha.YLabel.String,'Depressed Fraction') || contains(ha.YLabel.String,'Potentiated Fraction')
        %ha.YLim=[0 0.6];
        ha.YAxis(1).Limits=[0 0.6];
    elseif contains(ha.YLabel.String,'Avg amount')
        %ha.YLim=[0 2.7];
        ha.YAxis(1).Limits=[0 2.7];
    elseif contains(ha.YLabel.String,'Avg Oscillation')
        %ha.YLim=[0 2.1];
        ha.YAxis(1).Limits=[0 2.1];
    elseif contains(ha.YLabel.String,'Avg added') || contains(ha.YLabel.String,'Avg eliminated')
        %ha.YLim=[0 2.6];
        ha.YAxis(1).Limits=[0 2.6];
    elseif contains(ha.YLabel.String,'Avg weight change per potentiation') || contains(ha.YLabel.String,'Avg weight change per depression')
        %ha.YLim=[0 2.6];
        ha.YAxis(1).Limits=[0 2.6];
    elseif contains(ha.YLabel.String,'Relative weight change')
        %ha.YLim=[0 0.15];
        ha.YAxis(1).Limits=[0 0.15];
    elseif contains(ha.YLabel.String,'Relative Potentiated') || contains(ha.YLabel.String,'Relative Depressed')
        %ha.YLim=[0 0.08];
        ha.YAxis(1).Limits=[0 0.08];
    end
end

cc=lines(5);
cc(2,:)=[];
txtobj=findobj(0,'Type','Text');
for i=1:numel(txtobj)
    txtcol=txtobj(i).Color;
    ord=find(sum(bsxfun(@eq,txtcol,cc),2)==3);
    if ~isempty(ord)
        ord(ord==1)=2;%For network plots where Blue and Yellow are equivalent
        pos=txtobj(i).Position;
        hax=txtobj(i).Parent;
        txtobj(i).HorizontalAlignment='center';
        %txtobj(i).Position(2)=(ord)*(0.05*hax.YLim(2));
        %txtobj(i).Position(2)=(ord)*(0.05*hax.YLim(2));
    end
end
set(findobj(0,'Type','Line'),'LineWidth',2,'Marker','s');
set(findobj(0,'Type','Axes'),'XLim',[-2 65]-12);
drawnow;

hf=findobj(0,'Type','Figure');
for i=1:numel(hf)
    ha=findobj(hf(i),'Type','Axes');
    pathlist;
    %figname=[res_pth,'Motility-10Sess-',ha.YLabel.String];
    figname=[res_pth,'Motility-10Sess-83-withL/','Motility-10Sess-83-withL-rem5-',ha.YLabel.String];
    print(hf(i),'-dpng',[figname,'.png']);
    %savefig(hf(i),[figname,'.fig'])
end
end

function NN_biostats_addlearningcurve(h,Dat)

h=h(isgraphics(h));
for i=1:numel(h)
    if strcmp(h(i).Type,'figure')
        h(i).Position=[h(i).Position(1) h(i).Position(2) 668 636];
        hax=findobj(h(i).Children,'flat','Type','Axes');
        if ~isempty(hax)
            axes(hax)
            yyaxis right
            
            %Shifing and plotting reaction times
            Dat.L.beh_TP_times(Dat.L.beh_t<Dat.L.im_t(1) | Dat.L.beh_t>Dat.L.im_t(end))=[];
            Dat.L.beh_t(Dat.L.beh_t<Dat.L.im_t(1) | Dat.L.beh_t>Dat.L.im_t(end))=[];
            Dat.L.beh_TP_times=Dat.L.beh_TP_times-mean(Dat.L.beh_TP_times)+0.5;
            plot(Dat.L.beh_t-Dat.L.im_t(1),Dat.L.beh_TP_times,'-x');hold on
            
            %Plotting fitted learning curves
            plot(Dat.L.im_t-Dat.L.im_t(1),Dat.L.im_Lfit,'.-r')
            text(Dat.L.im_t-Dat.L.im_t(1),Dat.L.im_Lfit+0.05,Dat.L.im_id,'Color','r','HorizontalAlignment','center',...
    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
            ylim([0 1])
            ylabel('Performance index')
            drawnow;
            yyaxis right
        end
    end
end
end

