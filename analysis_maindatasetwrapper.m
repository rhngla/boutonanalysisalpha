% function []=analysis_maindatasetwrapper(A83,D83,A101,D101,A102,D102,A88,D88)
% 
% opt='SurvivalFrac';
% [Out83]=analysis_maindataset(A83,D83,opt);
% [Out101]=analysis_maindataset(A101,D101,opt);
% [Out102]=analysis_maindataset(A102,D102,opt);
% [Out88]=analysis_maindataset(A88,D88,opt);

%Exludes points not used for fitting
y_fit=[Out83.sfrac(Out83.fitind);Out101.sfrac(Out101.fitind);Out102.sfrac(Out102.fitind);Out88.sfrac(Out88.fitind)];
e_fit=[Out83.e(Out83.fitind);Out101.e(Out101.fitind);Out102.e(Out102.fitind);Out88.e(Out88.fitind)];
t_orig_fit=[Out83.t(Out83.fitind);Out101.t(Out101.fitind);Out102.t(Out102.fitind);Out88.t(Out88.fitind)];
t_temp_fit=[Out83.t(Out83.fitind);Out101.t(Out101.fitind)+200;Out102.t(Out102.fitind)+300;Out88.t(Out88.fitind)+400];
id_fit=[1*ones(size(Out83.fitind(:)));3*ones(size(Out101.fitind(:)));4*ones(size(Out102.fitind(:)));5*ones(size(Out88.fitind(:)))];

%For plotting only
y_plot=[Out83.sfrac(:);Out101.sfrac(:);Out102.sfrac(:);Out88.sfrac(:)];
e_plot=[Out83.e(:);Out101.e(:);Out102.e(:);Out88.e(:)];
t_orig_plot=[Out83.t(:);Out101.t(:);Out102.t(:);Out88.t(:)];
t_temp_plot=[Out83.t(:);Out101.t(:)+200;Out102.t(:)+300;Out88.t(:)+400];
id_plot=[1*ones(size(Out83.sfrac(:)));3*ones(size(Out101.sfrac(:)));4*ones(size(Out102.sfrac(:)));5*ones(size(Out88.sfrac(:)))];

%Simultaneous fitting for all animals with different a and same t1,t2
F1=@(a1,ts,tl,x) (a1*exp(-x/ts)+(1-a1)*exp(-x/tl));
F2=@(a2,ts,tl,x) (a2*exp(-(x-200)/ts)+(1-a2)*exp(-(x-100)/tl));
F3=@(a3,ts,tl,x) (a3*exp(-(x-200)/ts)+(1-a3)*exp(-(x-200)/tl));
F4=@(a4,ts,tl,x) (a4*exp(-(x-300)/ts)+(1-a4)*exp(-(x-300)/tl));
F5=@(a5,ts,tl,x) (a5*exp(-(x-400)/ts)+(1-a5)*exp(-(x-400)/tl));

F=@(a1,a3,a4,a5,ts,tl,x) F1(a1,ts,tl,x).*(x<100 & x>=0)+...
    F3(a3,ts,tl,x).*(x<300 & x>=200)+...
    F4(a4,ts,tl,x).*(x<400 & x>=300)+...
    F5(a5,ts,tl,x).*(x<500 & x>=400);

[fitsfrac,goodness,~]=fit(t_temp_fit,y_fit,F,'StartPoint',[0.3,0.3,0.2,0.2,4,130],'Weights',1./e_fit);
%}

%{
%Simultaneous fitting for all animals with different a,t1 but same t2
F1=@(a1,t1,tl,x) (a1*exp(-x/t1)+(1-a1)*exp(-x/tl));
F2=@(a2,t2,tl,x) (a2*exp(-(x-100)/t2)+(1-a2)*exp(-(x-100)/tl));
F3=@(a3,t3,tl,x) (a3*exp(-(x-200)/t3)+(1-a3)*exp(-(x-200)/tl));
F4=@(a4,t4,tl,x) (a4*exp(-(x-300)/t4)+(1-a4)*exp(-(x-300)/tl));
F5=@(a5,t5,tl,x) (a5*exp(-(x-400)/t5)+(1-a5)*exp(-(x-400)/tl));

F=@(a1,a2,a3,a4,a5,t1,t2,t3,t4,t5,tl,x) F1(a1,t1,tl,x).*(x<100 & x>=0)+...
    F2(a2,t2,tl,x).*(x<200 & x>=100)+...
    F3(a3,t3,tl,x).*(x<300 & x>=200)+...
    F4(a4,t4,tl,x).*(x<400 & x>=300)+...
    F5(a5,t5,tl,x).*(x<500 & x>=400);

[fitsfrac,goodness,c]=fit(t_temp_fit,y_fit,F,'StartPoint',[0.3,0.3,0.3,0.2,0.2,4,4,4,4,4,130],'Weights',1./e_fit);
%}

%{
%Simultaneous fitting for all animals with different a,t2 but same t1
F1=@(a1,ts,t1,x) (a1*exp(-x/ts)+(1-a1)*exp(-x/t1));
F2=@(a2,ts,t2,x) (a2*exp(-(x-100)/ts)+(1-a2)*exp(-(x-100)/t2));
F3=@(a3,ts,t3,x) (a3*exp(-(x-200)/ts)+(1-a3)*exp(-(x-200)/t3));
F4=@(a4,ts,t4,x) (a4*exp(-(x-300)/ts)+(1-a4)*exp(-(x-300)/t4));
F5=@(a5,ts,t5,x) (a5*exp(-(x-400)/ts)+(1-a5)*exp(-(x-400)/t5));

F=@(a1,a2,a3,a4,a5,ts,t1,t2,t3,t4,t5,x) F1(a1,ts,t1,x).*(x<100 & x>=0)+...
    F2(a2,ts,t2,x).*(x<200 & x>=100)+...
    F3(a3,ts,t3,x).*(x<300 & x>=200)+...
    F4(a4,ts,t4,x).*(x<400 & x>=300)+...
    F5(a5,ts,t5,x).*(x<500 & x>=400);

[fitsfrac,goodness,c]=fit(t_temp_fit,y_fit,F,'StartPoint',[0.3,0.3,0.3,0.2,0.2,4,130,130,130,130,130],'Weights',1./e_fit);
%}

%{
opt='SurvivalFrac';
[Out83]=analysis_maindataset(A83,D83,opt);
[Out85]=analysis_maindataset(A85,D85,opt);
[Out101]=analysis_maindataset(A101,D101,opt);
[Out102]=analysis_maindataset(A102,D102,opt);
[Out88]=analysis_maindataset(A88,D88,opt);

%Exludes points not used for fitting
y_fit=[Out83.sfrac(Out83.fitind);Out85.sfrac(Out85.fitind);Out101.sfrac(Out101.fitind);Out102.sfrac(Out102.fitind);Out88.sfrac(Out88.fitind)];
e_fit=[Out83.e(Out83.fitind);Out85.e(Out85.fitind);Out101.e(Out101.fitind);Out102.e(Out102.fitind);Out88.e(Out88.fitind)];
t_orig_fit=[Out83.t(Out83.fitind);Out85.t(Out85.fitind);Out101.t(Out101.fitind);Out102.t(Out102.fitind);Out88.t(Out88.fitind)];
t_temp_fit=[Out83.t(Out83.fitind);Out85.t(Out85.fitind)+100;Out101.t(Out101.fitind)+200;Out102.t(Out102.fitind)+300;Out88.t(Out88.fitind)+400];
id_fit=[1*ones(size(Out83.fitind(:)));2*ones(size(Out85.fitind(:)));3*ones(size(Out101.fitind(:)));4*ones(size(Out102.fitind(:)));5*ones(size(Out88.fitind(:)))];

%For plotting only
y_plot=[Out83.sfrac(:);Out85.sfrac(:);Out101.sfrac(:);Out102.sfrac(:);Out88.sfrac(:)];
e_plot=[Out83.e(:);Out85.e(:);Out101.e(:);Out102.e(:);Out88.e(:)];
t_orig_plot=[Out83.t(:);Out85.t(:);Out101.t(:);Out102.t(:);Out88.t(:)];
t_temp_plot=[Out83.t(:);Out85.t(:)+100;Out101.t(:)+200;Out102.t(:)+300;Out88.t(:)+400];
id_plot=[1*ones(size(Out83.sfrac(:)));2*ones(size(Out85.sfrac(:)));3*ones(size(Out101.sfrac(:)));4*ones(size(Out102.sfrac(:)));5*ones(size(Out88.sfrac(:)))];

%Simultaneous fitting for all animals with different a and same t1,t2
F1=@(a1,ts,tl,x) (a1*exp(-x/ts)+(1-a1)*exp(-x/tl));
F2=@(a2,ts,tl,x) (a2*exp(-(x-100)/ts)+(1-a2)*exp(-(x-100)/tl));
F3=@(a3,ts,tl,x) (a3*exp(-(x-200)/ts)+(1-a3)*exp(-(x-200)/tl));
F4=@(a4,ts,tl,x) (a4*exp(-(x-300)/ts)+(1-a4)*exp(-(x-300)/tl));
F5=@(a5,ts,tl,x) (a5*exp(-(x-400)/ts)+(1-a5)*exp(-(x-400)/tl));

F=@(a1,a2,a3,a4,a5,ts,tl,x) F1(a1,ts,tl,x).*(x<100 & x>=0)+...
    F2(a2,ts,tl,x).*(x<200 & x>=100)+...
    F3(a3,ts,tl,x).*(x<300 & x>=200)+...
    F4(a4,ts,tl,x).*(x<400 & x>=300)+...
    F5(a5,ts,tl,x).*(x<500 & x>=400);

[fitsfrac,goodness,~]=fit(t_temp_fit,y_fit,F,'StartPoint',[0.3,0.3,0.3,0.2,0.2,4,180],'Weights',1./e_fit);
%}

disp('Combined fitting results')
display(fitsfrac);
display(goodness);
figure(5023),
cc=lines(5);
for i=[1,2,3,4,5]
    errorbar(t_orig_plot(id_plot==i),y_plot(id_plot==i),e_plot(id_plot==i),'.','Color',cc(i,:),'MarkerEdgeColor','none','Marker','none'),hold on
    plot(t_orig_plot(id_plot==i),y_plot(id_plot==i),'s','Color',cc(i,:),'MarkerSize',5,'MarkerFaceColor',cc(i,:)),hold on
    plot([t_orig_plot(id_plot==i)],fitsfrac([t_temp_plot(id_plot==i)]),'-','Color',cc(i,:)),hold on
end
axis square;
ylim([-0.1 1.1])
xlim([-2,max(t_orig_fit(:))])
xlim([-2 70]);ylim([-0.02 1.02]);hax=gca;hax.XTick=0:8:72;
xlabel('Time (days)')
ylabel('Survival Fraction')
set(gca,'FontName','Calibri')
set(gca,'FontSize',15)
1;
%end