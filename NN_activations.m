function [ind] = NN_activations(net,D)
%This function returns the activations at the classification layer before
%thresholding. The distributions of learners and non-learners are expected
%for networks that are able to classify learners and non-learners with high
%accuracy. This can be used to identify boutons that produce the highest
%activations, and thus could be treated as "learning boutons"
%input D is a matrix of normalized bouton weights
%net is the trained network

after_relu=nan(size(D,2),50);
after_softmax=nan(size(D,2),2);
for i=1:size(D,2)
    this_bouton=repmat(D(:,i),1,50);
    after_relu(i,:)=activations(net,this_bouton,3);
    after_softmax(i,:)=activations(net,this_bouton,6);
end

%figure,imagesc(after_relu);caxis([0 1])
unique(after_relu);
figure,histogram(after_relu,[0:0.1:1],'Normalization','PDF')

%figure(103),histogram(after_softmax(:,1),1000);hold on
ind=find(any(after_relu,2));
end