function [] = NN_script(anid)

[~,val]=system('hostname');
if strcmp(val(1:3),'NCT')
    path_exp='/home/sm1/Desktop/Rohan/dat/NN/NN_Arch_2/';
    load('/home/sm1/Desktop/Rohan/dat/NN_Dat_v2.mat','O*');
else
    parentdir=pwd;
    parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
    path_exp=isunixispc([parentdir,'/dat/NN/NN_Arch_2/']);
    load(isunixispc([parentdir,'/BoutonAnalysis/dat/NN_Dat_v2.mat']),'O*');
end

%Cuts data, equalizes histograms and generate L and NL sets
[L,~,~,NL] = NN_eqhist(O83,O85,O101,O102,O88,anid);

%n_networks will be trained per compute node
n_networks=50;
for i=1:n_networks
    rng('shuffle');
    [IM,lbl,layers,options]=NN_genex(L,NL);
    [net,traininfo] = trainNetwork(IM,lbl,layers,options);
    
    %Averaging training information to limit data size
    traininfo.TrainingLoss=movmean(traininfo.TrainingLoss,1000);
    traininfo.TrainingLoss=traininfo.TrainingLoss(1:1000:end);
    traininfo.TrainingAccuracy=movmean(traininfo.TrainingAccuracy,1000);
    traininfo.TrainingAccuracy=traininfo.TrainingAccuracy(1:1000:end);
    traininfo=rmfield(traininfo,'BaseLearnRate');
    
    save(isunixispc([path_exp,num2str(anid),'-',num2str(randi(100000)),'-',datestr(now,'FFF')]),'net','traininfo','options');
end
end
