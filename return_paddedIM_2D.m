function Canvas=return_paddedIM_2D(IM,r,pad)
%r is a 2d position
%pad is a scalar
Canvas=zeros(2*pad+1,2*pad+1);
overflowx=zeros(1,2);
overflowy=zeros(1,2);

r=round(r);
minx=max(1,r(1)-pad);
maxx=min(r(1)+pad,size(IM,1));

miny=max(1,r(2)-pad);
maxy=min(r(2)+pad,size(IM,2));

overflowx(1)=minx-(r(1)-pad)+1;
overflowx(2)=size(Canvas,1)-(r(1)+pad-maxx);

overflowy(1)=miny-(r(2)-pad)+1;
overflowy(2)=size(Canvas,2)-(r(2)+pad-maxy);

Canvas(overflowx(1):overflowx(end),overflowy(1):overflowy(end))=IM(minx:maxx,miny:maxy);
end