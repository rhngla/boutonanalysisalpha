%{
p = @(n,m,k) ...
    1 - (((n-k)*(n-m)./n./(n-m-k)).^0.5).*...
    (...
    ((1-(m./n)).^(n-m)).*...
    ((1-(k./n)).^(n-k))./...
    ((1-(m+k)./n).^(n-(m+k)))...
    );

n=500;  % dataset from which k samples are drawn
m=1:n;   % number of outliers
k=10;   % number of samples per example

figure,
s=p(n,m,k);
inrange=s>0 & s<1;
plot(m(inrange),s(inrange),'.','MarkerSize',10)
title(sprintf('n = %d ,k = %d',n,k))
ylabel('Fraction of examples with atleast one outlier')
xlabel('# of outliers')
ylim([0 1])
grid on
axis square
drawnow;
%}

M=cell(4,1);
Max=cell(4,1);
M{1}=O83.w(end-9:end,:);
M{2}=O101.w(end-9:end,:);
M{3}=O102.w(end-9:end,:);
M{4}=O88.w(end-9:end,:);

Max{1}=O83.ax_id+83*10^4;
Max{2}=O101.ax_id+101*10^4;
Max{3}=O102.ax_id+102*10^4;
Max{4}=O88.ax_id+88*10^4;

Md{1}=O83.d;
Md{2}=O101.d;
Md{3}=O102.d;
Md{4}=O88.d;

%Remove boutons from the cut set
for i=1:numel(M)
    keep=any(M{i}>2);
    Max{i}=Max{i}(:,keep);
    M{i}=M{i}(:,keep);
    Md{i}=Md{i}(:,keep);
    
    %Count number of boutons on unique axons
    %sort(histcounts(categorical(Max{i}))');
    
    %Keep 250 randomly boutons from each animal
    %This is done to allow histogram equalization and also to avoid bias
    %from different size of datasets for different animals
    %ind=sort(randperm(size(M{i},2),250));
    ind=1:250;
    M{i}=M{i}(:,ind);
    Max{i}=Max{i}(:,ind);
    Md{i}=Md{i}(:,ind);
end

%Histogram equalization:
dist=sort(M{1}(1,:));
for i=1:numel(M)
    for t=1:size(M{i},1)
        [~,ind]=sort(M{i}(t,:));
        M{i}(t,ind)=dist;
    end
end

L=[M{1},M{2}];NL=[M{3},M{4}];
Lax=[Max{1},Max{2}];NLax=[Max{3},Max{4}];
Ld=[Md{1},Md{2}];NLd=[Md{3},Md{4}];