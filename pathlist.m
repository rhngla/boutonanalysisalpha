%im_pth - Raw image files parent folder
%man_pth - Path for manual trace .swc files
%profile_pth - Path for profile data structure (.mat file)

[~,val]=system('hostname');
parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

%{
%Default paths for BoutonAnalyzer software
im_pth=isunixispc([parentdir,'\BoutonAnalysis\dat\Images\']);
man_pth=isunixispc([parentdir,'\BoutonAnalysis\dat\Traces\']);
profile_pth=isunixispc([parentdir,'\BoutonAnalysis\dat\Profiles\']);
excel_pth=isunixispc([parentdir,'\BoutonAnalysis\dat\ExcelData\']);
err_pth=isunixispc([parentdir,'\BoutonAnalysis\dat\errorlogs\']);
proc_pth=isunixispc([parentdir,'\BoutonAnalysis\dat\Results\']);
%}

%{
%For controls: different imaging conditions
im_pth='E:\DatasetIM\Control-1\';
man_pth=isunixispc([parentdir,'\dat\Controls\Conditions\ManualTraces\']);
optim_pth=isunixispc([parentdir,'\dat\Controls\Conditions\OptimizedTraces_v3\']);
profile_pth=isunixispc([parentdir,'\dat\Controls\Conditions\Profiles_v5\']);%Gauss fitted using LoGxy peaks mu
%profile_pth=isunixispc([parentdir,'\dat\Controls\Conditions\Profiles_v3\']);%Gauss and LoGxy fitted independently
proj_pth=isunixispc([parentdir,'\dat\Projections_v2\']);
err_pth=isunixispc([parentdir,'\BoutonAnalysis\code\errorlogs\']);
%}

%
%Main dataset
im_pth='E:\Rohan\DatasetIM\';
man_pth=isunixispc([parentdir,'\dat\ManualTraces_v2\']);
profile_pth=isunixispc([parentdir,'\dat\Profiles_v3\']);
err_pth=isunixispc([parentdir,'\BoutonAnalysis\release\errorlogs\']);
behfold=isunixispc([parentdir,'\dat\LearningData_v3\']);
headerf=isunixispc([parentdir,'\dat\IMHeaders\']);
path_3state=isunixispc([parentdir,'\dat\ThreeStateModel\']);
res_pth=isunixispc([parentdir,'\docs\Update v12\']);
%}

