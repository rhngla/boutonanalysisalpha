function exitcount=nogui_optimization(Im,AM,r,inform)
%This code takes AM,r,Im as input and saves a profile, using default
%parameters.
nogui_paramset;
pathlist;

channel=fieldnames(Im);
channel=channel{1};

%1. Optimize trace
[Profile.AM.optim,Profile.r.optim,~,~,exitcount]=...
    Optimize_Trace_v1(Im.(channel),AM,r,zeros(size(r,1),1), ...
    params.opt.Rtypical,params.opt.Optimize_bps,params.opt.Optimize_tps,params.opt.pointspervoxel,params.opt.MaxIterations, ...
    params.opt.alpha_r,params.opt.betta_r,params.opt.isadjustpointdensity,1);

%2. Subdividing optimized trace
[Profile.AM.optim,Profile.r.optim,~]=...
    AdjustPPM(Profile.AM.optim,Profile.r.optim,zeros(size(Profile.r.optim,1),1),params.profile.pointspervoxel);

%3. Generate profile
%Re-order AM and r
Profile.annotate.ignore=false(size(Profile.r.optim,1),1);
startt=find(sum(Profile.AM.optim,1)==1);
startt=startt(1);
[Profile.AM.optim,Profile.r.optim,Profile.annotate.ignore]=...
    orderprofile(Profile.AM.optim,Profile.r.optim,Profile.annotate.ignore,startt);

%Calculating path distance and filter intensity in all channels
[Profile.d.optim]=vx2um(Profile.r.optim);
Profile.d.aligned=Profile.d.optim;
Profile.d.alignedxy=px2um(Profile.r.optim);
channel=fieldnames(Im);
for ch=1:numel(channel)
    for fi=1:numel(params.filt.types)
        [temp.I,temp.R]=profilefilters(Profile.r.optim,Im.(channel{ch}),params.filt.types{fi},params);
        Profile.I.(channel{ch}).(params.filt.types{fi}).raw=temp.I;
        Profile.I.(channel{ch}).(params.filt.types{fi}).caliber=temp.R;
        Profile.I.(channel{ch}).(params.filt.types{fi}).norm=temp.I./mean(temp.I(~Profile.annotate.ignore));
    end
end

%4. Create projections using optimized trace
channel=fieldnames(Im);
sizeIm=size(Im.(channel{1}));%Size of all channels is the same.

pad=20;
minr=min(r,[],1);
maxr=max(r,[],1);
minx=round(max(minr(1)-pad,1));maxx=round(min(maxr(1)+pad,sizeIm(1)));
miny=round(max(minr(2)-pad,1));maxy=round(min(maxr(2)+pad,sizeIm(2)));
minz=round(max(minr(3)-pad,1));maxz=round(min(maxr(3)+pad,sizeIm(3)));

[~,SVr,~]=AdjustPPM(Profile.AM.optim,Profile.r.optim,zeros(size(Profile.r.optim,1),1),1);

%Perform fast marching on restricted volume:
[KT]=FastMarchingTube([maxx-minx+1,maxy-miny+1,maxz-minz+1],[SVr(:,1)-minx+1,SVr(:,2)-miny+1,SVr(:,3)-minz+1],params.proj.fm_dist,[1 1 1]);
Filter=false(sizeIm);
Filter(minx:maxx,miny:maxy,minz:maxz)=KT;
Filter=double(Filter);

display('Updating projections...');
for ch=1:numel(fieldnames(Im))
    Profile.proj.(channel{ch}).xy.full=max(permute(Im.(channel{ch}),[1,2,3]),[],3);
    Profile.proj.(channel{ch}).zy.full=max(permute(Im.(channel{ch}),[3,2,1]),[],3);
    Profile.proj.(channel{ch}).xz.full=max(permute(Im.(channel{ch}),[1,3,2]),[],3);
    
    Im_ax=Filter.*Im.(channel{ch});
    Profile.proj.(channel{ch}).xy.ax=max(permute(Im_ax,[1,2,3]),[],3);
    Profile.proj.(channel{ch}).zy.ax=max(permute(Im_ax,[3,2,1]),[],3);
    Profile.proj.(channel{ch}).xz.ax=max(permute(Im_ax,[1,3,2]),[],3);
    display(['Completed for channel ',channel{ch}]);
end

%5. Create additional fields
for ch=1:numel(fieldnames(Im))
    Profile.fit.(channel{ch})=struct();
end

%Create fit and id fields
stack_id=[inform.animal,inform.timepoint,inform.section];
fname=isunixispc([profile_pth,stack_id,filesep,inform.axon,'.mat']);
Profile.id=[stack_id,'-',inform.axon];

Profile=orderfields(Profile,{'AM','r','d','I','annotate','fit','proj','id'});

%Check directory
if ~exist(isunixispc([profile_pth,stack_id]),'dir')
    mkdir(isunixispc(profile_pth),stack_id);
    display(['Creating directory: ', isunixispc([profile_pth,stack_id])]);
end
save(fname,'-struct','Profile');
disp(['Saved profile in ',fname]);