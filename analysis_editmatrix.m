function [Dat] = analysis_editmatrix(D)
%This function
%1. puts normalizied bouton weights in 'w' field
%2. excludes potentially mismatched boutons
%3. removes extra child structure fields in D

[D]=edit_D(D,[]);
f=fP_handles;

%Restrict analysis to boutons defined by the following criteria
%bouton_def=any(f.P(D.G.LoGxy.w)>0,1);
%bouton_def=sum((D.G.LoGxy.w)>2,1)>=1;
bouton_def=sum(f.P(D.G.LoGxy.w)>0.5,1)>=1;

%Calculate boutons that are far based on optimal linear transformations to
%align axons over any pair of times
[~,btndist] = analysis_nldistortion(D);
dthr=mean(btndist)+2*std(btndist);
mismatchedind=btndist>dthr;
mismatched=false(size(bouton_def));
mismatched(mismatchedind)=true;

keep=bouton_def & ~mismatched;
[D]=edit_D(D,keep);

%{
disp(['Animal: ',num2str(D.anid)]);
sprintf('Sessions:%5.0f \nn(Axons):%5.0f \nTotalAxonLength:%5.1f  \nn(Boutons):%5.0f \nn(Put. Boutons)%5.0f \nn(Mismatch removals)%5.0f \nn(Did not meet bouton def.)%5.0f  \nn(Unique removals)%5.0f',...
    size(D.G.LoGxy.amp,1),numel(unique(D.G.LoGxy.ax_id)),mean(sum(D.G.LoGxy.blen,2)),mean(sum(f.P(D.G.LoGxy.w),2)),size(D.G.LoGxy.amp,2),sum(mismatched),sum(~bouton_def),sum(~keep))
%}

Dat.w=D.G.LoGxy.w;
Dat.dw=f.P_noise(Dat.w);
Dat.P=f.P(Dat.w);
Dat.ax_id=D.G.LoGxy.ax_id;
Dat.ax_length=D.ax_length;
Dat.rx=D.G.LoGxy.rx;
Dat.ry=D.G.LoGxy.ry;
Dat.rz=D.G.LoGxy.rz;
Dat.d=D.G.LoGxy.d;
Dat.nx=D.G.LoGxy.nx;
Dat.ny=D.G.LoGxy.ny;
Dat.nz=D.G.LoGxy.nz;
Dat.L=D.L;
Dat.blen=D.G.LoGxy.blen;
Dat.ind=D.G.LoGxy.ind;
Dat.anid=D.anid;

end

%Support functions
%--------------------------------------------------------------------------
function [D]=edit_D(D,keep)
%This function cuts the D.G.LoGxy matrix
%Input 'keep' can be logical or indexed
%All fields other than norm have second dimension = n(boutons)

if isempty(keep)
    %All boutons retained if keep is empty
    keep=true(size(D.G.LoGxy.ax_id));
end

ax_orig=unique(D.G.LoGxy.ax_id);
fnames=fieldnames(D.G.LoGxy);
[~,ind]=ismember('norm',fnames);
fnames(ind)=[];
for f=1:numel(fnames)
    D.G.LoGxy.(fnames{f})=D.G.LoGxy.(fnames{f})(:,keep);
end

lia=ismember(ax_orig,unique(D.G.LoGxy.ax_id));
D.G.LoGxy.norm=D.G.LoGxy.norm(:,lia);

%Add the weight field------------------------------------------------------
ax_lbl=unique(D.G.LoGxy.ax_id,'stable');

%Calculate remaining axon length and normalization constants
Nf=ones(size(D.G.LoGxy.amp,1),numel(ax_lbl));
ax_length=ones(size(D.G.LoGxy.amp,1),numel(ax_lbl));
for ax=1:numel(ax_lbl)
    for ti=1:size(D.G.LoGxy.norm,1)
        Nf(ti,ax)=D.G.LoGxy.norm{ti,ax}.gauss_norm;
        ax_length(ti,ax)=mean(sum(D.G.LoGxy.blen(:,D.G.LoGxy.ax_id==ax_lbl(ax)),2));
    end
end

w=D.G.LoGxy.amp+D.G.LoGxy.Ibg;
w(isnan(w))=D.G.LoGxy.Inorm(isnan(w));
wmedian=D.G.LoGxy.Imedian;

%Normalization is averaged over all times.
Nf=mean(Nf,1);
Nf=bsxfun(@times,ones(size(D.G.LoGxy.amp,1),1),Nf);
for ax=1:numel(ax_lbl)
    for ti=1:size(D.G.LoGxy.norm,1)
        w(ti,D.G.LoGxy.ax_id==ax_lbl(ax))=w(ti,D.G.LoGxy.ax_id==ax_lbl(ax))./Nf(ti,ax);
        wmedian(ti,D.G.LoGxy.ax_id==ax_lbl(ax))=wmedian(ti,D.G.LoGxy.ax_id==ax_lbl(ax))./Nf(ti,ax);
    end
end

D.G.LoGxy.w=w;
D.G.LoGxy.wmedian=wmedian;
D.Nf=Nf;
D.ax_length=ax_length;
D.ax_lbl=ax_lbl;
end