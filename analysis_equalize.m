function Weq=analysis_equalize(W,W_distrib)
%This function equalizes weight matrix W using distribution created by
%pooling weight matrices in W_distrib

disp('Data equalized using distributions provided');
distrib=[];
for i=1:numel(W_distrib)
    distrib=[distrib;W_distrib{i}(:)];
end
distrib=sort(distrib);
distrib(distrib<0)=0;
Weq=W;
for t=1:size(W,1)
    [~,ind]=sort(W(t,:));
    val=nan(size(W,2),1);
    nbtns=numel(distrib)./numel(W(t,:));
    for b=1:size(W,2)
        val(b)=mean(distrib(round((b-1)*nbtns:b*nbtns-1)+1));
    end
    Weq(t,ind)=val;
end
end