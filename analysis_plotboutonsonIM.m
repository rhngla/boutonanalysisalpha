function analysis_plotboutonsonIM(D,A,ind)
%This function takes in D in the format specified by analysis_editmatrix.m and A specified
%by analysis_loaddat.m. It imports the axon projections for boutons specified by ind 
%Input ind is logical.

%>Import trace for profile in session t
%>Mark ignored regions based on the bouton length field
%>use ind to highlight specific boutons
%>A is assumed to have all axons that are listed in D

pathlist;%The projections will be loaded from within profiles.

t=1;%Any available session works.
if ~islogical(ind)
   temp=false(size(D.ax_id));
   temp(ind)=true;
   ind=temp;clear temp;
end

ax_lbl=unique(D.ax_id);
ss=unique(round(ax_lbl./1000));
sA=round(ax_lbl./1000);
ax_id=mod(ax_lbl,1000);
axA=nan(size(ax_lbl));
Nf_times=nan(size(D.ind,1),numel(ax_lbl));
Nf_axons=nan(size(D.ind,1),numel(ax_lbl));

for i=1:numel(ss)
    sA(sA==ss(i))=i;
end

%Finding the correct indices for axons in A that correspond to boutons in D
for a=1:numel(ax_lbl)
    for temp_a=1:numel(A(1).Section{sA(a)}.Time{t}.Axon)
        if isequal(str2double(A(1).Section{sA(a)}.Time{t}.Axon{temp_a}.id(end-2:end)),ax_id(a))
            axA(a)=temp_a;
        end
    end
end

%Calculate normalization to mean - This is recalculated to normalize the image
for a=1:numel(ax_lbl)
    for t=1:size(D.ind,1)
        Axon=A(1).Section{sA(a)}.Time{t}.Axon{axA(a)};
        Nf_times(t,a)=mean((Axon.I.G.LoGxy.raw(~Axon.annotate.ignore)));%Normalization to the mean
        Nf_axons(t,a)=Axon.norm.LoGxy.gauss_norm;%2nd normalization
        clear Axon;
    end
end

Nf_axons=mean(Nf_axons,1);
Nf_axons=bsxfun(@times,ones(size(D.w,1),1),Nf_axons);

%Retrieve boutons projections
pad=30;
imsiz=(2*pad+1);
sep=1;
nbtns=sum(ind);
Canvas=1000*ones(imsiz*size(D.ind,1)+sep*(size(D.ind,1)-1),imsiz*nbtns+sep*(nbtns-1));
for t=1:size(D.ind,1)
    bcount=0;
    for a=1:numel(ax_lbl)
        if ismember(ax_lbl(a),unique(D.ax_id(ind)))
            thisax=D.ax_id==ax_lbl(a);
            rx=D.rx(t,thisax & ind);
            ry=D.ry(t,thisax & ind);
            
            axon_name=strsplit(A(1).Section{sA(a)}.Time{t}.Axon{axA(a)}.id,'-');
            axon_IM=load(isunixispc([profile_pth,axon_name{1},filesep,axon_name{2},'.mat']),'proj');
            axon_IM=axon_IM.proj.G.xy.ax;
            for b=1:numel(rx)
                IM_btn=return_paddedIM_2D(axon_IM,[rx(b),ry(b)],pad);
                xstart=(imsiz+sep)*bcount+1;
                xend=xstart+imsiz-1;
                tstart=(imsiz+sep)*(t-1)+1;
                tend=tstart+imsiz-1;
                Canvas(tstart:tend,xstart:xend)=IM_btn./Nf_times(t,a)./Nf_axons(t,a)./10000;
                bcount=bcount+1;
            end
        end
    end
end

figure,
imagesc(Canvas);colormap gray;axis equal;axis tight,grid on;hold on;
tcount=size(D.ind,1);
x=((1:bcount)-1).*(imsiz+sep)+pad+1;
y=((1:tcount)-1).*(imsiz+sep)+pad+1;y=y(:);
text(x',-10*ones(size(x))',num2str(D.ax_id(ind)'),'HorizontalAlignment','center')
x=repmat(x,tcount,1);
y=repmat(y,1,bcount);
plot(x(:),y(:),'rx')

caxis([0 5]);colorbar;
xlim(get(gca,'YLim'));
end
