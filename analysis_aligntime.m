function [t] = analysis_aligntime(D,letterid,epoch)
%Input D is the output form analysis_editmatrix.m
%times aligned by letterid if specified and epoch=='custom'
%Example: analysis_aligntime(D,'G','custom')

switch epoch
    
    case 'start'
        t=D.L.im_t-D.L.im_t(1);
        
    case 'audio'
        t=D.L.im_t-D.L.t_audio;
        
    case 'audio&stim'
        t=D.L.im_t-D.L.t_stimaudio;
        
    case 'stim'
        t=D.L.im_t-D.L.t_stimonly;
        
    case 'learn'
        vals=coeffvalues(D.L.fit_f);%The third coefficient is mu for the sigmoid fit in analysis_loadlearn.m
        t=D.L.im_t-vals(3);
        
    case 'custom'
        switch D.anid
            case 83
                letterid='G';
            case 85
                letterid='G';
            case 101
                letterid='G';
            case 102
                letterid='G';
            case 88
                letterid='G';
        end
        t_ind = contains(D.L.im_id,letterid); %Works in Matlab2016+
        t=D.L.im_t-D.L.im_t(t_ind);
end

end