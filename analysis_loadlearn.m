function L = analysis_loadlearn(anid,timepoint,fitind)
%analysis_loadlearn - Pull behavior and imaging time data from header files. Uses
%hard coded values if loaded_let and fitind are passed as empty. Function
%is meant for use by analysis_loaddata, or to check full Learning curve directly.
%Usage example:
%L83=analysis_loadlearn(83,[],[]);
pathlist;
animal=sprintf('DL%03d',anid);
if anid==83
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('N'))');
        fitind=13:48;
    end
    lblcells=228;%From cell count summary_v2.pdf
elseif anid==85
    if isempty(timepoint)
        timepoint=cellstr(char(double('C'):double('T'))');
        fitind=9:72;
    end
    lblcells=186;%From cell count summary_v2.pdf
elseif anid==101
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('R'))');
        fitind=15:80;
    end
    lblcells=272;%From cell count summary_v2.pdf
elseif anid==102
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('N'))');
        fitind=21:79;%Inflection point changes if starting from 23;
    end
    lblcells=24;%From cell count summary_v2.pdf
elseif anid==88
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('Q'))');
        fitind=8:54;
    end
    lblcells=210;%From cell count summary_v2.pdf
elseif anid==68
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('E'))');
        fitind=11:48;
    end
    lblcells=68;%From cell count summary_v2.pdf
elseif anid==89
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('E'))');
        fitind=16:45;
    end
    lblcells=156;%From cell count summary_v2.pdf
elseif anid==108
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('M'))');
        fitind=12:56;
    end
    lblcells=nan;%From cell count summary_v2.pdf
elseif anid==01
    if isempty(timepoint)
        timepoint=cellstr(char(double('A'):double('G'))');
        fitind=[];
    end
end

if anid~=01
    dbeh=load(isunixispc([behfold,animal,'.mat']));
    H=load(isunixispc([headerf,animal,'h.mat']));
    sfn=numel(H.fname);
    dimage=nan(numel(timepoint),1);
    
    letterlist=cell(sfn,1);
    for i=1:sfn
        letterlist{i}=H.fname{i}(13);
    end
    
    for s = 1:numel(timepoint)
        list_ind=find(cellfun(@(x) (strcmp(x,timepoint(s))),letterlist));
        if ~isempty(list_ind)
            dat = H.t2p(list_ind,1).internal.triggerTimeString;
            form = 'mm/dd/yyyy HH:MM:SS.FFF';
            dimage(s) = datenum(dat,form);
        end
    end
    
    L.licktimesTP=nan(numel(dbeh.reactionTime),1);
    L.licktimesTN=nan(numel(dbeh.reactionTime),1);
    L.licktimesFP=nan(numel(dbeh.reactionTime),1);
    L.licktimesFN=nan(numel(dbeh.reactionTime),1);
    for i=1:numel(dbeh.reactionTime)
        L.licktimesTP(i)=nanmedian(dbeh.reactionTime(i).hit);
        L.licktimesTN(i)=nanmedian(dbeh.reactionTime(i).correctReject);
        L.licktimesFP(i)=nanmedian(dbeh.reactionTime(i).falseAlarm);
        L.licktimesFN(i)=nanmedian(dbeh.reactionTime(i).miss);
    end

    %----------------------------------------------------------------------
    %Fit here. Fit performed on absolute time.
    s=fittype('a+(b)/(1+exp((mu-x)/t))');
    L.t=dbeh.expdate.num;
    L.measure=dbeh.percentCorrect/100;
    L.stimamp=dbeh.stimAmp.mW;
    [fit_f,~,~]=fit(L.t(fitind),L.measure(fitind),s,'StartPoint',[0.5,0.5,mean(L.t(fitind)),4],'Lower',[0 0 0 0]);
    
    %Merge with imaging data points here
    all_t=[dbeh.expdate.num(:);dimage(:)]; %times for merged imaging+learning data
    all_Lfit=fit_f(all_t);
    all_Lmeasure=[L.measure(:);nan(size(dimage(:)))];
    %all_im_ind=[zeros(size(dbeh.expdate.num(:)));ones(size(dim(:)))];
    
    [all_t,sortind]=sort(all_t);
    all_Lfit=all_Lfit(sortind);
    all_Lmeasure=all_Lmeasure(sortind);
    %all_im_ind=all_im_ind(sortind);
    
    im_t=dimage(:);
    im_Lfit=fit_f(im_t);
    im_id=timepoint;
    
    t_audio=dbeh.expdate.num(find(dbeh.phase==1,1,'first'));
    t_stimaudio=dbeh.expdate.num(find(dbeh.phase==2,1,'first'));
    t_stimonly=dbeh.expdate.num(find(dbeh.phase==3,1,'first'));
    t_inflection=fsolve(@(x) (fit_f.a+(fit_f.b)/(1+exp((fit_f.mu-x)/fit_f.t)))-(fit_f.a+0.5*fit_f.b),fit_f.mu,optimoptions('fsolve','display','none'));
    %t_startlearn=im_t(find(diff(im_Lfit)>0.01,1,'first'));
    t_startlearn2=fit_f.mu-3*fit_f.t;
    t_endlearn2=fit_f.mu+3*fit_f.t;
    
    direc={'northwest','north','northeast','south','southeast','southwest','northwest','north'};
    allan=[83,85,101,102,88,108,68,89];
    anind=(allan==anid);
    anc=lines(numel(allan));anc(end,:)=[0 0 0];
    anc=anc(anind,:);
    direc=direc{anind};
    hh=figure;movegui(hh,direc);hold on;hh.Name=animal;
    yyaxis left
    plot(all_t-im_t(1),all_Lfit,'-','Color',anc)
    %plot(all_t,all_Lmeasure,'x','Color',anc);
    plot(L.t(fitind)-im_t(1),L.measure(fitind),'o','Color',[0.5 0.5 0.5],'MarkerSize',5);
    plot(im_t-im_t(1),im_Lfit,'s','MarkerSize',7,'Color',anc,'MarkerFaceColor',anc);
    plot([t_audio,t_audio]-im_t(1),[0 1],'-','Color',[0.8 0 0]);
    plot([t_stimaudio,t_stimaudio]-im_t(1),[0 1],'-','Color',[0.8 0.8 0]);
    plot([t_stimonly,t_stimonly]-im_t(1),[0 1],'-','Color',[0 0.7 0]);
    %plot([t_inflection,t_inflection]-im_t(1),[0 1],'-','Color',[0.4 0.4 0.4]);
    %plot([t_startlearn2,t_startlearn2]-im_t(1),[0 1],'--','Color',[0.4 0.4 0.4]);
    %plot([t_endlearn2,t_endlearn2]-im_t(1),[0 1],'--','Color',[0.4 0.4 0.4]);
    text(im_t-im_t(1),im_Lfit+0.05,im_id,'horizontalAlignment', 'center','FontSize',13)
    %text(im_t,im_Lfit-0.05,num2str((1:numel(im_id))'),'horizontalAlignment', 'center','FontSize',13)
    text_x=(im_t(1:end-1)+im_t(2:end))./2 - -im_t(1);
    text_y=fit_f(text_x)+0.02;
    title(['# GFP labeled cells = ',num2str(lblcells)])
    %text(text_x,text_y,num2str(round(diff(im_t),1)),'horizontalAlignment', 'center','FontSize',13,'Color',[0.5 0 0],'Rotation',45)
    xlim([all_t(1)-im_t(1)-1,all_t(end)-im_t(1)+1]);ylim([0 1])
    xlabel('Time')
    h=gca;axis square;
    h.YColor=[0 0 0];
    ylabel('Performace index')
    h.XLim(1)=round(h.XLim(1))-2;
    h.XLim(2)=h.XLim(1)+77; %round(h.XLim(2))+2;
    %h.XTick=min(im_t-im_t(1)):16:h.XLim(2);
    h.YTick=0:0.2:1;
    set(gca,'FontName','Calibri','FontSize',15);
    grid on
    drawnow;
    
    yyaxis right
    h=gca;
    plot(L.t(:)-im_t(1),L.licktimesTP(:),'-x','Color',[0.8 0 0],'MarkerSize',5);
    h.YColor=[0.8 0 0];
    ylabel('Hit reaction time');
    h.YLim=[0 1.8];
    drawnow;
    
    %{
    fname=isunixispc(['/Users/Fruity/Dropbox/Lab/Plasticity/docs/Update v12/Psychophysics-',hh.Name]);
    savefig(hh,[fname,'.fig']);
    print(gcf, '-dpng',[fname,'.png']);
    %}
    
    %{
    figure,
    cc=lines(4);
    plot(L.t(:)-im_t(1),L.licktimesTP(:),'-','Color',cc(1,:),'MarkerSize',5);hold on
    plot(L.t(:)-im_t(1),L.licktimesTN(:),'-','Color',cc(2,:),'MarkerSize',5);hold on
    plot(L.t(:)-im_t(1),L.licktimesFP(:),'-','Color',cc(3,:),'MarkerSize',5);hold on
    plot(L.t(:)-im_t(1),L.licktimesFN(:),'-','Color',cc(4,:),'MarkerSize',5);hold on
    legend({'TP','TN','FP','FN'})
    set(gca,'FontName','Calibri','FontSize',20)
    drawnow
    %}
    
    L=[];
    L.fit_f=fit_f;
    L.im_t=im_t;
    L.im_Lfit=im_Lfit;
    L.im_id=im_id;
    L.t_audio=t_audio;
    L.t_stimaudio=t_stimaudio;
    L.t_stimonly=t_stimonly;
    L.t_inflection=t_inflection;
    L.t_startlearn=t_startlearn2;
    L.t_endlearn=t_endlearn2;
else
    s=fittype('a+(b)/(1+exp((mu-x)/t))');
    L.im_t=4*(1:numel(timepoint))';
    L.im_Lfit=0.5*ones(size(L.im_t));
    L.fit_f=fit(L.im_t,L.im_Lfit,s,'StartPoint',[0.5,0.5,8,4],'Lower',[0 0 4 0],'Upper',[1 1 12 inf]);
    L.im_id=timepoint';
    L.t_audio=4;
    L.t_stimaudio=4;
    L.t_stimonly=4;
    L.t_inflection=8;
    L.t_startlearn=8;
    L.t_endlearn=8;
end


end
