%This script can be used to fit profiles without the GUI. 

%{
animal='DL083';
section={'001','002'};
timepoints=cellstr(char(double('B'):double('N'))')';
ch='G';
axon={[1,2,3,5,7,8,9,10,11,12,14,15,16,17,18,19,21,23,24,25,26,27,28,29,30,31],[1,2,3,4,5,6,7,8,9,10]};

animal='DL085';
section={'001'};
timepoints=cellstr(char(double('C'):double('T'))')';
ch='G';
axon={[4,13]};

animal='DL101';
section={'008'};
timepoints=cellstr(char(double('B'):double('S'))')';
ch='G';
axon={[20,21,22,23]};

animal='DL088';
section={'005'};
timepoints=cellstr(char(double('B'):double('Q'))')';
ch='G';
axon={[1,2,3,4,5,6,7,8,9,10,11,12]};
%}

pathlist;
for se=1:numel(section)
    for ax=1:numel(axon{se})
        
        Time=cell(numel(timepoints),1);
        axonstr=sprintf('A%0.3d',axon{se}(ax));
        for ti=1:numel(timepoints)
            fname=[profile_pth,[animal,timepoints{ti},section{se}],filesep,axonstr];
            Time{ti}=load(fname);
        end
        
        for ti=1:numel(timepoints)
            
            Dat=Time{ti};
            Dat.I.(ch).LoGxy.norm=...
                Dat.I.(ch).LoGxy.raw./mean((Dat.I.(ch).LoGxy.raw(~Dat.annotate.ignore)));
            
            %Normalize Gauss intensity excluding ignored regions
            Dat.I.(ch).Gauss.norm=...
                Dat.I.(ch).Gauss.raw./mean((Dat.I.(ch).Gauss.raw(~Dat.annotate.ignore)));
            
            %Fit LoGxy profile
            Dat=fitLoGxy_v1(Dat,ch);
            
            %Fit Gauss based on LoGxy peaks
            [Dat.fit.(ch).Gauss.fg,...
                Dat.fit.(ch).Gauss.bg]=fitGauss_v1(...
                Dat.I.(ch).Gauss.norm,...
                Dat.d.optim,...
                Dat.fit.(ch).LoGxy.fg,...
                Dat.fit.(ch).LoGxy.bg);
            
            Time{ti}=Dat;
            clear Dat;
        end
        
        Time2=automatch(Time,ch);
        for ti=1:numel(Time2)
            Dat=Time2{ti};
            fname=[profile_pth,[animal,timepoints{ti},section{se}],filesep,axonstr];
            save(fname,'-struct','Dat')
            clear Dat
        end
        clear Time Time2;
    end
end
