hf=findobj(0,'Type','Figure');
for i=1:numel(hf)
    ha=findobj(hf(i),'Type','Axes');
    if contains(ha.Title.String,'Added Fraction') || contains(ha.Title.String,'Eliminated Fraction')
        ha.CLim=[0.1 0.3];
    elseif contains(ha.Title.String,'Turnover')
        ha.CLim=[0.2 0.5];
    elseif contains(ha.Title.String,'Added Weight') || contains(ha.Title.String,'Eliminated Weight')
        ha.CLim=[1.5 3.2];
    elseif contains(ha.Title.String,'Depressed Fraction') || contains(ha.Title.String,'Potentiated Fraction')
        ha.CLim=[0.25 0.5];
    elseif contains(ha.Title.String,'Potentiation') || contains(ha.Title.String,'Depression')
        ha.CLim=[1.5 3.2];
    end
end

drawnow;
hf=findobj(0,'Type','Figure');
for i=1:numel(hf)
    ha=findobj(hf(i),'Type','Axes');
    pathlist;
    ha.Title.String=[ha.Title.String,'-',hf(i).Name];
    print(hf(i),'-dpng',[res_pth,'Maps-',ha.Title.String,'.png']);
    savefig(hf(i),[res_pth,'Maps-',ha.Title.String,'.fig'])
end

