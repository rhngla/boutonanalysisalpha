%Input is manual .swc traces
%Output is optimized .swc traces
pathlist;
paramset;
animal={'DL001'};
%timepoint=cellstr(char(double('B'):double('G'))');
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20]};%[1,3,4,10,11,14,15,16,17,18,19,20,21,22,23];
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';

%Handles to errorlogs
if ~exist(isunixispc([err_pth,'run_optim.txt']), 'file')
    display('No error log present, creating log')
    fid=fopen([err_pth,'run_optim.txt'],'wt+');fclose(fid);
end

for an=1:numel(animal)
    for se=1:numel(section)
        for ti=1:numel(timepoint)
            stackid=[animal{an},timepoint{ti},section{se}];
            %Load stack
            if exist(isunixispc([im_pth,stackid,'G.mat']),'file')
                clear Original;
                load(isunixispc([im_pth,stackid,'G.mat']),'Original');
                
                %Check/create optimized trace destination directory
                if ~exist(isunixispc([optim_pth,stackid]),'dir')
                    mkdir(isunixispc(optim_pth),stackid)
                    display(['Creating new directory',isunixispc([optim_pth,stackid])])
                end
                
                for ax=1:numel(axon{se})
                    AM=[]; r=[]; R=[];
                    %Optimize traces by different users simultaneously (for controls)
                    for tr=1:numel(tracer)
                        axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
                        
                        fname=isunixispc([man_pth,stackid,'/',axonstr,'.swc']);
                        [AM1,r1,R1]=swc2AM(fname);
                        AM1=AM1~=0;%Each trace is expected to be single tree
                        if numel(tracer)>1
                            display([axonstr,' - ',num2str(tr)])
                        end
                        
                        AM=blkdiag(AM,AM1*tr);
                        r=[r;r1];
                        R=[R;R1];
                    end
                    
                    %Optimize; parameters are loaded from file 'paramset.m'
                    [AM_opt,r_opt,R_opt,~,exitcount]=Optimize_Trace(Original,AM,r,R, ...
                        opt.Rtypical,opt.Optimize_bps,opt.Optimize_tps,opt.pointsperum,opt.MaxIterations, ...
                        opt.alpha_r,opt.alpha_R,opt.betta_r,opt.betta_R,opt.adjustPPM,opt.output);
                    
                    %Write individual optimized .swc trace files after separating
                    for tr=1:numel(tracer)
                        axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
                        
                        [ind1,ind2]=find(AM_opt==tr);
                        ind=unique([ind1;ind2]);
                        AM_sav=AM_opt(ind,:);AM_sav=AM_sav(:,ind);
                        r_sav=r_opt(ind,:);
                        R_sav=R_opt(ind,:);
                        
                        [AM_sav,r_sav,R_sav] = AdjustPPM(AM_sav,r_sav,R_sav,profile.ppm);
                        swcmat=AM2swc(AM_sav,r_sav,R_sav,1,1,1);
                        
                        fname=isunixispc([optim_pth,stackid,'/',axonstr,'.swc']);
                        dlmwrite(fname,swcmat,'delimiter',' ');
                    end 
                    msg=([stackid,'-',axonstr,' - count at exit: ',num2str(exitcount)]);
                    [~,sysname]=system('hostname');
                    dt=datestr(datetime('now'),'dd-mm-yy');
                    fid=fopen(isunixispc([err_pth,'run_optim.txt']),'a');
                    fprintf(fid,['\n',msg,' ',sysname(1:3),' ',dt]);
                    fclose(fid);
                end
            else
                msg=([stackid,' - Image stack not found!']);
                [~,sysname]=system('hostname');
                dt=datestr(datetime('now'),'dd-mm-yy');
                fid=fopen(isunixispc([err_pth,'run_optim.txt']),'a');
                fprintf(fid,['\n',msg,' ',sysname(1:3),' ',dt]);
                fclose(fid);
            end
        end
    end
end