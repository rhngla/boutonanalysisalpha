function M=analysis_3statemodelcalc(D)
%This code calculates created, eliminated, recently created and recently
%eliminated fractions to fit with the three satate model.
%{
M83=analysis_3statemodelcalc(D83);
M85=analysis_3statemodelcalc(D85);
M101=analysis_3statemodelcalc(D101);
M102=analysis_3statemodelcalc(D102);
M88=analysis_3statemodelcalc(D88);

fieldnm={'Ncreated_data','Nelim_data','Nrecentlycreated_data','Nrecentlyelim_data'};
pos={'northwest','northeast','southwest','southeast'};
for f=1:numel(fieldnm)
    h=figure,hold on;
    movegui(h,pos{f});```
    fd=fieldnm{f};
    plot(M83.(fd),'-*')
    plot(M85.(fd),'-*')
    plot(M101.(fd),'-*')
    plot(M102.(fd),'-*')
    plot(M88.(fd),'-*')
    axis square,box on
    ylabel(fd)
    drawnow;
end
%}

pathlist;
avgflag=false;
%--------------------------------------------------------------------------
%Block for normalization below is the same as in analysis_maindataset
filter='LoGxy';
w=D.G.(filter).amp+D.G.(filter).Ibg;
w(isnan(w))=D.G.(filter).Inorm(isnan(w));
normtype='gauss_norm';
axlbl=unique(D.G.(filter).ax_id,'stable');
ax_id=D.G.(filter).ax_id;
Nf=ones(size(D.G.(filter).amp,1),numel(axlbl));
ax_length=ones(size(D.G.(filter).amp,1),numel(axlbl));

for ax=1:numel(axlbl)
    ax_id(ax_id==axlbl(ax))=ax;
    for ti=1:size(D.G.(filter).norm,1)
        if ~strcmp(normtype,'none')
            Nf(ti,ax)=D.G.(filter).norm{ti,ax}.(normtype);
        end
        ax_length(ti,ax)=D.G.(filter).norm{ti,ax}.axlen_legitimate;
    end
end

Nf=mean(Nf,1);
Nf=bsxfun(@times,ones(size(D.G.(filter).amp,1),1),Nf);

for ax=1:numel(axlbl)
    ax_id(ax_id==axlbl(ax))=ax;
    for ti=1:size(D.G.(filter).norm,1)
        w(ti,ax_id==ax)=w(ti,ax_id==ax)./Nf(ti,ax);
    end
end
D.G.LoGxy.Ifg=w;

%alpha and wthr for probabilistic calculations
global wthr;
wthr=2;
global alpha
alpha=0.2389;

keep=any(fP(w)>0,1);
Dat.w=w(:,keep);
Dat.P=fP(Dat.w);
Dat.L=D.L;

%Restrict analysis to particular phase of experiment-----------------------
poi=nan(5,1);
poi(1)=find((Dat.L.im_t-Dat.L.t_stimaudio)>0,1,'first');
poi(2)=find((Dat.L.im_t-Dat.L.t_stimonly)>0,1,'first');
poi(3)=find(Dat.L.fit_f(Dat.L.im_t)>Dat.L.fit_f(Dat.L.fit_f.mu-2*Dat.L.fit_f.t),1,'first');
poi(4)=find((Dat.L.im_t-Dat.L.t_startlearn)>0,1,'first');
poi(5)=find((Dat.L.im_t-Dat.L.t_inflection)>0,1,'first');

allowedtimes=(poi(1)+1):size(Dat.P,1);
Dat.P=Dat.P(allowedtimes,:);

%Fit only part of the data-------------------------------------------------
fitind=2:size(Dat.P,1);

%All experimental measures:------------------------------------------------
ntimes=size(Dat.P,1);
Ncreated_data=nan(ntimes,1);Ecreated_data=nan(ntimes,1);
Ncreated_mat=nan(ntimes);Ecreated_mat=nan(ntimes);
Ncreated_exact=nan(ntimes,1);

Nrecentlycreated_data=nan(ntimes,1);Erecentlycreated_data=nan(ntimes,1);
Nrecentlycreated_mat=nan(ntimes);Erecentlycreated_mat=nan(ntimes);
Nrecentlycreated_exact=nan(ntimes,1);

Nelim_data=nan(ntimes,1);Eelim_data=nan(ntimes,1);
Nelim_mat=nan(ntimes);Eelim_mat=nan(ntimes);
Nelim_exact=nan(ntimes,1);

Nrecentlyelim_data=nan(ntimes,1);Erecentlyelim_data=nan(ntimes,1);
Nrecentlyelim_mat=nan(ntimes);Erecentlyelim_mat=nan(ntimes);
Nrecentlyelim_exact=nan(ntimes,1);

for startstate=1:(ntimes-1)
    p_cut=Dat.P(startstate:end,:);
    %Created
    for n=1:(size(p_cut)-1)
        p_created=p_cut(n+1,:).*(1-p_cut(1,:));
        Ncreated_mat(startstate,n+1)=sum(p_created);
        Ecreated_mat(startstate,n+1)=(sum(p_created.*(1-p_created)))^0.5;
    end
    
    %Eliminated
    for n=1:(size(p_cut)-1)
        p_elim=(1-p_cut(n+1,:)).*p_cut(1,:);
        Nelim_mat(startstate,n+1)=sum(p_elim);
        Eelim_mat(startstate,n+1)=(sum(p_elim.*(1-p_elim)))^0.5;
    end
    
    %Recently created
    for n=1:(size(p_cut)-1)
        p_recentlycreated=prod(1-p_cut(1:n,:),1).*p_cut(n+1,:);
        Nrecentlycreated_mat(startstate,n+1)=sum(p_recentlycreated);
        Erecentlycreated_mat(startstate,n+1)=(sum(p_recentlycreated.*(1-p_recentlycreated)))^0.5;
    end
    
    %Recently Eliminated
    for n=1:(size(p_cut,1)-1)
        p_recentlyelim=prod(p_cut(1:n,:),1).*(1-p_cut(n+1,:));
        Nrecentlyelim_mat(startstate,n+1)=sum(p_recentlyelim);
        Erecentlyelim_mat(startstate,n+1)=(sum(p_recentlyelim.*(1-p_recentlyelim))).^0.5;
    end
end

if avgflag
    Ncreated_data(:)=nanmean(Ncreated_mat,1);
    Nrecentlycreated_data(:)=nanmean(Nrecentlycreated_mat,1);
    Nelim_data(:)=nanmean(Nelim_mat,1);
    Nrecentlyelim_data(:)=nanmean(Nrecentlyelim_mat,1);
    Nb=mean(sum(Dat.P(1:end-1,:),2));
    
    Ecreated_data(:)=nanmean(Ecreated_mat,1)./(sum(~isnan(Ecreated_mat),1)).^0.5./Nb;
    Erecentlycreated_data(:)=nanmean(Erecentlycreated_mat,1)./(sum(~isnan(Erecentlycreated_mat),1)).^0.5./Nb;
    Eelim_data(:)=nanmean(Eelim_mat,1)./(sum(~isnan(Eelim_mat),1)).^0.5./Nb;
    Erecentlyelim_data(:)=nanmean(Erecentlyelim_mat,1)./(sum(~isnan(Erecentlyelim_mat),1)).^0.5./Nb;
    
else
    Ncreated_data(:)=Ncreated_mat(1,:);
    Nrecentlycreated_data(:)=Nrecentlycreated_mat(1,:);
    Nelim_data(:)=Nelim_mat(1,:);
    Nrecentlyelim_data(:)=Nrecentlyelim_mat(1,:);
    Nb=sum(Dat.P(1,:));
    
    Ecreated_data(:)=Ecreated_mat(1,:)./Nb;
    Erecentlycreated_data(:)=Erecentlycreated_mat(1,:)./Nb;
    Eelim_data(:)=Eelim_mat(1,:)./Nb;
    Erecentlyelim_data(:)=Erecentlyelim_mat(1,:)./Nb;
end

Ncreated_data=Ncreated_data./Nb;
Nrecentlycreated_data=Nrecentlycreated_data./Nb;
Nelim_data=Nelim_data./Nb;
Nrecentlyelim_data=Nrecentlyelim_data./Nb;

n00=sum(sum((1-Dat.P(1:end-1,:)).*(1-Dat.P(2:end,:)))); %n(0<-0)
n01=sum(sum(Dat.P(1:end-1,:).*(1-Dat.P(2:end,:))));     %n(0<-1)
n10=sum(sum((1-Dat.P(1:end-1,:)).*Dat.P(2:end,:)));     %n(1<-0)
n11=sum(sum(Dat.P(1:end-1,:).*Dat.P(2:end,:)));         %n(1<-1)

%Homeostasis related-------------------------------------------------------
n01=(n01+n10)./2;
n10=n01;

%Outputs-------------------------------------------------------------------
M.avgflag=avgflag;
M.allowedtimes=allowedtimes;
M.fitind=fitind;

M.Ncreated_data=Ncreated_data;
M.Nrecentlycreated_data=Nrecentlycreated_data;
M.Nrecentlyelim_data=Nrecentlyelim_data;
M.Nelim_data=Nelim_data;

M.Ecreated_data=Ecreated_data;
M.Erecentlycreated_data=Erecentlycreated_data;
M.Erecentlyelim_data=Erecentlyelim_data;
M.Eelim_data=Eelim_data;

M.n00=n00;
M.n01=n01;
M.n10=n10;
M.n11=n11;

%save([path_3state,'M',num2str(D.anid)],'-struct','M');
%disp(['File saved: ',path_3state,'M',num2str(D.anid)]);


end
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

function P=fP(varargin)
global alpha;
global wthr;
w=varargin{1};
if nargin==2
    thr=varargin{2};
else
    thr=wthr;
end
P=nan(size(w));
w(w<eps)=abs(eps);
P(:)=0.5*(1+erf((w(:)-thr)./((alpha*w(:)).^0.5)));
end
