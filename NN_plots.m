%{
%Custom script run within
%/Applications/MATLAB_R2017b.app/toolbox/nnet/cnn/+nnet/+internal/+cnn/Trainer.m
%/usr/local/MATLAB/R2017b/toolbox/nnet/cnn/+nnet/+internal/+cnn
%for visualizing weights during training.

if ~exist('hf','var')
    hf = figure(999);
    ha = axes(hf);
else
    if ~isempty(ha.Children)
        %Update the plot handle XData and YData values
    else
        %Plot weights for the first time here
    end
end
%}