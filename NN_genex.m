function [IM,lbl,layers,options]=NN_genex(L,NL)
k=50;
n_examples=10000;
nfeatures=1;
t_l=1:10;
t_nl=1:10;
IM=zeros(numel(t_l),k,1,n_examples);

lbl=nan(n_examples,1);
for i=1:n_examples
    if mod(i,2)==0
        ind=randperm(size(L,2),k);
        IM(:,:,1,i)=L(t_l,ind);
        lbl(i)=1;
    else
        ind=randperm(size(NL,2),k);
        IM(:,:,1,i)=NL(t_nl,ind);
        lbl(i)=0;
    end
end
lbl=categorical(lbl);

layers = [
    imageInputLayer([size(IM,1) k 1],'Normalization','none')
    convolution2dLayer([size(IM,1),1],nfeatures,'Stride',1);
    clippedReluLayer(1)
    averagePooling2dLayer([1 k],'Stride',1)
    fullyConnectedLayer(2)
    softmaxLayer()
    classificationLayer];

%Weight initialization
layers(2).Weights=single(0.1*rand(size(IM,1),nfeatures));
layers(2).Bias=single(0.5*ones(nfeatures,1));

options = trainingOptions('sgdm',...
    'Momentum',0,...
    'MaxEpochs',200, ...
    'Verbose',true,...
    'VerboseFrequency',100,...
    'LearnRateSchedule','piecewise',...
    'LearnRateDropFactor',1,...
    'ExecutionEnvironment','auto',...
    'Plots','none',...
    'MiniBatchSize',1);
end