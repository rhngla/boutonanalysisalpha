function [D]=analysis_processdat(D)
%This function cuts the D matrix
%All fields other than norm have second dimension = n(boutons)

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
flagpth=isunixispc([parentdir,'/dat/Axon-Sheet3.mat']);
Flag=load(flagpth);
Flag=Flag.(['A',num2str(D.anid)]);

alltimes=cellstr(char(double('B'):double('T'))');
dattimes=D.L.im_id;
keeptimes=find(ismember(alltimes,dattimes))+1; %The +1 is to ignore the column with axon identies in the Flag matrix
Flagax=Flag(:,1);
Flagdat=Flag(:,keeptimes);

%Condition for removing axons:
remax=sum(Flagdat==0,2)>0 | sum(Flagdat==2,2)>1 | sum(Flagdat==4,2)>0;
Flagdat=Flagdat(~remax,:);
Flagax=Flagax(~remax,:);

%Cutting D
keep=ismember(D.G.LoGxy.ax_id,Flagax);
D=cut_D(D,keep);
end

function [D]=cut_D(D,keep)
%This function cuts the D matrix
%All fields other than norm have second dimension = n(boutons)
if isempty(keep)
    %All boutons retained if keep is empty
    keep=true(size(D.G.LoGxy.ax_id));
end

ax_orig=unique(D.G.LoGxy.ax_id);
fnames=fieldnames(D.G.LoGxy);
[~,ind]=ismember('norm',fnames);
fnames(ind)=[];
for f=1:numel(fnames)
    D.G.LoGxy.(fnames{f})=D.G.LoGxy.(fnames{f})(:,keep);
end

lia=ismember(ax_orig,unique(D.G.LoGxy.ax_id));
D.G.LoGxy.norm=D.G.LoGxy.norm(:,lia);
end