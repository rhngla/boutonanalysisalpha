%Loads swc and initializes profile with fields:
%AM.optim
%r.optim
%annotate.ignore
%id
%Resulting profile will be edited by annotate_optimized.m

pathlist;

animal={'DL083'};
%timepoint=cellstr([char(double('K'):double('T'))]');
timepoint={'B'};
section={'001'};
axon={[99]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';

for an=1:numel(animal)
    for se=1:numel(section)
        for ti=1:numel(timepoint)
            stackid=[animal{an},timepoint{ti},section{se}];
            for ax=1:numel(axon{se})
                AM=[]; r=[]; R=[];
                for tr=1:numel(tracer)
                    axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
                    
                    fname=isunixispc([optim_pth,stackid,'/',axonstr,'.swc']);
                    if exist(fname,'file')
                        [AM.optim,r.optim,~]=swc2AM(fname);
                        id=[stackid,'-',axonstr];
                        annotate.ignore=false(size(r.optim,1),1);
                        if ~exist(isunixispc([profile_pth,stackid]),'dir')
                            mkdir(isunixispc([profile_pth]),stackid)
                            display(['Creating new directory',isunixispc([profile_pth,stackid])])
                        end
                        save(isunixispc([profile_pth,stackid,'/',axonstr]),'AM','r','annotate','id')
                        display(['Saved ',isunixispc([profile_pth,stackid,'/',axonstr])]);
                    else
                        display([fname,' not found!'])
                    end
                end
            end
        end
    end
end

