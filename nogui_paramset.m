%Parameter controling size of tube to remove background around axon used in gui_optimization.m
params.proj.fm_dist=4;                         %default: proj.fm_dist=4                    (Image intensity beyond this number of voxels in 
                                               %                                             3D is set to zero for visualizing axon within tube)                                                         
%Trace optimization parameters
params.opt.Rtypical=3;                         %default: opt.Rtypical=3
params.opt.Optimize_bps=0;                     %default: opt.Optimize_bps=0                (true/false value)
params.opt.Optimize_tps=0;                     %default: opt.Optimize_tps=0                (true/false value)
params.opt.isadjustpointdensity=1;             %default: opt.isadjustpointdensity=1;       (true/false value)
params.opt.pointspervoxel=0.5;                 %default: opt.pointspervoxel=0.5
params.opt.MaxIterations=2000;                 %default: opt.MaxIterations=2000;
params.opt.alpha_r=0.001;                      %default: opt.alpha_r=0.001
params.opt.betta_r=10;                         %default: opt.betta_r=10
params.opt.output=1;                           %default: opt.output=1;                     (true/false value)

%Parameters related to generation of profiles
params.profile.pointspervoxel=4;               %default: profile.pointspervoxel=4;
params.profile.umpervox=[0.26,0.26,0.80];      %default: profile.umpervox=[0.26,0.26,0.80] (Image resolution)
params.filt.types={'LoGxy','Gauss'};           %default: filt.types={'LoGxy','Gauss'}
params.filt.LoGxy_R_min=1.5;                   %default: filt.LoGxy_R_min=1.5
params.filt.LoGxy_R_step=0.01;                 %default: filt.LoGxy_R_step=0.01
params.filt.LoGxy_R_max=3;                     %default: filt.LoGxy_R_max=3
params.filt.LoGxy_Rz=2;                        %default: filt.LoGxy_Rz=2
params.filt.Gauss_R=2;                         %default: filt.Gauss_R=2