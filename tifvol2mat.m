%This script creates separate NCTracer library files from a tif volume in
%which green and red channel data is interleaved.

%File selection:
animal='DL108';
section=[8];
timepoint={'K'};
load_pth='F:\Rohan\RawImages\';
save_pth=('F:\Rohan\RawImages\');

AM=[];r=[];R=[];
reduction_x=2;reduction_y=1;reduction_z=1;reduct_method='Mean';
for t=1:length(timepoint)
    for s=1:length(section)
        dataset_id=[animal,timepoint{t},'00',num2str(section(s))];
        FileTif=[load_pth,dataset_id,'.tif'];
        if exist(FileTif,'file')
            %Load tif to matlab array
            InfoImage=imfinfo(FileTif);
            mImage=InfoImage(1).Width;
            nImage=InfoImage(1).Height;
            NumberImages=length(InfoImage);
            GR=zeros(nImage,mImage,NumberImages,'uint16');
            TifLink = Tiff(FileTif, 'r');
            for ni=1:NumberImages
                TifLink.setDirectory(ni);
                GR(:,:,ni)=TifLink.read();
            end
            TifLink.close();
            
            %reduce
            GR=Reduce_Stack(GR,reduction_x,reduction_y,reduction_z,reduct_method);
            
            %split into green and save
            Original=GR(:,:,1:2:end);
            IM=Original;
            save([save_pth,dataset_id,'G.mat'],'Original','IM','AM','r','R','reduction_x','reduction_y','reduction_z');
            
            %split into red and save
            Original=GR(:,:,2:2:end);
            IM=Original;
            save([save_pth,dataset_id,'R.mat'],'Original','IM','AM','r','R','reduction_x','reduction_y','reduction_z');
        end
    end
end

%{
%Original script:
FileTif='xyz.tif';
InfoImage=imfinfo(FileTif);
mImage=InfoImage(1).Width;
nImage=InfoImage(1).Height;
NumberImages=length(InfoImage);
Original=zeros(nImage,mImage,NumberImages,'uint16');
 
TifLink = Tiff(FileTif, 'r');
for t=1:NumberImages
   TifLink.setDirectory(t);
   Original(:,:,t)=TifLink.read();
end
TifLink.close();
%}