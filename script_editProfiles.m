maindir='F:\Rohan\DL083\';
profiledir=dir(maindir);profiledir=[{profiledir.name}'];
keep=regexp(profiledir,'(DL083[B-N]001)');

keep=find(~cellfun(@(x) isempty(x),keep));
for k=1:numel(keep)
    filelist=dir([maindir,filesep,profiledir{keep(k)}]);
    filelist=[{filelist.name}'];
    keepfile=regexp(filelist,'(A0)');
    keepfile=find(~cellfun(@(x) isempty(x),keepfile));
    for f=1:numel(keepfile)
        fname=[maindir,filesep,profiledir{keep(k)},filesep,filelist{keepfile(f)}];
        Profile=load(fname);
        save(fname,'-struct','Profile');
        display(['Saved profile in ',fname]);
        clear Profile;
    end
end