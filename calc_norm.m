function [An]=calc_norm(An)
%This code calculates axon lengths factors for individual axons for
%profiles created using the gui in release.

Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
filter={'LoGxy'};
for se=1:numel(An.Section)
    for ax=1:numel(An.Section{se}.Time{1}.Axon)
        for f=1:numel(filter)
            dmin=-inf;
            dmax=inf;
            for ti=1:numel(An.Section{se}.Time)
                Dat=An.Section{se}.Time{ti}.Axon{ax};
                dmin=max([dmin,min(Dat.fit.G.(filter{f}).d.man)]);
                dmax=min([dmax,max(Dat.fit.G.(filter{f}).d.man)]);
            end
            
            %Calculate union of ignored regions over all sessions, based on
            %manually aligned distances
            digstartlist=[];
            digendlist=[];
            for ti=1:numel(An.Section{se}.Time)
                Dat=An.Section{se}.Time{ti}.Axon{ax};
                igstart=find(diff(Dat.annotate.ignore)==1)+1;
                igend=find(diff(Dat.annotate.ignore)==-1);
                igstart=igstart(:);
                igend=igend(:);
                if Dat.annotate.ignore(1)==1
                    igstart=[1;igstart];
                end
                if Dat.annotate.ignore(end)==1
                    igend=[igend;numel(Dat.annotate.ignore)];
                end
                digstartlist=[digstartlist;Dat.fit.G.(filter{f}).d.man(igstart(:))];
                digendlist=[digendlist;Dat.fit.G.(filter{f}).d.man(igend(:))];
            end
            
            %Calculate background around boutons present in
            %all times
            for ti=1:numel(An.Section{se}.Time)
                Dat=An.Section{se}.Time{ti}.Axon{ax};
                
                %1: Remove regions from backbone based on present time only
                backbone=~Dat.annotate.ignore;
                backbone(Dat.fit.G.(filter{f}).d.man<dmin | Dat.fit.G.(filter{f}).d.man>dmax)=false;
                
                %2: Remove regions ignored at any time from current backbone
                for jj=1:numel(digstartlist)
                   backbone(Dat.fit.G.(filter{f}).d.man>digstartlist(jj) & Dat.fit.G.(filter{f}).d.man<digendlist(jj))=false;
                end
                
                %3: Calculate fitted background in Gaussian profile(s)
                Gauss_bg=zeros(size(backbone));
                for i=1:numel(Dat.fit.G.('Gauss').bg.ind)
                    Gauss_bg=Gauss_bg+Ff(Dat.d.optim,Dat.fit.G.('Gauss').bg.amp(i),Dat.fit.G.('Gauss').bg.mu(i),Dat.fit.G.('Gauss').bg.sig(i));
                end
                
                %Length of axon corresponding to the remaining backbone
                startt=find(diff(backbone)==1)+1;
                endd=find(diff(backbone)==-1)+1;
                startt=startt(:);
                endd=endd(:);
                if backbone(1)==1
                    startt=[1;startt];
                end
                if backbone(end)==1
                    endd=[endd;numel(backbone)];
                end
                axlen=Dat.d.optim(endd)-Dat.d.optim(startt);
                axlen=sum(axlen);
                %An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).gauss_norm=mean(Gauss_bg(~Dat.annotate.ignore));
                %disp([mean(Gauss_bg(backbone)),mean(Gauss_bg(~Dat.annotate.ignore))]);%Negligible
                %difference between means based on backbone or
                %~annotate.ignore
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).gauss_norm=mean(Gauss_bg(backbone));
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).axlen_legitimate=axlen;
            end
        end
    end
end

