function [h] = fP_handles()
%This function performs probabilistic calculations for the bouton dataset.
%The parameters for this function are located in analysis_params.m

h.P=@fP;
h.P_add=@fPadded;
h.P_elim=@fPeliminated;
h.P_pot=@fPpot;
h.P_dep=@fPdep;
h.P_change=@fPchange;
h.P_noise=@fdw;
end

function Prob=fP(varargin)
analysis_params;
w=varargin{1};
if nargin==2
    thr=varargin{2};
else
    thr=wthr;
end
Prob=nan(size(w));
w(w<eps)=abs(eps);
Prob(:)=0.5*(1+erf((w(:)-thr)./((alph*w(:)).^0.5)));
end

function dw=fdw(w)
%Standard deviation of noise
analysis_params;
dw=((1/2).*alph.*w).^0.5;
end

function Padded=fPadded(wi,wf)
Padded=(1-fP(wi)).*(fP(wf));
end

function Peliminated=fPeliminated(wi,wf)
Peliminated=(fP(wi)).*(1-fP(wf));
end

function Ppot=fPpot(wi,wf)
analysis_params;
wi(wi<eps)=abs(eps);
wf(wf<eps)=abs(eps);
Ppot=fP(wi).*fP(wf).*(0.5.*(1+erf((wf-wi)./((alph.*(wi+wf)).^0.5))));
end

function Pdep=fPdep(wi,wf)
analysis_params;
wi(wi<eps)=abs(eps);
wf(wf<eps)=abs(eps);
Pdep=fP(wi).*fP(wf).*(0.5.*(1+erf((wi-wf)./((alph.*(wi+wf)).^0.5))));
end

function Pchange=fPchange(wi,wf)
analysis_params;
wi(wi<eps)=abs(eps);
wf(wf<eps)=abs(eps);
Pchange=(0.5.*(1+erf(abs(wf-wi)./((alph.*(wi+wf)).^0.5))));
end