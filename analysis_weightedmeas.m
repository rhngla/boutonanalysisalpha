%This script constructs weighted measure for correlations
clear O;
%{
%--------------------------------------------------------------------------
analysis_type='SpatialCorr';
O{1}=analysis_maindataset(A83,d83,full_dataset,analysis_type);
O{2}=analysis_maindataset(A101,d101,full_dataset,analysis_type);
O{3}=analysis_maindataset(A102,d102,full_dataset,analysis_type);
O{4}=analysis_maindataset(A88,d88,full_dataset,analysis_type);
%Animals with less data:
O{5}=analysis_maindataset(A85,d85,full_dataset,analysis_type);
O{6}=analysis_maindataset(A108,d108,full_dataset,analysis_type);
O{7}=analysis_maindataset(A68,d68,full_dataset,analysis_type);
O{8}=analysis_maindataset(A89,d89,full_dataset,analysis_type);

hh=gcf;
ha=gca;
set(findobj(hh,'Type','ErrorBar'),'LineWidth',2);
set(findobj(hh,'Type','Line'),'LineWidth',2);
ylim([-0.25 0.25])
grid on
drawnow
pathlist;
print(gcf,'-dpng',[res_pth,'Spatial correlation of weights - All animals.png']);
savefig(gcf,[res_pth,'Spatial correlation of weights - All animals.fig'])

%Weighted spatial correlations
counts=nan(20,numel(O));
xx=nan(20,numel(O));
yy=nan(20,numel(O));
ee=nan(20,numel(O));
for a=1:numel(O)
    inds=(1:numel(O{a}.counts(:)))';
    counts(inds,a)=O{a}.counts(:);
    xx(inds,a)=O{a}.xx(:);
    yy(inds,a)=O{a}.yy(:);
    ee(inds,a)=O{a}.ee(:);
end
X=sum(xx.*counts,2)./sum(counts,2);
Y=sum(yy.*counts,2)./sum(counts,2);
E=(sum(ee.^2.*counts,2)./sum(counts,2)).^0.5;

hf=figure;copyobj(ha,hf);
delete(hf.Children.Children);drawnow;
errorbar(X,Y,E,'Color',[0 0 0]);

%For bootstraps:
counts=nan(20,numel(O));
xx=nan(20,numel(O));
yy=nan(20,numel(O));
ee=nan(20,numel(O));
for a=1:numel(O)
    inds=(1:numel(O{a}.bscounts(:)))';
    counts(inds,a)=O{a}.bscounts(:);
    xx(inds,a)=O{a}.bsxx(:);
    yy(inds,a)=O{a}.bsyy(:);
end
X=sum(xx.*counts,2)./sum(counts,2);
Y=sum(yy.*counts,2)./sum(counts,2);
plot(X,Y,'--','Color',[0 0 0]);

set(findobj(hf,'Type','ErrorBar'),'LineWidth',2);
set(findobj(hf,'Type','Line'),'LineWidth',2);
grid on

print(gcf,'-dpng',[res_pth,'Spatial correlation of weights - Averaged.png']);
savefig(gcf,[res_pth,'Spatial correlation of weights - Averaged.fig'])
%--------------------------------------------------------------------------
%}

%Weighted temporal correlation
%--------------------------------------------------------------------------
analysis_type='TemporalCorr';
%Animals with more than 10 time points
O{1}=analysis_maindataset(A83,d83,full_dataset,analysis_type);
O{2}=analysis_maindataset(A101,d101,full_dataset,analysis_type);
O{3}=analysis_maindataset(A102,d102,full_dataset,analysis_type);
O{4}=analysis_maindataset(A88,d88,full_dataset,analysis_type);
O{5}=analysis_maindataset(A85,d85,full_dataset,analysis_type);

%Animals with less than 10 time points
O{6}=analysis_maindataset(A108,d108,full_dataset,analysis_type);
O{7}=analysis_maindataset(A68,d68,full_dataset,analysis_type);
O{8}=analysis_maindataset(A89,d89,full_dataset,analysis_type);

hh=gcf;
ha=gca;
set(findobj(hh,'Type','ErrorBar'),'LineWidth',2);
set(findobj(hh,'Type','Line'),'LineWidth',2);
grid on
drawnow
pathlist;
print(gcf,'-dpng',[res_pth,'Temporal correlation of weights - 10 timepoints or more.png']);
savefig(gcf,[res_pth,'Temporal correlation of weights - 10 timepoints or more.fig'])

%print(gcf,'-dpng',[res_pth,'Temporal correlation of weights - less than 10 timepoints.png']);
%savefig(gcf,[res_pth,'Temporal correlation of weights - less than 10 timepoints.fig'])

%Weighted spatial correlations
counts=nan(20,numel(O));
xx=nan(20,numel(O));
yy=nan(20,numel(O));
ee=nan(20,numel(O));
for a=1:5
    inds=(1:numel(O{a}.xx(:)))';
    xx(inds,a)=O{a}.xx(:);
    yy(inds,a)=O{a}.yy(:);
    ee(inds,a)=O{a}.ee(:);
    counts(inds,a)=O{a}.counts;
end

X=nansum(xx.*counts,2)./nansum(counts,2);
Y=nansum(yy.*counts,2)./nansum(counts,2);
E=(nansum(ee.^2.*counts,2)./nansum(counts,2)).^0.5;

hf=figure;copyobj(ha,hf);
delete(hf.Children.Children);drawnow;
errorbar(X,Y,E,'Color',[0 0 0]);

%For bootstraps:
%Dont have to calculate counts as long as number of bootstrap iterations
%are the same for each animal
xx=nan(20,numel(O));
yy=nan(20,numel(O));
ee=nan(20,numel(O));
for a=1:numel(O)
    inds=(1:numel(O{a}.bsxx(:)))';
    xx(inds,a)=O{a}.bsxx(:);
    yy(inds,a)=O{a}.bsyy(:);
end
X=nansum(xx.*counts,2)./nansum(counts,2);
Y=nansum(yy.*counts,2)./nansum(counts,2);
plot(X,Y,'--','Color',[0 0 0]);

set(findobj(hf,'Type','ErrorBar'),'LineWidth',2);
set(findobj(hf,'Type','Line'),'LineWidth',2);
grid on

print(gcf,'-dpng',[res_pth,'Temporal correlation of weights - Averaged.png']);
savefig(gcf,[res_pth,'Temporal correlation of weights - Averaged.fig'])
%--------------------------------------------------------------------------
%}
