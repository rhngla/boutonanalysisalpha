%This script creates data used by the neural network
%tp for analysis_loaddat is set to 2

%Learners:

A83=analysis_loaddat(83,2,1);
D83=analysis_getmat(A83);
d83=analysis_processdat(D83);
O83=analysis_editmatrix(d83);

A85=analysis_loaddat(85,2,1);
D85=analysis_getmat(A85);
d85=analysis_processdat(D85);
O85=analysis_editmatrix(d85);

A101=analysis_loaddat(101,2,1);
D101=analysis_getmat(A101);
d101=analysis_processdat(D101);
O101=analysis_editmatrix(d101);

%Non-learners:

A102=analysis_loaddat(102,2,1);
D102=analysis_getmat(A102);
d102=analysis_processdat(D102);
O102=analysis_editmatrix(d102);

A88=analysis_loaddat(88,2,1);
D88=analysis_getmat(A88);
d88=analysis_processdat(D88);
O88=analysis_editmatrix(d88);

%save('/Users/Fruity/Dropbox/Lab/Plasticity/BoutonAnalysis/dat/NN_Dat_v2.mat','O*');