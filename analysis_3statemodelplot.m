function [P,f,flm] = analysis_3statemodelplot(anid)
%Plots the results of analysis_3statemodelfit

pathlist;

allan=[83,85,101,102,88];
P=cell(length(anid),1);
id=nan(length(anid),1);
f=nan(length(anid),1);
flm=nan(length(anid),1);
for a=1:length(anid)
    anind=(allan==anid(a));
    anc=lines(numel(allan));
    anc=anc(anind,:);
    
    files=dir([path_3state,'Fits',filesep,num2str(anid(a)),'*']);
    chisqgrid=[];
    errorsq=inf;
    for i=1:length(files)
        A=load([path_3state,'Fits',filesep,files(i).name]);
        if A.errorsq<errorsq
            errorsq=A.errorsq;
            P{a}=A.bestP;
            flm(a)=A.bestflm;
            f(a)=A.bestf;
            id(a)=anid(a);
            chisqgrid=[chisqgrid;A.chisqgrid(:)];
            p01range=A.p01range;
            p10range=A.p10range;
            p12range=A.p12range;
            p21range=A.p21range;
            flmrange=A.flmrange;
            frange=A.frange;
        end
        
    end
    
    [ev,lambda] = eig(P{a});
    [~,ind]=min(abs(diag(lambda)-1));
    ev=ev(:,ind)';
    ev=ev./sum(ev);
    dist=[1-f(a),flm(a)*f(a),(1-flm(a))*f(a)];
    
    display('-------------------------------------------------------------')
    disp(['Animal ',num2str(anid(a))]);
    disp('P');
    disp(P{a});
    disp('flm');
    disp(flm(a));
    disp('f');
    disp(f(a));
    disp('distribution');
    disp(dist);
    disp('ev');
    disp(ev);
    disp('lambda');
    disp(lambda)
    display('-------------------------------------------------------------')
    %----------------------------------------------------------------------
    
    avgflag=A.M.avgflag;
    allowedtimes=A.M.allowedtimes;
    fitind=A.M.fitind;
    
    Ncreated_data=A.M.Ncreated_data(:);
    Nrecentlycreated_data=A.M.Nrecentlycreated_data(:);
    Nrecentlyelim_data=A.M.Nrecentlyelim_data(:);
    Nelim_data=A.M.Nelim_data(:);
    
    Ecreated_data=A.M.Ecreated_data(:);
    Erecentlycreated_data=A.M.Erecentlycreated_data(:);
    Erecentlyelim_data=A.M.Erecentlyelim_data(:);
    Eelim_data=A.M.Eelim_data(:);
    
    %----------------------------------------------------------------------
    %Calculate theoretical curves for best fit P,f,flm
    Z=eye(3);Z(1,1)=0;
    N1=flm(a);N2=1-flm(a);
    Pn=eye(3);
    PZ=(P{a}*Z);
    PZn=eye(3);
    Ncreated_exact=nan(numel(allowedtimes),1);
    Nelim_exact=nan(numel(allowedtimes),1);
    Nrecentlyelim_exact=nan(numel(allowedtimes),1);
    
    Nrecentlycreated_exact=((1-f(a))/f(a))*((1-P{a}(1,1))/P{a}(1,1))*(P{a}(1,1).^(0:(numel(allowedtimes)-1))');
    for n=0:(numel(allowedtimes)-1)
        Ncreated_exact(n+1)=((1-f(a))/f(a))*(Pn(2,1)+Pn(3,1));
        Nelim_exact(n+1)=Pn(1,2)*N1+Pn(1,3)*N2;
        Nrecentlyelim_exact(n+1)=PZn(1,2)*N1+PZn(1,3)*N2;
        Pn=Pn*P{a};
        PZn=PZn*(PZ);
    end
    
    %----------------------------------------------------------------------
    figure(1),hold on
    errorbar(fitind-1,Ncreated_data(fitind),Ecreated_data(fitind),'.','Color',anc,'MarkerSize',10,'MarkerEdgeColor','none')
    plot(fitind-1,Ncreated_data(fitind),'.','Color',anc,'MarkerSize',10)
    plot(fitind-1,Ncreated_exact(fitind),'--','Color',anc)
    M=CalcGoF(Ncreated_data(fitind),Ncreated_exact(fitind),Ecreated_data(fitind));
    text(15,0.40-0.025*a,num2str(M(1)),'FontSize',12,'Color',anc)
    ylabel('N_{created}')
    xlabel('Imaging session #')
    xlim([0 16]);ylim([0 0.3]);
    axis square,box on;
    set(gca,'FontName','Calibri','FontSize',15)
    drawnow;
    
    
    figure(2),hold on
    errorbar(fitind-1,Nrecentlycreated_data(fitind),Erecentlycreated_data(fitind),'.','Color',anc,'MarkerSize',10,'MarkerEdgeColor','none')
    plot(fitind-1,Nrecentlycreated_data(fitind),'.','Color',anc,'MarkerSize',10)
    plot(fitind-1,Nrecentlycreated_exact(fitind),'--','Color',anc)
    M=CalcGoF(Nrecentlycreated_data(fitind),Nrecentlycreated_exact(fitind),Erecentlycreated_data(fitind));
    text(15,0.30-0.025*a,num2str(M(1)),'FontSize',12,'Color',anc)
    ylabel('N_{recently created}')
    xlabel('Imaging session #')
    xlim([0 15]);ylim([0 0.3])
    axis square,box on;
    set(gca,'FontName','Calibri','FontSize',15)
    drawnow;
    
    figure(3),hold on
    errorbar(fitind-1,Nelim_data(fitind),Eelim_data(fitind),'.','Color',anc,'MarkerSize',10,'MarkerEdgeColor','none')
    plot(fitind-1,Nelim_data(fitind),'.','Color',anc,'MarkerSize',10)
    plot(fitind-1,Nelim_exact(fitind),'--','Color',anc)
    M=CalcGoF(Nelim_data(fitind),Nelim_exact(fitind),Eelim_data(fitind));
    text(15,0.30-0.025*a,num2str(M(1)),'FontSize',12,'Color',anc)
    ylabel('N_{eliminated}')
    xlabel('Imaging session #')
    xlim([0 16]);ylim([0 0.3]);
    axis square,box on;
    set(gca,'FontName','Calibri','FontSize',15)
    drawnow;
    
    figure(4),hold on
    title('Recently Eliminated Boutons','FontSize',15)
    errorbar(fitind-1,Nrecentlyelim_data(fitind),Erecentlyelim_data(fitind),'.','Color',anc,'MarkerSize',10,'MarkerEdgeColor','none')
    plot(fitind-1,Nrecentlyelim_data(fitind),'.','Color',anc,'MarkerSize',10)
    plot(fitind-1,Nrecentlyelim_exact(fitind),'--','Color',anc)
    
    M=CalcGoF(Nrecentlyelim_data(fitind),Nrecentlyelim_exact(fitind),Erecentlyelim_data(fitind));
    text(15,0.30-0.025*a,num2str(M(1)),'FontSize',12,'Color',anc)
    ylabel('N_{recently eliminated}')
    xlabel('Imaging session #')
    xlim([0 15]);ylim([0 0.3])
    axis square,box on;
    set(gca,'FontName','Calibri')
    set(gca,'FontSize',15)
    drawnow;
    
    %{
    figure(7),hold on
    title('Evolving state distribution','FontSize',15)
    dist=dist(:);
    temp=[];
    for i=1:20
        temp=[temp,dist];
        dist=P{a}*dist;
    end
    plot(0:19,temp(1,:),'.-','Color',anc,'MarkerSize',15,'LineWidth',1),hold on
    plot(0:19,temp(2,:)+temp(3,:),'--','Color',anc,'MarkerSize',15,'LineWidth',1)
    ylabel('Fraction in 0 or 1','FontSize',15)
    xlabel('Imaging session #','FontSize',15)
    xlim([0 20]);ylim([0 1])
    axis square,box on;
    drawnow;
    %}
    
    temp=chi2inv(0.68,4*numel(fitind)-2);
    chival=nanmin(chisqgrid(:))+temp;
    %{
    %----With p01,p10 and p12 on the grid:---------------------------------
    figure(5)
    chisqgrid=reshape(chisqgrid,[length(p01range),length(p10range),length(p12range)]);
    [p10,p01,p12]=meshgrid(p10range,p01range,p12range);% Order here is reversed for the first two elements
    
    patch(isosurface(p01,p10,p12,chisqgrid,chival),'FaceColor',anc,'FaceAlpha',0.2,'EdgeColor',anc,'EdgeAlpha',0.6);hold on;
    plot3(P{a}(1,2),P{a}(2,1),P{a}(2,3),'*','MarkerSize',15,'Color',anc);
    daspect([1,1,1]);view(3);
    xlim([0 1]);xlabel('p_{0 \leftarrow learn}');
    ylim([0 1]);ylabel('p_{learn \leftarrow 0}');
    zlim([0 1]);zlabel('p_{learn \leftarrow memory}');
    camlight;lighting none;box on;grid on;axis tight;
    set(gca,'FontSize',15)
    drawnow;
    %}
    
    %{
    figure(6)
    flm_within=flmgrid(chisqgrid<=chival);
    [h,e]=histcounts(flm_within(:),linspace(0,1,30),'Normalization','PDF');
    plot((e(1:end-1)+e(2:end))/2,h,'.-','MarkerSize',15,'Color',anc)
    hold on, axis square
    xlabel('f_{learning}','FontSize',15)
    ylabel('PDF','FontSize',15)
    drawnow;
    %}
    
    
    %----With p10,p21,flm on the grid--------------------------------------
    figure(5)
    chisqgrid=reshape(chisqgrid,[length(p10range),length(p21range),length(flmrange)]);
    [p21,p10,flmmesh]=meshgrid(p21range,p10range,flmrange);% Order here is reversed for the first two elements
    
    patch(isosurface(p10,p21,flmmesh,chisqgrid,chival),'FaceColor',anc,'FaceAlpha',0.2,'EdgeColor',anc,'EdgeAlpha',0.6);hold on;
    plot3(P{a}(2,1),P{a}(3,2),flm(a),'*','MarkerSize',15,'Color',anc);
    daspect([1,1,1]);view(3);
    xlim([0 1]);xlabel('p_{learn \leftarrow 0}');
    ylim([0 1]);ylabel('p_{learn \leftarrow memory}');
    zlim([0 1]);zlabel('f_{learn}');
    camlight;lighting none;box on;grid on;axis tight;
    set(gca,'FontSize',15)
    drawnow;
    %----------------------------------------------------------------------
    %}
end
movegui(1,'northeast')
movegui(2,'northwest')
movegui(3,'southeast')
movegui(4,'southwest')
movegui(5,'north')
%movegui(6,'south');
%figure(5),axis tight
%n00fit=((1./P{a}(2,1))-1)*n10;
%display(['n00 calc/fit ' num2str(n00./n00fit)]);

Pmat=nan([size(P{1}),length(P)]);
for i=1:length(P)
    Pmat(:,:,i)=P{i};
end
display('----------------------Summary--------------------------------')
display('-------------------------------------------------------------')
meanP=mean(Pmat,3);
stdP=std(Pmat,1,3);
display('mean P')
disp(meanP)
display('std P')
disp(stdP)
display(['f(mean+std)   ', num2str(mean(f)),' + ',num2str(std(f,1))])
display(['flm(mean+std) ', num2str(mean(flm)),' + ',num2str(std(flm,1))])
end


function [M] = CalcGoF(yy,ff,ee)
p=3;%fit performed using 3 transition matrix elements
n=numel(yy);
yy=yy(:);ff=ff(:);
Rsq=1-(sum((yy-ff).^2)/sum((yy-mean(yy)).^2));
AdjRsq=1-(1-Rsq)*(n-1)/(n-p-1);

Chisq=sum(((yy-ff)./ee).^2);
AdjChisq=Chisq./(n-p-1);
M=[Chisq];
end