function [Dat] = analysis_getmat(An)
%This function creates matrices for analysis from registered data.
%[An] is output from analysis_loaddat.m
%[Dat] does not contain probabilities yet.
% Matrices contain all valid (based on flags) detected peaks. 
% Boutons definition related to weight crossing a threshold in at least n
% sessions is not enforced at this stage.

channel={'G'};
filter={{'LoGxy'}};
%Fitfunction
%Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
Ff_fast = @(x,A,mu,sigma) bsxfun(@times,exp(-bsxfun(@rdivide,(bsxfun(@minus,x,mu)).^2,2*sigma.^2)),A); %Same as Ff

nvecsteps=50;
for ch=1:numel(channel)
    for fi=1:numel(filter{ch})
        Dat.(channel{ch}).(filter{ch}{fi}).ax_id=[];     %(1 x btn) This is identifies section and axon
        Dat.(channel{ch}).(filter{ch}{fi}).btn_id=[];    %(1 x btn)This is bouton lbl assigned during registration
        Dat.(channel{ch}).(filter{ch}{fi}).flag=[];      %(time x btn) These are assigned during registration
        Dat.(channel{ch}).(filter{ch}{fi}).ind=[];       %(time x btn) Index along corresponding profile
        Dat.(channel{ch}).(filter{ch}{fi}).d=[];         %(1 x btn) 1d position along the aligned profile
        Dat.(channel{ch}).(filter{ch}{fi}).dorig=[];     %(time x btn) 1d position along the original profile
        Dat.(channel{ch}).(filter{ch}{fi}).blen=[];      %(time x btn) length associated with bouton
        Dat.(channel{ch}).(filter{ch}{fi}).rx=[];        %(time x btn) 3d x-position of peak in each time point
        Dat.(channel{ch}).(filter{ch}{fi}).ry=[];        %(time x btn) 3d y-position of peak in each time point
        Dat.(channel{ch}).(filter{ch}{fi}).rz=[];        %(time x btn) 3d z-position of peak in each time point
        
        Dat.(channel{ch}).(filter{ch}{fi}).nx=[];        %(time x btn) x-component of normalized vector along the axon
        Dat.(channel{ch}).(filter{ch}{fi}).ny=[];        %(time x btn) y-component of normalized vector along the axon
        Dat.(channel{ch}).(filter{ch}{fi}).nz=[];        %(time x btn) z-component of normalized vector along the axon
        
        Dat.(channel{ch}).(filter{ch}{fi}).Iraw=[];      %Non-normalized intensity at location on profile.
        Dat.(channel{ch}).(filter{ch}{fi}).Inorm=[];     %Intensity at location on profile. Inorm is roughly Ifg+Ibg
        Dat.(channel{ch}).(filter{ch}{fi}).Ibg=[];       %Intensity of background at given foreground peak location
        Dat.(channel{ch}).(filter{ch}{fi}).sig=[];       %Peak width for every detected peak. nan otherwise
        Dat.(channel{ch}).(filter{ch}{fi}).amp=[];       %Intensity of fitted foreground peak. nan otherwise
        Dat.(channel{ch}).(filter{ch}{fi}).Imedian=[];   %Intensity of fitted foreground peak based on median filter.
        
        for se=1:numel(An.Section)
            for ax=1:numel(An.Section{se}.Time{1}.Axon)
                
                %1. Initializing matrices----------------------------------
                max_nbtns=numel(An.Section{se}.Time{1}.Axon{ax}.fit.(channel{ch}).(filter{ch}{fi}).fg.ind)*numel(An.Section{se}.Time);
                n_times=numel(An.Section{se}.Time);
                
                AxonMat.ax_id=ones(1,max_nbtns).*...     %ax_id = 1000 x [Section #] + [Axon #] This convention is used in analysis codes to identify the axon from the data matrix
                    ((10^3*str2double(An.Section{se}.Time{1}.Axon{ax}.id(9)))+...
                    str2double(An.Section{se}.Time{1}.Axon{ax}.id(end-2:end)));
                
                AxonMat.btn_id=nan(1,max_nbtns);
                AxonMat.flag=nan(n_times,max_nbtns);
                AxonMat.ind=nan(n_times,max_nbtns);
                AxonMat.d=nan(1,max_nbtns);
                AxonMat.dorig=nan(n_times,max_nbtns);
                AxonMat.blen=nan(n_times,max_nbtns);
                AxonMat.rx=nan(n_times,max_nbtns);
                AxonMat.ry=nan(n_times,max_nbtns);
                AxonMat.rz=nan(n_times,max_nbtns);
                
                AxonMat.nx=nan(n_times,max_nbtns);
                AxonMat.ny=nan(n_times,max_nbtns);
                AxonMat.nz=nan(n_times,max_nbtns);
                
                AxonMat.Imedian=nan(n_times,max_nbtns);
                AxonMat.Iraw=nan(n_times,max_nbtns);
                AxonMat.Inorm=nan(n_times,max_nbtns);
                AxonMat.Ibg=nan(n_times,max_nbtns);
                AxonMat.sig=nan(n_times,max_nbtns);
                AxonMat.amp=nan(n_times,max_nbtns);
                
                %2. Populating matrices------------------------------------
                dmin=-inf;dmax=inf;
                Ibg=cell(numel(An.Section{se}.Time),1);
                for ti=1:numel(An.Section{se}.Time)
                    Axon=An.Section{se}.Time{ti}.Axon{ax};
                    
                    dmin=max([(Axon.fit.(channel{ch}).(filter{ch}{fi}).d.man(1)),dmin]);
                    dmax=min([(Axon.fit.(channel{ch}).(filter{ch}{fi}).d.man(end)),dmax]);
                    nanind=isnan(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.manid);
                    Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.manid(nanind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.id(nanind);
                    
                    [~,matind]=ismember(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.manid,AxonMat.btn_id);
                    startind=find(isnan(AxonMat.btn_id),1,'first');
                    endind=startind+sum(matind==0)-1;
                    matind(matind==0)=startind:endind;
                    
                    AxonMat.btn_id(1,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.manid;
                    AxonMat.flag(ti,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.flag;
                    AxonMat.ind(ti,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind;
                    AxonMat.d(1,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).d.man(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    AxonMat.dorig(ti,matind)=Axon.d.optim(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    
                    %{
                    %Extra peaks are assumed as removed from dataset (editpeaks).
                    temp=diff([dmin,AxonMat.d(1,matind),dmax]);
                    blen=(temp(1:end-1)+temp(2:end))./2;
                    blen(1)=blen(1)+temp(1)/2;
                    blen(end)=blen(end)+temp(end)/2;
                    AxonMat.blen(ti,matind)=blen;
                    %}
                    
                    AxonMat.rx(ti,matind)=Axon.r.optim(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind,1);
                    AxonMat.ry(ti,matind)=Axon.r.optim(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind,2);
                    AxonMat.rz(ti,matind)=Axon.r.optim(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind,3);
                    
                    AxonMat.Iraw(ti,matind)=Axon.I.(channel{ch}).(filter{ch}{fi}).raw(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    AxonMat.Inorm(ti,matind)=Axon.I.(channel{ch}).(filter{ch}{fi}).norm(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    AxonMat.Imedian(ti,matind)=Axon.I.(channel{ch}).median333.norm(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    
                    %If peaks were added manually, their intensity is nan
                    %after operations by gui_alignment. Here those
                    %intensities are set to the normalized profile value.
                    %This will not be entered if profiles are re-fit
                    %after manual peak additions.
                    man_addedpeaks=find(isnan(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.amp));
                    if numel(man_addedpeaks)>0
                        Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.amp(man_addedpeaks)=...
                            Axon.I.(channel{ch}).(filter{ch}{fi}).norm(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind(man_addedpeaks));
                    end
                    AxonMat.amp(ti,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.amp;
                    AxonMat.sig(ti,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.sig;
                    
                    %{
                    Ibg{ti}=zeros(size(Axon.d.optim));
                    for p=1:numel(Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.ind)
                        Ibg{ti}=Ibg{ti}+Ff(Axon.d.optim,...
                            Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.amp(p),...
                            Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.mu(p),...
                            Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.sig(p));
                    end
                    %}
                    Ibg{ti}=sum(Ff_fast(Axon.d.optim(:),...
                        Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.amp(:)',...
                        Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.mu(:)',...
                        Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.sig(:)'),2);
                    AxonMat.Ibg(ti,matind)=Ibg{ti}(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                end
                
                %3. Remove extra columns from initialization---------------
                fldnm=fieldnames(AxonMat);
                remind=find(sum(isnan(AxonMat.ind),1)==size(AxonMat.ind,1));
                remind=unique(remind);
                for f=1:numel(fldnm)
                    AxonMat.(fldnm{f})(:,remind)=[];
                end
                
                %Sortind boutons in every axon by distance-----------------
                [~,sortind]=sort(AxonMat.d);
                for f=1:numel(fldnm)
                    AxonMat.(fldnm{f})=AxonMat.(fldnm{f})(:,sortind);
                end
                
                %4. Replace nans with corresponding value on profile-------
                %These are boutons detected in only a subset of sessions
                for ti=1:numel(An.Section{se}.Time)
                    Axon=An.Section{se}.Time{ti}.Axon{ax};
                    
                    nanind=find(isnan(AxonMat.ind(ti,:)));
                    nanind=nanind(:)';
                    
                    [~,minind]=min(abs(bsxfun(@minus,AxonMat.d(nanind),Axon.fit.(channel{ch}).(filter{ch}{fi}).d.man(:))),[],1);
                    AxonMat.ind(ti,nanind)=minind;
                    
                    AxonMat.dorig(ti,nanind)=Axon.d.optim(minind);
                    AxonMat.rx(ti,nanind)=Axon.r.optim(minind,1);
                    AxonMat.ry(ti,nanind)=Axon.r.optim(minind,2);
                    AxonMat.rz(ti,nanind)=Axon.r.optim(minind,3);
                    AxonMat.Iraw(ti,nanind)=Axon.I.(channel{ch}).(filter{ch}{fi}).raw(minind);
                    AxonMat.Inorm(ti,nanind)=Axon.I.(channel{ch}).(filter{ch}{fi}).norm(minind);
                    AxonMat.Ibg(ti,nanind)=Ibg{ti}(minind);
                    AxonMat.Imedian(ti,nanind)=Axon.I.(channel{ch}).median333.norm(minind);
                end
                
                %5. Remove boutons within annotated regions----------------
                iswithinignored=nan(size(AxonMat.ind));
                for ti=1:numel(An.Section{se}.Time)
                    Axon=An.Section{se}.Time{ti}.Axon{ax};
                    iswithinignored(ti,:)=Axon.annotate.ignore(AxonMat.ind(ti,:));
                end
                remind=any(iswithinignored,1);
                fldnm=fieldnames(AxonMat);
                for f=1:numel(fldnm)
                    AxonMat.(fldnm{f})(:,remind)=[];
                end
                
                %6. Assign distances to each peak before removing based on
                %flags ----------------------------------------------------
                
                %For below algorithm, it is assumed that boutons within ignored
                %regions are removed, and boutons are sorted by distance in
                %the matrix.
                
                %Algorithm steps:
                %{
                a) Find the start and end points of ignored regions, and
                   assign them id = 1
                b) Find Boutons, and assign them id = 0
                c) Sort id list [0s and 1s] based on position along the
                   axon
                d) Distance between successive 1s and 0s is calculated.
                e) Distances between successive 1s is discarded. The
                   remaining distances are assigned to the 0s (boutons)
                   e.g. [1 <--d1--> 0 <--d2--> 0]; the middle 0 is assigned
                   (d1+d2)/2
                %}
                for ti=1:numel(An.Section{se}.Time)
                    Axon=An.Section{se}.Time{ti}.Axon{ax};
                    igstart=find(diff(Axon.annotate.ignore)==1)+1;
                    igend=find(diff(Axon.annotate.ignore)==-1);
                    igstart=igstart(:);
                    igend=igend(:);
                    if Axon.annotate.ignore(1)==1
                        igstart=[1;igstart];
                    end
                    if Axon.annotate.ignore(end)==1
                        igend=[igend;numel(Axon.annotate.ignore)];
                    end
                    ig_dstart=Axon.fit.G.(filter{ch}{fi}).d.man(igstart(:));
                    ig_dend=Axon.fit.G.(filter{ch}{fi}).d.man(igend(:));
                    ig_d=[ig_dstart;ig_dend];
                    ig_id=ones(size(ig_d));
                    
                    btn_d=Axon.fit.G.(filter{ch}{fi}).d.man(AxonMat.ind(ti,:));
                    btn_id=zeros(size(AxonMat.ind(ti,:)));
                    
                    dists=[Axon.fit.G.(filter{ch}{fi}).d.man(1);ig_d(:);btn_d(:);Axon.fit.G.(filter{ch}{fi}).d.man(end)];
                    ids=[1;ig_id(:);btn_id(:);1];
                    
                    [dists,sortind]=sort(dists);
                    ids=ids(sortind);%Stable sorting expected. (i.e. order is preserved if entries are the same)
                    
                    %This part assumes the first element is not a bouton,
                    %which is true since we are appending the first and
                    %last points to dists above
                    btninds=find(ids==0);
                    temp=diff(dists,1,1);
                    AxonMat.blen(ti,:)=(temp(btninds-1)+temp(btninds))./2;
                end
                
                %7. Remove boutons based on flags
                % Convention for flags:
                % Flag 0. Reset flag to nan
                % Flag 1. Ignore peak
                % Flag 2. Terminal bouton
                % Flag 5. No match found - keep this
                temp=AxonMat.flag;
                temp(isnan(temp))=0;%because nan~=0 = true;
                temp=temp~=0 & temp~=5;
                [~,remind1]=find(temp);
                remind2=find(AxonMat.d<dmin | AxonMat.d>dmax);
                remind=unique([remind1(:);remind2(:)]);
                for f=1:numel(fldnm)
                    AxonMat.(fldnm{f})(:,remind)=[];
                end
                
                %Calculate normal vectors along the axon for each bouton
                for ti=1:numel(An.Section{se}.Time)
                    Axon=An.Section{se}.Time{ti}.Axon{ax};
                    vecind=[AxonMat.ind(ti,:)'-nvecsteps,AxonMat.ind(ti,:)'+nvecsteps];
                    vecind(vecind<1)=1;vecind(vecind>numel(Axon.d.optim))=numel(Axon.d.optim);
                    AxonMat.nx(ti,:)=Axon.r.optim(vecind(:,1),1)-Axon.r.optim(vecind(:,2),1);
                    AxonMat.ny(ti,:)=Axon.r.optim(vecind(:,1),2)-Axon.r.optim(vecind(:,2),2);
                    AxonMat.nz(ti,:)=Axon.r.optim(vecind(:,1),3)-Axon.r.optim(vecind(:,2),3);
                    
                    vecnorm=(AxonMat.nx(ti,:).^2+AxonMat.ny(ti,:).^2+AxonMat.nz(ti,:).^2).^0.5;
                    AxonMat.nx(ti,:)=AxonMat.nx(ti,:)./vecnorm;
                    AxonMat.ny(ti,:)=AxonMat.ny(ti,:)./vecnorm;
                    AxonMat.nz(ti,:)=AxonMat.nz(ti,:)./vecnorm;
                end
                
                
                %8. Append axons to Dat
                for f=1:numel(fldnm)
                    Dat.(channel{ch}).(filter{ch}{fi}).(fldnm{f})=cat(2,Dat.(channel{ch}).(filter{ch}{fi}).(fldnm{f}),AxonMat.(fldnm{f}));
                end
                
                %Related to normalization
                Temp=cell(numel(An.Section{se}.Time),1);
                for ti=1:numel(An.Section{se}.Time)
                    %{
                    %Calculate normalization only based on current time:
                    Gauss_bg=zeros(size(An.Section{se}.Time{ti}.Axon{ax}.d.optim));
                    for i=1:numel(An.Section{se}.Time{ti}.Axon{ax}.fit.G.('Gauss').bg.ind)
                        Gauss_bg=Gauss_bg+Ff(An.Section{se}.Time{ti}.Axon{ax}.d.optim,...
                            An.Section{se}.Time{ti}.Axon{ax}.fit.G.('Gauss').bg.amp(i),...
                            An.Section{se}.Time{ti}.Axon{ax}.fit.G.('Gauss').bg.mu(i),...
                            An.Section{se}.Time{ti}.Axon{ax}.fit.G.('Gauss').bg.sig(i));
                    end
                    Temp{ti}.norm=mean(Gauss_bg(~An.Section{se}.Time{ti}.Axon{ax}.annotate.ignore));
                    %}
                    
                    Temp{ti}=An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{ch}{fi});
                    Temp{ti}.ax_id=AxonMat.ax_id(1);
                end
                
                if ~isfield(Dat.(channel{ch}).(filter{ch}{fi}),'norm')
                    Dat.(channel{ch}).(filter{ch}{fi}).norm=Temp;
                else
                    Dat.(channel{ch}).(filter{ch}{fi}).norm=cat(2,Dat.(channel{ch}).(filter{ch}{fi}).norm,Temp);
                    clear Temp;
                end
            end
        end
    end
    
    if isfield(An,'L')
        Dat.L=An.L;
    end
    
    Dat.anid=str2double(An.Section{1}.Time{1}.Axon{1}.id(3:5));
end

