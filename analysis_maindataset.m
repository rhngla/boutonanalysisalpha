function Out=analysis_maindataset(Animal,D,Other_Data,analysis_type)
%This function generates results for the main dataset.
%Animal is output from analysis_loaddat;
%D is output from either analysis_getmat or analysis_processdat;
%Other_Data is a cell of D from other animals {D83,D101} etc - this is used for equalization only;
isequalize=false;

if ismac
    set(0,'defaultAxesFontSize',18,'defaultAxesFontName','Calibri')
end
Out=[];

%Impose bouton definitions, assign normalized bouton weights and
%probabilities:
Dat=analysis_editmatrix(D);

%Functions for probabilistic calculations:
f=fP_handles;

if isequalize
    for i=1:numel(Other_Data)
        Other_Data{i}=analysis_editmatrix(Other_Data{i});
        Other_Data{i}=Other_Data{i}.w;
    end
    Dat.w=analysis_equalize(Dat.w,Other_Data);
    Dat.P=f.P(Dat.w);
end

%Define plotting color for animals
allan=[83,85,101,102,88,108,68,89];
anind=(allan==Dat.anid);
anc=lines(numel(allan));anc(end,:)=[0.3 0.3 0.3];
anc=anc(anind,:);

%For printing figures:
is_savefig=false;

switch analysis_type
    case 'Axon outliers'%--------------------------------------------------
        %This analysis can be used to check for axons that show high
        %turnover or other test measures that may indicate either a problem
        %with image quality, registration of boutons or
        
        unique_axid=unique(Dat.ax_id);
        
        %Define measure to check for artifacts
        test_measure=abs(f.P_add(Dat.w(1:end-1,:),Dat.w(2:end,:))-f.P_elim(Dat.w(1:end-1,:),Dat.w(2:end,:)));
        [~,pot_errors]=sort(nanmean(test_measure,1));
        test_measure_sort=test_measure(:,pot_errors);
        
        %{
        %Visually inspect potential errors boutons on image projections
        analysis_plotboutonsonIM(Dat,Animal,pot_errors(end-20:end));
        %}
        
        %{
        %Plot measure for each bouton
        figure,imagesc(test_measure);hold on
        figure,imagesc(test_measure_sort);hold on
        caxis([0 1]);colorbar;box on;
        title(['Animal ',num2str(Dat.anid)]);
        %}
        
        %{
        %plot axon divisions - valid if measure on the image is not sorted
        for ax=1:numel(unique_axid)
            axind=find(Dat.ax_id==unique_axid(ax),1,'last');
            xx=(find(Dat.ax_id==unique_axid(ax),1,'first')+find(Dat.ax_id==unique_axid(ax),1,'last'))./2;
            text(xx,1,num2str(unique_axid(ax)),'HorizontalAlignment','center','BackgroundColor',[1 1 1],...
                'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
            plot(axind.*[1,1],[-1 size(Dat.w,1)],'-','Color',[0.8 0 0],'LineWidth',2);hold on
        end
        %}
        
        %Sort axons according to measure
        axonwise_measure=nan(numel(unique_axid),1);
        for ax=1:numel(unique_axid)
            temp=test_measure(:,Dat.ax_id==unique_axid(ax));
            axonwise_measure(ax)=mean(temp(:));
        end
        [~,ind]=sort(axonwise_measure);
        axidsort=unique_axid(ind);
        
        test_measure_sort=nan(size(test_measure));
        test_measure_sorted_w=nan(size(Dat.w));
        test_measure_sorted_axid=nan(1,size(test_measure,2));
        
        endd=0;
        for ax=1:numel(unique_axid)
            startt=endd+1;
            endd=startt+sum(Dat.ax_id==axidsort(ax))-1;
            test_measure_sort(:,startt:endd)=test_measure(:,Dat.ax_id==axidsort(ax));
            test_measure_sorted_w(:,startt:endd)=Dat.w(:,Dat.ax_id==axidsort(ax));
            test_measure_sorted_axid(1,startt:endd)=Dat.ax_id(:,Dat.ax_id==axidsort(ax));
        end
        
        figure(2);clf(2),imagesc(test_measure_sort);hold on;
        caxis([0 1]);colorbar;box on;
        title(['After sort, Animal ',num2str(Dat.anid)]);
        for ax=1:numel(axidsort)
            axind=find(test_measure_sorted_axid==axidsort(ax),1,'last');
            plot(axind.*[1,1],[-1 size(test_measure_sorted_w,1)],'-','Color',[0.8 0 0],'LineWidth',2)
            xx=(find(test_measure_sorted_axid==axidsort(ax),1,'first')+find(test_measure_sorted_axid==axidsort(ax),1,'last'))./2;
            text(xx,0.5,num2str(axidsort(ax)),'HorizontalAlignment','center','BackgroundColor',[1 1 1],...
                'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        end
        ylim([-0.5 size(Dat.w,1)+0.5])
        %}
        
    case 'Distributions'%--------------------------------------------------
        %Plot bouton weight distributions
        thr=1;
        binedges=thr:2:25;
        bincenters=(binedges(1:end-1)+binedges(2:end))./2;
        binsiz=(binedges(2:end)-binedges(1:end-1));
        cc=parula(size(Dat.w,1)+2);
        
        count=nan(size(Dat.w,1),numel(bincenters));
        std_count=nan(size(Dat.w,1),numel(bincenters));
        for ee=1:length(bincenters)
            %Probabilistic count of boutons within each bin
            P_in_bin=f.P(Dat.w,binedges(ee))-f.P(Dat.w,binedges(ee+1));
            count(:,ee)=sum(P_in_bin,2);
            
            %Error bars from Poisson model
            std_count(:,ee)=sum(P_in_bin.*(1-P_in_bin),2);
        end
        
        prob=bsxfun(@rdivide,count,sum(count,2));%divide by total count in each time point
        pdf=bsxfun(@rdivide,prob,binsiz);%account for unequal bin-sizes
        
        std_prob=bsxfun(@rdivide,std_count,sum(count,2));%divide by total count in each time point
        std_pdf=bsxfun(@rdivide,std_prob,binsiz);%account for unequal bin-sizes
        
        h=gobjects(size(Dat.w,1),1);
        for i=1:size(Dat.w,1)
            figure(500+find(anind));
            errorbar(bincenters,pdf(i,:),std_pdf(i,:),'LineStyle','none','Marker','none','Color',[cc(i,:),0.8]);
            h(i)=plot(bincenters,pdf(i,:),'.-','Color',[cc(i,:),0.8],'LineWidth',1.5,'MarkerFaceColor',cc(i,:),'MarkerSize',5);hold on;
            drawnow;
        end
        title(['Animal ',num2str(Dat.anid)]);xlabel('Intensity (Bin centers)');ylabel('Probability density');
        
        patch([0;thr;thr;0],[0;0;1;1],[0.2 0.2 0.2],'FaceAlpha',0.1,'EdgeColor','none');
        xlim([0 binedges(end)]),ylim([0 0.3])
        ax = gca;ax.XTick = bincenters;
        box on;axis square;grid on
        ylim([0.0 0.27])
        legend(h,{Dat.L.im_id{:}});
        drawnow;
        
        if is_savefig
            hh=gcf;
            hh.Position=[0,40,650,579];
            set(findobj(hh,'Type','ErrorBar'),'LineWidth',2);
            set(findobj(hh,'Type','Line'),'LineWidth',2);
            drawnow
            pathlist;
            print(gcf,'-dpng',[res_pth,'Distribution over time-',num2str(Dat.anid),'.png']);
            savefig(gcf,[res_pth,'Distribution over time-',num2str(Dat.anid),'.fig'])
        end
        
        
        figure(500);movegui(500,'northeast')
        if isempty(get(gca,'Children'))
            patch([0;thr;thr;0],[0;0;1;1],[0.2 0.2 0.2],'FaceAlpha',0.1,'EdgeColor','none');hold on
        end
        errorbar(bincenters,mean(pdf,1),std(pdf,0,1),'-','Color',anc,'LineStyle','none');hold on
        plot(bincenters,mean(pdf,1),'.-','Color',anc,'MarkerSize',5,'MarkerFaceColor',anc);hold on
        xlabel('Bouton weight');ylabel('Probability density')
        xlim([0 binedges(end)]),ylim([0 0.3])
        ax=gca;ax.XTick=bincenters;
        box on;axis square;grid on
        drawnow;
        
        if is_savefig
            pathlist;
            hh=gcf;
            hh.Position=[0,40,650,579];
            set(findobj(hh,'Type','ErrorBar'),'LineWidth',2);
            set(findobj(hh,'Type','Line'),'LineWidth',2);
            print(gcf,'-dpng',[res_pth,'Overall distribution-All animals.png']);
            savefig(gcf,[res_pth,'Overall distribution-All animals.fig'])
        end
        
    case 'Homeostasis'
        %Plot bouton weight and number densities
        
        ax_lbl=unique(Dat.ax_id);
        n_axons=numel(ax_lbl);
        tt=analysis_aligntime(Dat,[],'start');
        
        num=nan(size(Dat.w,1),n_axons);
        var_num=nan(size(Dat.w,1),n_axons);
        Wt=nan(size(Dat.w,1),n_axons);
        var_Wt=nan(size(Dat.w,1),n_axons);
        ax_length=mean(Dat.ax_length,1);
        ax_identifier=cell(n_axons,1);
        
        for ax=1:n_axons
            ax_w=Dat.w(:,Dat.ax_id==ax_lbl(ax));
            ax_P=Dat.P(:,Dat.ax_id==ax_lbl(ax));
            ax_dw=Dat.dw(:,Dat.ax_id==ax_lbl(ax));
            
            num(:,ax)=sum(ax_P,2);
            var_num(:,ax)=sum(ax_P.*(1-ax_P),2);
            
            Wt(:,ax)=sum(ax_w,2);
            var_Wt(:,ax)=sum((ax_dw.^2),2);
            
            ax_identifier{ax}=['Axon: ',num2str(ax_lbl(ax)),' , # boutons: ',num2str(mean(num(:,ax),1)),' , Axon length: ',num2str(ax_length(ax))];
        end
        
        num_density=sum(num,2)./sum(ax_length);
        std_num_density=(sum(var_num,2).^0.5)./sum(ax_length); %From counting of boutons
        std_ax_num_density=std(bsxfun(@rdivide,num,ax_length),[],2)./(size(num,2)^0.5); %From differences in axons
        var_num=std_num_density.^2+std_ax_num_density.^2;
        
        Wt_density=sum(Wt,2)./sum(ax_length);
        std_Wt_density=(sum(var_Wt,2).^0.5)./sum(ax_length); %From individual boutons
        std_ax_num_density=std(bsxfun(@rdivide,Wt,ax_length),[],2)./(size(Wt,2)^0.5); %From differences in axons
        var_Wt=std_Wt_density.^2+std_ax_num_density.^2;
        
        figure(1000);movegui(1000,'northwest');
        yy=num_density;
        ee=std_num_density;
        txt=Dat.L.im_id;
        errorbar(tt,yy,ee,'Marker','none','Color',anc,'MarkerSize',5,'LineStyle','-'),hold on
        plot(tt,yy,'s','Color',anc,'MarkerSize',5,'MarkerFaceColor',anc),hold on
        %text(tt,1.05*yy,txt,'HorizontalAlignment','center','Color',anc,...
        %    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        plot([min(tt),max(tt)],[mean(yy),mean(yy)],'-','Color',anc)
        h=gca;h.YLim(1)=0;h.YLim(2)=max([h.YLim(2),1.2*max(yy)]);
        h.XLim(1)=min([min(tt(:))-4,h.XLim(1)]);h.XLim(2)=max([h.XLim(2),max(tt(:))+4]);
        xlabel('Time (days)');ylabel('Bouton number density (\mu{m}^{-1})')
        box on;axis square;
        drawnow;
        
        if is_savefig
            ylim([0 0.55])
            pathlist;
            hh=gcf;
            hh.Position=[0,40,650,579];
            set(findobj(hh,'Type','ErrorBar'),'LineWidth',2);
            set(findobj(hh,'Type','Line'),'LineWidth',2);
            print(gcf,'-dpng',[res_pth,'Homeostasis-Number-All animals.png']);
            savefig(gcf,[res_pth,'Homeostasis-Number-All animals.fig'])
        end
        
        figure(1002);movegui(1002,'north')
        yy=Wt_density;
        ee=std_Wt_density;
        errorbar(tt,yy,ee,'Marker','none','Color',anc,'LineStyle','-'),hold on;
        plot(tt,yy,'s','Color',anc,'MarkerSize',5,'MarkerFaceColor',anc),hold on;
        %text(tt,1.05*yy,txt,'HorizontalAlignment','center','Color',anc,...
        %    'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        plot([min(tt),max(tt)],[mean(yy),mean(yy)],'-','Color',anc);
        h=gca;h.YLim(1)=0;h.YLim(2)=max([h.YLim(2),1.2*max(yy(:))]);
        h.XLim(1)=min([min(tt(:))-4,h.XLim(1)]);h.XLim(2)=max([h.XLim(2),max(tt(:))+4]);
        xlabel('Time (days)');ylabel('Bouton weight density (\mu{m}^{-1})')
        box on;axis square;
        drawnow;
        
        if is_savefig
            ylim([0 4])
            pathlist;
            hh=gcf;
            hh.Position=[0,40,650,579];
            set(findobj(hh,'Type','ErrorBar'),'LineWidth',2);
            set(findobj(hh,'Type','Line'),'LineWidth',2);
            print(gcf,'-dpng',[res_pth,'Homeostasis-Weight-All animals.png']);
            savefig(gcf,[res_pth,'Homeostasis-Weight-All animals.fig'])
        end
        
        
        %Plot axon wise measures
        cc_grays=gray(40);
        
        ax_num_density=bsxfun(@rdivide,num,ax_length);
        figure(1010+find(anind));movegui(1010+find(anind),'southwest')
        h=plot(tt,ax_num_density,'s-','MarkerSize',5,'Color',[0.7 0.7 0.7]);hold on
        title(['Animal ',num2str(Dat.anid)]);xlabel('Time (days)');ylabel('Number density per axon \mu{m}^{-1}')
        %text(max(tt)*ones(size(num,2),1),ax_num_density(end,:),ax_identifier)
        box on;axis square;
        for ii=1:numel(h)
            set(h(ii),'Color',cc_grays(ii,:),'MarkerFaceColor',cc_grays(ii,:),...
                'ButtonDownFcn',@analysis_explorelines,'UserData',ax_identifier(ii))
        end
        h=gca;h.YLim=[0 1.2*max(ax_num_density(:))];
        h.XLim(1)=min([min(tt(:))-4,h.XLim(1)]);h.XLim(2)=max([h.XLim(2),max(tt(:))+4]);
        drawnow;
        
        if is_savefig
            ylim([0 0.55])
            pathlist;
            hh=gcf;
            hh.Position=[0,40,650,579];
            set(findobj(hh,'Type','ErrorBar'),'LineWidth',2);
            set(findobj(hh,'Type','Line'),'LineWidth',2);
            print(gcf,'-dpng',[res_pth,'Homeostasis-Axons-Number-',num2str(Dat.anid),'.png']);
            savefig(gcf,[res_pth,'Homeostasis-Axons-Number-',num2str(Dat.anid),'.fig'])
        end
        
        ax_wt_density=bsxfun(@rdivide,Wt,ax_length);
        figure(1020+find(anind));movegui(1020+find(anind),'south')
        h=plot(tt,ax_wt_density,'s-','MarkerSize',5,'Color',[0.7 0.7 0.7]);hold on;
        title(['Animal ',num2str(Dat.anid)]);xlabel('Time (days)');ylabel('Weight density per axon \mu{m}^{-1}')
        %text(max(tt)*ones(size(num,2),1),ax_wt_density(end,:),ax_identifier)
        box on;axis square;ylim([0 7]);xlim([min(tt)-2,max(tt)+2])
        for ii=1:numel(h)
            set(h(ii),'Color',cc_grays(ii,:),'MarkerFaceColor',cc_grays(ii,:),...
                'ButtonDownFcn',@analysis_explorelines,'UserData',ax_identifier(ii))
        end
        h=gca;h.YLim=[0 1.2*max(ax_wt_density(:))];
        h.XLim(1)=min([min(tt(:))-4,h.XLim(1)]);h.XLim(2)=max([h.XLim(2),max(tt(:))+4]);
        drawnow;
        
        if is_savefig
            ylim([0 4])
            pathlist;
            hh=gcf;
            hh.Position=[0,40,650,579];
            set(findobj(hh,'Type','ErrorBar'),'LineWidth',2);
            set(findobj(hh,'Type','Line'),'LineWidth',2);
            print(gcf,'-dpng',[res_pth,'Homeostasis-Axons-Weight-',num2str(Dat.anid),'.png']);
            savefig(gcf,[res_pth,'Homeostasis-Axons-Weight-',num2str(Dat.anid),'.fig'])
        end
        
    case 'Statistics'
        %Plot addition, elimination, turnover, potentiation, depression and
        %oscillations
        
        w=Dat.w;
        
        %To threshold probabilities using significance level
        sig_level=0; %siglevel~=0 uses thresholded values
        
        if sig_level==0
            P=f.P(w);
            P_add=f.P_add(w(1:end-1,:),w(2:end,:));
            P_elim=f.P_elim(w(1:end-1,:),w(2:end,:));
            pot=f.P_pot(w(1:end-1,:),w(2:end,:));
            dep=f.P_dep(w(1:end-1,:),w(2:end,:));
            changed=f.P_change(w(1:end-1,:),w(2:end,:));
            
        else
            P=f.P(w)>sig_level;
            P_add=f.P_add(w(1:end-1,:),w(2:end,:))>sig_level;
            P_elim=f.P_elim(w(1:end-1,:),w(2:end,:))>sig_level;
            pot=f.P_pot(w(1:end-1,:),w(2:end,:))>sig_level;
            dep=f.P_dep(w(1:end-1,:),w(2:end,:))>sig_level;
            changed=f.P_change(w(1:end-1,:),w(2:end,:))>sig_level;
        end
        
        %for weight calculations
        S_mean=sum(w(1:end-1,:)+w(2:end,:),2);
        S_change=abs(w(2:end,:)-w(1:end-1,:));
        
        tt=analysis_aligntime(Dat,[],'start');
        tt=(tt(1:end-1)+tt(2:end))./2;
        tt_txt=cell(size(w,1)-1,1);
        for i=1:numel(tt_txt)
            tt_txt{i}=[Dat.L.im_id{i},Dat.L.im_id{i+1}];
        end
        
        hfn=100;
        hf=gobjects(20,1);
        figind=1;
        %Added-----------------
        hf(figind)=figure(80+figind);movegui(hf(figind),'northwest');
        %yy=sum(P_add,2)./sum(P(2:end,:),2);
        yy=sum(P_add,2)./mean(sum(P(1:end,:),2));
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Added Fraction');drawnow;hold on;
        figind=figind+1;
        
        %Avg added weight
        hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
        S_add=S_change.*P_add;
        yy=sum(S_add,2)./sum(P_add,2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Avg added weight');drawnow;hold on;
        figind=figind+1;
        
        %Elimination------------
        hf(figind)=figure(80+figind);movegui(hf(figind),'north');
        %yy=sum(P_elim,2)./sum(P(1:end-1,:),2);
        yy=sum(P_elim,2)./mean(sum(P(1:end,:),2));
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Eliminated Fraction');drawnow;hold on;
        figind=figind+1;
        
        %Avg eliminated weight
        hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
        S_elim=S_change.*P_elim;
        yy=sum(S_elim,2)./sum(P_elim,2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Avg eliminated weight');drawnow;hold on;
        figind=figind+1;
        
        %Turnover---------------
        hf(figind)=figure(80+figind);movegui(hf(figind),'northeast');
        yy=(sum(P_add,2)+sum(P_elim,2))./((sum(P(1:end-1,:),2)+sum(P(2:end,:),2))./2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Turnover Fraction');drawnow;hold on;
        figind=figind+1;
        
        %Potentiation-------------
        %------------Number
        hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
        yy=sum(pot,2)./sum(P(1:end-1,:),2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Potentiated Fraction');drawnow;hold on;
        figind=figind+1;
        
        %------------Relative amount
        hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
        S_pot=S_change.*pot;
        yy=sum(S_pot./S_mean,2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Relative Potentiated Weight');drawnow;hold on;
        figind=figind+1;
        
        %------------Avg amount
        hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
        S_pot=S_change.*pot;
        yy=sum(S_pot,2)./sum(pot,2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Avg amount of Potentiation');drawnow;hold on;
        figind=figind+1;
        
        %Depression-------------
        %------------Number
        hf(figind)=figure(80+figind);movegui(hf(figind),'south');
        yy=sum(dep,2)./sum(P(1:end-1,:),2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Depressed Fraction');drawnow;hold on;
        figind=figind+1;
        
        %------------Relative amount
        hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
        S_dep=S_change.*dep;
        yy=sum(S_dep./S_mean,2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Relative Depressed Weight');drawnow;hold on;
        figind=figind+1;
        
        %------------Avg amount
        hf(figind)=figure(80+figind);movegui(hf(figind),'southwest');
        S_dep=S_change.*dep;
        yy=sum(S_dep,2)./sum(dep,2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,0.05*mean(yy)+yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Avg amount of Depression');drawnow;hold on;
        figind=figind+1;
        
        %{
        %Weight change fraction---
        hf(figind)=figure(80+figind);movegui(hf(figind),'southeast');
        yy=sum(changed,2)./sum(P(1:end-1,:),2);
        plot(tt,yy,'o-','Color',anc);
        text(tt,1.05*yy,tt_txt,'HorizontalAlignment','center','Color',anc,...
                'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Changed Fraction');drawnow;hold on;
        figind=figind+1
        %}
        
        %Oscillation---------------
        P=f.P(w);
        S_d2=nan(size(w(2:end-1,:)));
        P_bouton=nan(size(w(2:end-1,:),1),1);
        for i=2:(size(w,1)-1)
            S_d2(i-1,:)=sum(([-0.5;1;-0.5]*ones(1,size(w,2))).*w(i-1:i+1,:),1).*(P(i,:));
            P_bouton(i-1)=sum(P(i,:));
        end
        
        ttt=analysis_aligntime(Dat,[],'start');
        ttt=ttt(2:end-1);
        ttt_txt=cell(size(w,1)-2,1);
        for i=1:numel(ttt_txt)
            ttt_txt{i}=[Dat.L.im_id{i},Dat.L.im_id{i+1},Dat.L.im_id{i+2}];
        end
        
        yy=sum(abs(S_d2),2)./P_bouton;
        hf(figind)=figure(80+figind);movegui(hf(figind),'northeast');
        plot(ttt,yy,'o-','Color',anc);
        text(ttt,0.05*mean(yy)+yy,ttt_txt,'HorizontalAlignment','center','Color',anc,...
            'FontName',get(groot,'DefaultAxesFontName'),'FontSize',0.8*get(groot,'DefaultAxesFontSize'));
        xlabel('Time (days)');ylabel('Oscillation Fraction');drawnow;hold on;
        
        analysis_maindataset_MakePretty(hf);
        
    case 'Survival Fraction'
        %Plot simple survival curves
        
        W=Dat.w;
        
        fprintf(['\n--------------Animal',num2str(Dat.anid),'--------------\n'])
        fitind=2:(size(W,1)-1);
        %fitind=2:(size(W,1));
        t_interval=4;
        t_all=((1:size(W,1))'-1)*t_interval;
        
        sfrac=nan(size(W,1));
        snum=nan(size(W,1));
        snumvar=nan(size(W,1));
        sfrac_error=nan(size(W,1));
        
        binedges=[1.5,2.5,4.5,8.5,16.5];
        bincen=(binedges(1:end-1)+binedges(2:end))./2;
        snum_siz=nan(numel(bincen),size(W,1),size(W,1));
        sfrac_siz=nan(numel(bincen),size(W,1),size(W,1));
        snumvar_siz=nan(numel(bincen),size(W,1),size(W,1));
        sfrac_error_siz=nan(numel(bincen),size(W,1),size(W,1));
        
        for i=1:size(W,1)-1
            P_cut=f.P(W(i:end,:));
            psurvived=cumprod(P_cut,1);
            %psurvived=bsxfun(@times,P_cut,P_cut(1,:));
            snum(1:size(P_cut,1),i)=nansum(psurvived,2);
            sfrac(1:size(P_cut,1),i)=snum(1:size(P_cut,1),i)./snum(1,i);
            %snumvar(1:size(P_cut,1),i)=nansum(psurvived.*(1-bsxfun(@rdivide,psurvived,P_cut(1,:))),2);
            %diving by P_cut(1,:) because error in survival at time t=0 must be zero.
            sfrac_error(1:size(P_cut,1),i)=nansum(psurvived.*(1-psurvived),2).^0.5./snum(1,i);
            %sfrac_error(1:size(P_cut,1),i)=snumvar(1:size(P_cut,1),i).^0.5./snum(1,i);
            
            for j=1:numel(bincen)
                %Probability that bouton is in a given size range
                P_insizrange=f.P(W(i,:),binedges(j))-f.P(W(i,:),binedges(j+1));
                
                %P that it survives in later sessions, given that it
                %was in the specified size range: P in first
                %session is replaced by P to be in size range
                Psizsurvived=cumprod([P_insizrange;P_cut(2:end,:)],1);
                snum_siz(j,1:size(P_cut,1),i)=nansum(Psizsurvived,2);
                sfrac_siz(j,1:size(P_cut,1),i)=snum_siz(j,1:size(P_cut,1),i)./snum_siz(j,1,i);
                snumvar_siz(j,1:size(P_cut,1),i)=nansum(Psizsurvived.*(1-bsxfun(@rdivide,Psizsurvived,P_insizrange)),2);
                sfrac_error_siz(j,1:size(P_cut,1),i)=snumvar_siz(j,1:size(P_cut,1),i).^0.5./snum(1,i);
            end
        end
        
        %{
        sfrac_data=sfrac(:,1);
        sfrac_err=sfrac_error(:,1);
        disp('Data is not averaged. Error bars from Poisson model')
        %}
        
        %
        figure;plot(sfrac);
        title(['Animal ',num2str(Dat.anid)]);
        %caxis([0 1]),colorbar;
        %axis equal;axis tight;
        drawnow;
        %}
        
        sfrac_data=nanmean(sfrac,2);
        sfrac_err=nanstd(sfrac,[],2);
        sfrac_err=sfrac_err./(sum(isnan(sfrac_error),2)).^0.5;
        sfrac_err(sfrac_err==0)=eps;%For fitting
        disp('Data is averaged over starting points. Errors bars are standard error of the mean.')
        %}
        
        %Survival curves for boutons of different initial size-------------
        sfrac_mean_siz=nan(numel(bincen),size(W,1));
        sfrac_num_siz=nan(numel(bincen),size(W,1));
        sfrac_std_siz=nan(numel(bincen),size(W,1));
        for j=1:numel(bincen)
            sfrac_mean_siz(j,:)=nanmean(squeeze(sfrac_siz(j,:,:)),2);
            sfrac_num_siz(j,:)=nanmean(squeeze(snum_siz(j,:,:)),2);
            %Standard error of the mean:
            sfrac_std_siz(j,:)=(nanstd(squeeze(sfrac_siz(j,:,:)),[],2))./(sum(~isnan(squeeze(sfrac_error_siz(j,:,:))),2)).^0.5;
            
            %{
            %To not average
            temp=squeeze(sfrac_siz(j,:,:));
            sfrac_mean_siz(j,:)=temp(:,1);
            temp=squeeze(snum_siz(j,:,:));
            sfrac_num_siz(j,:)=temp(:,1);
            temp=squeeze(sfrac_error_siz(j,:,:));
            sfrac_std_siz(j,:)=temp(:,1);
            %}
        end
        sfrac_mean_siz=sfrac_mean_siz';
        sfrac_std_siz=sfrac_std_siz';
        sfrac_num_siz=sfrac_num_siz';
        %------------------------------------------------------------------
        
        
        %Fitting for regular survival fraction:----------------------------
        %a, t1, t2 are evaluated separately for each animal
        s=fittype('a*exp(-x/t1)+(1-a)*exp(-x/t2)');
        [fitsfrac,goodness]=fit(t_all(fitind),sfrac_data(fitind),s,'StartPoint',[0.5,10,200],'Weights',1./(sfrac_err(fitind)+eps));
        Fvars=coeffnames(fitsfrac);
        Fvals=coeffvalues(fitsfrac);
        Fints=confint(fitsfrac);
        
        %
        fprintf('\n');
        disp('Fitting survival curve independently for this animal')
        fprintf('\n');
        display(['Adj. R sq: ',num2str(goodness.adjrsquare)])
        fprintf('\n');
        for i=1:numel(Fvals)
            fprintf('%5s %10.3f %10.3f %10.3f \n',Fvars{i},Fvals(i),Fints(1,i),Fints(2,i))
        end
        
        
        figure(2001),movegui(2001,'northwest');hold on
        errorbar(t_all,sfrac_data,sfrac_err,'.','MarkerEdgeColor','none','Color',anc);
        plot(t_all,sfrac_data,'.','MarkerSize',5,'Color',anc);
        plot(t_all,fitsfrac(t_all),'-','Color',anc);
        ax = gca;ax.XTick = ((1:size(sfrac,1))'-1)*t_interval;
        xlim([-2,(size(sfrac,1)-1)*t_interval+2]);ylim([-0.1,1.1]);
        xlim([-2 70]);ylim([-0.02 1.02]);hax=gca;hax.XTick=0:8:72;
        xlabel('Time (days)');ylabel('Survival fraction')
        axis square;box on;grid on;
        drawnow
        
        %Out survival curve data from individual animals
        Out.sfrac=sfrac_data;
        Out.t=t_all;
        Out.e=sfrac_err;
        Out.fitind=fitind;
        %{
        %Script to fit data from 4 animals with the same t_fast and t_slow
        
        %}
        %------------------------------------------------------------------
        
        %{
        %Fit size dependent survival curves one-by-one---------------------
        fitind2=fitind;
        dat=[];
        
        cc=parula(numel(bincen));
        for j=1:numel(bincen)
            %Display individual fits for boutons of different initial sizes----
            [fitsfrac,goodness]=fit(t_all(fitind2),sfrac_mean_siz(fitind2,j),s,'StartPoint',[0.5,7.5,90],'Lower',[0 7 0],'Upper',[1 8 inf],'Weights',1./sfrac_std_siz(fitind2,j));
            
            Fvars=coeffnames(fitsfrac);
            Fvals=coeffvalues(fitsfrac);
            Fints=confint(fitsfrac);
            
            display(['For Size: ',num2str(binedges(j)),' to ',num2str(binedges(j+1))])
            fprintf('\n');
            display(['Adj. R sq: ',num2str(goodness.adjrsquare)])
            fprintf('\n');
            for i=1:numel(Fvals)
                fprintf('%5s %10.3f %10.3f %10.3f \n',Fvars{i},Fvals(i),Fints(1,i),Fints(2,i))
            end
            
            %Solve for bouton halflife-------------------------------------
            fun = @(x) fitsfrac.a*exp(-x/fitsfrac.t1)+(1-fitsfrac.a)*exp(-x/fitsfrac.t2)-0.5;
            opt=optimoptions('fsolve','Display','none');
            halflife=fsolve(fun,10,opt);
            dat=[dat;[bincen(j),halflife,fitsfrac.a,fitsfrac.t1,fitsfrac.t2]];
            
            
            %Plot each curve with independent fits-------------------------
            figure(2002),hold on
            errorbar(t_all,sfrac_mean_siz(:,j),sfrac_std_siz(:,j),'.','MarkerEdgeColor','none','Color',cc(j,:));
            plot(t_all,sfrac_mean_siz(:,j),'.','MarkerSize',10,'Color',cc(j,:));
            plot(t_all,fitsfrac(t_all),'--','Color',cc(j,:));
            ax = gca;ax.XTick = ((1:size(sfrac,1))'-1)*t_interval;
            xlim([-2,(size(sfrac,1)-1)*t_interval+2]);ylim([-0.1,1.1])
            xlim([-2 70]);ylim([-0.02 1.02]);hax=gca;hax.XTick=0:8:72;
            xlabel('Time (days)');ylabel('Survival fraction')
            axis square;box on;
            drawnow
        end
        %End of fitting size dependent survival curves one-by-one-----------
        %}
        
        %
        %Obtain survival curves for sizes--------------------------------
        fitind2=fitind;
        y_fit=nan(numel(bincen),numel(fitind2));
        t_orig_fit=y_fit;
        e_fit=y_fit;
        t_temp_fit=y_fit;
        id_fit=y_fit;
        
        keepind=1:(size(sfrac_mean_siz,1)-2);
        y_plot=nan(numel(bincen),numel(keepind));
        t_orig_plot=y_plot;
        e_plot=y_plot;
        t_temp_plot=y_plot;
        id_plot=y_plot;
        
        for j=1:numel(bincen)
            y_fit(j,:)=sfrac_mean_siz(fitind2,j);
            e_fit(j,:)=sfrac_std_siz(fitind2,j);
            t_orig_fit(j,:)=t_all(fitind2);
            t_temp_fit(j,:)=(j-1)*100+t_all(fitind2);
            id_fit(j,:)=(j)*ones(numel(fitind2),1);
            
            y_plot(j,:)=sfrac_mean_siz(keepind,j);
            e_plot(j,:)=sfrac_std_siz(keepind,j);
            t_orig_plot(j,:)=t_all(keepind);
            t_temp_plot(j,:)=(j-1)*100+t_all(keepind);
            id_plot(j,:)=(j)*ones(numel(sfrac_mean_siz(keepind,j)),1);
        end
        
        %Simultaneous fitting of size-dependent survival curves with the
        %same t_fast-------------------------------------------------------
        F1=@(a1,ts,t1,x) (a1*exp(-x/ts)+(1-a1)*exp(-x/t1));
        F2=@(a2,ts,t2,x) (a2*exp(-(x-100)/ts)+(1-a2)*exp(-(x-100)/t2));
        F3=@(a3,ts,t3,x) (a3*exp(-(x-200)/ts)+(1-a3)*exp(-(x-200)/t3));
        F4=@(a4,ts,t4,x) (a4*exp(-(x-300)/ts)+(1-a4)*exp(-(x-300)/t4));
        
        F=@(a1,a2,a3,a4,ts,t1,t2,t3,t4,x) F1(a1,ts,t1,x).*(x<100 & x>=0)+...
            F2(a2,ts,t2,x).*(x<200 & x>=100)+...
            F3(a3,ts,t3,x).*(x<300 & x>=200)+...
            F4(a4,ts,t4,x).*(x<400 & x>=300);
        
        [fitsfrac,goodness,~]=fit(t_temp_fit(:),y_fit(:),F,...
            'StartPoint',[0.5,0.5,0.5,0.5,4,100,100,100,100],...
            'Lower',[0,0,0,0,0,0,0,0,0],...
            'Upper',[1,1,1,1,100,inf,inf,inf,inf],...
            'Weights',1./e_fit(:));
        
        Fvars=coeffnames(fitsfrac);
        Fvals=coeffvalues(fitsfrac);
        Fints=confint(fitsfrac);
        
        %{
        fprintf('\n');
        disp('Simultaneous fitting of survival curves with different initial size ')
        fprintf('\n');
        display(['Adj. R sq: ',num2str(goodness.adjrsquare)])
        fprintf('\n');
        for i=1:numel(Fvals)
            fprintf('%5s %10.3f %10.3f %10.3f \n',Fvars{i},Fvals(i),Fints(1,i),Fints(2,i))
        end
        %}
        
        %Plot simultaneous fits-------------------------------------------------------------
        hfig=2003+double(find(anind)>3);
        figure(hfig),
        mface={[1 1 1],[1 1 1],[1 1 1],[1 1 1],anc};
        %mstring={'.','.','.','.'};
        mstring={'^','s','*','d'};
        for i=1:4
            errorbar(t_all(1:end-2),sfrac_mean_siz(1:end-2,i),sfrac_std_siz(1:end-2,i),'.','Color',anc,'MarkerEdgeColor','none'),hold on
            plot(t_all(1:end-2),sfrac_mean_siz(1:end-2,i),'LineStyle','none','Marker',mstring{i},'Color',anc,'MarkerSize',7,'MarkerFaceColor',mface{i}),hold on
            plot(t_orig_plot(id_plot==i),fitsfrac(t_temp_plot(id_plot==i)),'--','Color',anc);
        end
        xlabel('Time (days)');ylabel('Survival Fraction');
        ylim([-0.1 1.1]);xlim([-2,70])
        hax=gca;hax.XTick=0:8:70;
        axis square;box on
        if find(anind)>3
            movegui(hfig,'north');
        else
            movegui(hfig,'northeast');
        end
        drawnow;
        %}
        
        %Variablesfor plotting
        a=Fvals(1:4);
        t_long=Fvals(6:9);
        a_err_L=abs(Fints(1,1:4)-a);
        a_err_U=abs(Fints(2,1:4)-a);
        t_err_L=abs(Fints(1,6:9)-t_long);
        t_err_U=abs(Fints(2,6:9)-t_long);
        
        %Plot fractions and times as a function of initial bouton size---------
        figure(2006),hold on;movegui(2006,'south')
        errorbar(bincen(:),t_long,t_err_L,t_err_U,'Marker','none','Color',anc)
        plot(bincen(:),t_long,'-','Color',anc,'MarkerSize',10)
        for i=1:4
            plot(bincen(i),t_long(i),mstring{i},'Color',anc,'MarkerSize',7,'MarkerFaceColor',mface{i}),hold on
        end
        ylim([0 500])
        xlim([0 binedges(end)])
        hax=gca;hax.XTick=binedges;
        xlabel('Size (Bin edges)');ylabel('\tau_{slow} (days)')
        axis square;box on;grid on;
        drawnow
        
        figure(2005),hold on;movegui(2005,'southeast')
        errorbar(bincen(:),a,a_err_L,a_err_U,'Marker','none','Color',anc)
        plot(bincen(:),a,'-','Color',anc,'MarkerSize',10)
        for i=1:4
            plot(bincen(i),a(i),mstring{i},'Color',anc,'MarkerSize',10,'MarkerFaceColor',mface{i}),hold on
        end
        ylim([0 1])
        xlim([0 binedges(end)])
        hax=gca;hax.XTick=binedges;
        xlabel('Size (Bin center)');ylabel('Fast fraction, a')
        axis square;box on;grid on;
        drawnow
        %}
        
    case 'Transition matrix'
        %{
        %Generate transition matrix in a window
        binedges=[-inf,[2:6:20],inf];
        T=nan(numel(binedges)-1,numel(binedges)-1,(size(Dat.w,1)-2));
        for i=1:(size(Dat.w,1)-2)
            keepind=any(f.P(Dat.w(i:i+2,:))>0.5,1);
            wi=Dat.w(i:i+1,keepind);
            wf=Dat.w(i+1:i+2,keepind);
            T(:,:,i)=fT(wi,wf,binedges,D.anid);
        end
        tt=analysis_aligntime(Dat,[],'start');
        im3dscroll(T,[],tt)
        axis square
        caxis([0 0.6])
        drawnow;
        %}
        
        %Generate transition matrix for the full dataset
        T=fT(Dat.w(1:end-1,:),Dat.w(2:end,:),[-inf,[2:2:20],inf],Dat.anid);
        
    case  'New Bouton Survival Fraction'
        fprintf(['\n--------------Animal',num2str(Dat.anid),'--------------\n'])
        siglevel=0; %siglevel~=0 => thresholded probabilities
        
        W=Dat.w;
        t_interval=4;
        t_all=((1:(size(W,1)-1))-1)'*t_interval;
        fitind=2:(numel(t_all)-1);
        
        sfrac=nan(size(W,1)-1);
        snum=nan(size(W,1)-1);
        snumvar=nan(size(W,1)-1);
        sfrac_error=nan(size(W,1)-1);
        
        for i=1:size(W,1)-1
            P_cut=f.P(W(i:end,:));
            P_cut(1,:)=1-P_cut(1,:); %Probability to be absent in the first session
            if siglevel~=0
                P_cut=P_cut>=siglevel;
            end
            psurvived=cumprod(P_cut,1);
            psurvived=psurvived(2:end,:);
            %psurvived=bsxfun(@times,P_cut,P_cut(1,:));
            snum(1:size(psurvived,1),i)=nansum(psurvived,2);
            sfrac(1:size(psurvived,1),i)=snum(1:size(psurvived,1),i)./snum(1,i);
            snumvar(1:size(psurvived,1),i)=nansum(psurvived.*(1-bsxfun(@rdivide,psurvived,psurvived(1,:))),2);
            %diving by psurvived(1,:) because error in survival at time t=0 must be zero.
            sfrac_error(1:size(psurvived,1),i)=nansum(psurvived.*(1-psurvived),2).^0.5./snum(1,i);
            %sfrac_error(1:size(P_cut,1),i)=snumvar(1:size(P_cut,1),i).^0.5./snum(1,i);
        end
        
        %{
    sfrac_data=sfrac(:,1);
    sfrac_err=sfrac_error(:,1);
    disp('Data is not averaged. Error bars from Poisson model')
        %}
        
        nboutons=mean(sum(f.P(W),2));
        figure,imagesc(snum./nboutons),title(Dat.anid);caxis([0 0.2]);colorbar;
        title(['Animal: ',num2str(Dat.anid)]);box on;
        drawnow;
        
        sfrac_data=nanmean(sfrac,2);
        sfrac_err=nanstd(sfrac,[],2);
        sfrac_err=sfrac_err./(sum(isnan(sfrac_error),2)).^0.5;
        sfrac_err(sfrac_err==0)=eps;%For fitting
        disp('Data is averaged over starting points. Errors bars are standard error of the mean.')
        %}
        
        %Fitting for regular survival fraction:--------------------------------
        %a, t1, t2 are evaluated separately for each animal
        s=fittype('a*exp(-x/t1)+(1-a)*exp(-x/t2)');
        [fitsfrac,goodness]=fit(t_all(fitind),sfrac_data(fitind),s,'StartPoint',[0.5,5,90],'Weights',1./sfrac_err(fitind));
        Fvars=coeffnames(fitsfrac);
        Fvals=coeffvalues(fitsfrac);
        Fints=confint(fitsfrac);
        
        fprintf('\n');
        disp('Fitting survival curve independently for this animal')
        fprintf('\n');
        display(['Adj. R sq: ',num2str(goodness.adjrsquare)])
        fprintf('\n');
        for i=1:numel(Fvals)
            fprintf('%5s %10.3f %10.3f %10.3f \n',Fvars{i},Fvals(i),Fints(1,i),Fints(2,i))
        end
        
        figure(2001),movegui(2001,'northwest');hold on
        errorbar(t_all,sfrac_data,sfrac_err,'.','MarkerEdgeColor','none','Color',anc);
        plot(t_all,sfrac_data,'.','MarkerSize',10,'Color',anc);
        plot(t_all,fitsfrac(t_all),'-','Color',[anc,0.2]);
        ax = gca;ax.XTick = ((1:size(sfrac,1))'-1)*t_interval;
        xlim([-2,(size(sfrac,1)-1)*t_interval+2]);ylim([-0.1,1.1]);
        xlim([-2 70]);ylim([-0.02 1.02]);hax=gca;hax.XTick=0:8:72;
        xlabel('Time (days)');ylabel('Survival fraction')
        axis square;box on;
        drawnow
        
        %Fix this along with analysis_maindatasetwrapper.
        Out.sfrac=sfrac_data;
        Out.t=t_all;
        Out.e=sfrac_err;
        Out.fitind=fitind;
        %------------------------------
        
    case 'SpatialCorr'
        %Calculate spatial correlation in weight---------------------------
        
        cthr=0.8;
        dthr=5; %For cumulative distribution plots
        small_d_thr=1; %ignore boutons that are closer than this distance
        bs_iter=100; %number of bootstrap iterations
        
        %X=diff(Dat.w,1,1);
        X=Dat.w;
        
        %X=bsxfun(@minus,X,mean(X,1));Xstd=std(X,1,1);
        %cc=(X'*X)./(Xstd'*Xstd)./size(X,1);%same as corrcoef
        cc=corrcoef(X);
        tdbinedges=2:4:60;
        
        %Calculate all-to-all bouton distances
        %Set nans for boutons that belong to different axons
        %Set nans for boutons that are too close
        axonidmat=bsxfun(@eq,Dat.ax_id',Dat.ax_id);
        td=abs(bsxfun(@minus,Dat.d',Dat.d));
        td(~triu(true(size(td)),1))=nan;
        td(~axonidmat)=nan;
        td(td<small_d_thr)=nan;
        %cc(isnan(td))=nan;
        
        td=td(:);
        td_bin_id=nan(size(td));
        for i=1:(numel(tdbinedges))
            td_bin_id(td>tdbinedges(i))=i; %Assign label i to interbouton distances that lie within the i-th bin
        end
        meantd=nan(numel(tdbinedges),1);
        stdtd=nan(numel(tdbinedges),1);
        meancc_td=nan(numel(tdbinedges),1);
        stdcc_td=nan(numel(tdbinedges),1);
        counts_td=nan(numel(tdbinedges),1);
        for i=1:(numel(tdbinedges))
            meantd(i)=mean(td(td_bin_id==i));
            stdtd(i)=std(td(td_bin_id==i));
            meancc_td(i)=mean(cc(td_bin_id==i));
            stdcc_td(i)=std(cc(td_bin_id==i),1,1)./(sum(td_bin_id==i)).^0.5;
            counts_td(i)=sum(td_bin_id==i);
        end
        
        axonid=unique(Dat.ax_id);
        %Initialize bins
        bs_sumtd=zeros(numel(tdbinedges),1);
        bs_stdtd=nan(numel(tdbinedges),1);
        bs_sumcc_td=zeros(numel(tdbinedges),1);
        bs_stdcc_td=nan(numel(tdbinedges),1);
        bs_counts_td=zeros(numel(tdbinedges),1);
        
        %Shuffling position along axon for bootstrap
        for itr=1:bs_iter
            bsd=Dat.d;
            for ax=1:numel(axonid)
                tempd=Dat.d(Dat.ax_id==axonid(ax));
                bsd(Dat.ax_id==axonid(ax))=tempd(randperm(numel(tempd)));
            end
            
            %Bootstrap interbouton distances
            bsd_td=abs(bsxfun(@minus,bsd(:),bsd(:)'));
            bsd_td(~triu(true(size(bsd_td)),1))=nan;
            bsd_td(~axonidmat)=nan;
            bsd_td=bsd_td(:);
            
            %Bootstrap binning
            bs_td_bin_id=nan(size(bsd_td));
            for i=1:(numel(tdbinedges))
                bs_td_bin_id(bsd_td>tdbinedges(i))=i;
            end
            
            for i=1:(numel(tdbinedges))
                bs_sumtd(i)=bs_sumtd(i)+sum(bsd_td(bs_td_bin_id==i));
                bs_stdtd(i)=std(bsd_td(bs_td_bin_id==i));
                bs_sumcc_td(i)=bs_sumcc_td(i)+sum(cc(bs_td_bin_id==i));
                bs_stdcc_td(i)=std(cc(bs_td_bin_id==i),1,1)./(sum(bs_td_bin_id==i)).^0.5;
                bs_counts_td(i)=bs_counts_td(i)+sum(bs_td_bin_id==i);
            end
        end
        
        figure(7001),hold on
        if isempty(get(gca,'Children'))
            plot([min(tdbinedges),max(tdbinedges)],[0 0],'-k')
        end
        %Data plot
        xx=meantd(1:end-1);
        yy=meancc_td(1:end-1);
        ee=stdcc_td(1:end-1);
        errorbar(xx,yy,ee,'Marker','none','Color',anc,'LineStyle','none','LineWidth',2);
        plot(xx,yy,'-s','MarkerSize',5,'Color',anc,'MarkerFaceColor',anc);
        Out.xx=xx;
        Out.yy=yy;
        Out.ee=ee;
        Out.counts=counts_td(1:end-1);
        
        %Bootstrap plot
        xx=[bs_sumtd(1)./bs_counts_td(1), bs_sumtd(end-1)./bs_counts_td(end-1)];
        yy=[mean(bs_sumcc_td(1:end-1)./bs_counts_td(1:end-1)),mean(bs_sumcc_td(1:end-1)./bs_counts_td(1:end-1))];
        plot(xx,yy,'--','Color',anc);
        xlabel('Distance along axon \mu{m}');ylabel('Correlation coefficient')
        ylim([-0.2 0.2]),xlim([min(tdbinedges),max(tdbinedges)])
        box on,axis square,
        set(gca,'FontName','Calibri','FontSize',15)
        drawnow;
        
        Out.bsxx=xx;
        Out.bsyy=yy;
        Out.bsee=ee;
        Out.bscounts=[mean(bs_counts_td(1:end-1)),mean(bs_counts_td(1:end-1))];
        %{
        %Show correlated boutons on traces---------------------------------
        td=abs(bsxfun(@minus,Dat.d',Dat.d));
        td(~triu(true(size(td)),1))=nan;
        td(~axonidmat)=nan;
        [a,b]=find(cc>cthr);
        AM=zeros(size(cc));
        ind=sub2ind(size(AM),a,b);
        AM(ind)=1;AM=LabelTreesAM(AM);
        lbl=unique(AM(AM>0));
        ind=unique([a,b]);
        ttt=5;
        analysis_plotboutonsontrace(Dat,Animal,[],ttt);
        cc=lines(numel(lbl));
        for l=1:numel(lbl)
            [a,b]=find(AM==lbl(l));
            ind=unique([a(:);b(:)]);
            plot3(Dat.ry(ttt,ind),Dat.rx(ttt,ind),312*ones(size(Dat.rx(ttt,ind))),'o','Color',cc(l,:),'MarkerSize',6,'MarkerFaceColor',cc(l,:),'LineStyle','none')
        end
        %Inspect highly correlated boutons:
        %analysis_plotboutonsonIM(D,Animal,ind);
        %}
        
        %{
        %Calculate correlation coefficients histograms---------------------
        X=Dat.w;
        corr_w=corrcoef(X);
        
        X=diff(Dat.w,1,1);
        corr_dw=corrcoef(X);
        
        axonidmat=bsxfun(@eq,Dat.ax_id',Dat.ax_id);
        td=abs(bsxfun(@minus,Dat.d',Dat.d));
        
        %Set entries below diagonal and those not on the same axon to nan
        td(~triu(true(size(td)),1))=nan;
        td(~axonidmat)=nan;
        td(td<small_d_thr)=nan;
        
        corr_w(isnan(td))=nan;
        corr_dw(isnan(td))=nan;
        
        binedges=-1:0.02:1;
        %Correlation of difference in Intensities vs. Distance along trace
        figure(60+find(anind))
        histogram(corr_dw(~isnan(corr_dw)),binedges,'Normalization','cdf','DisplayStyle','stairs','EdgeColor',[0.2 0.2 0.2]),hold on;
        histogram(corr_dw(td<=dthr),binedges,'Normalization','cdf','DisplayStyle','stairs','EdgeColor',anc);
        xlim([-1 1]),ylim([0 1])
        xlabel('Correlation coefficient for \Delta{w}');ylabel('Cumulative density function')
        axis square;box on;
        drawnow;
        
        %Correlation of Intensities vs. Distance along trace
        figure(70+find(anind));
        histogram(corr_w(~isnan(corr_w)),binedges,'Normalization','cdf','DisplayStyle','stairs','EdgeColor',[0.2 0.2 0.2]),hold on;
        histogram(corr_w(td<=dthr),binedges,'Normalization','cdf','DisplayStyle','stairs','EdgeColor',anc);
        xlim([-1 1]),ylim([0 1])
        xlabel('Correlation coefficient for w');ylabel('Cumulative density function')
        axis square;box on;
        drawnow;
        %}
        
        
    case 'TemporalCorr'
        
        lag=0:1:5;
        %siglevel=0.5;
        niter=1000; %Number of bootstrap iterations
        
        X=Dat.w;
        
        %xmat=diff(X,1,1);
        xmat=X;
        
        %Calculate correlation coeff
        cclag=nan(length(lag),size(xmat,2));
        for l=1:length(lag)
            x1=xmat(1:size(xmat,1)-lag(l),:);
            x2=xmat(1+lag(l):end,:);
            x1=bsxfun(@minus,x1,mean(x1,1));
            x2=bsxfun(@minus,x2,mean(x2,1));
            x1std=std(x1,1,1);
            x2std=std(x2,1,1);
            cclag(l,:)=sum(x1.*x2,1)./size(x1,1)./((x1std.*x2std)+eps);
        end
        
        %Generate bootstraps
        dat_size=size(xmat);
        bs_size=dat_size;
        bs_size(2)=dat_size(2)*niter;
        xmatbs=nan(bs_size);
        for iter=1:niter
            from_ind=(iter-1)*(dat_size(2))+1;
            to_ind=iter*dat_size(2);
            xmatbs(:,from_ind:to_ind)=shufflerowswithincols(xmat);
            %xmatbs(:,from_ind:to_ind)=rand(size(xmat));
        end
        
        %Calculate correlation coeff for bootstrap
        cclagbs=nan(length(lag),size(xmatbs,2));
        for l=1:length(lag)
            x1=xmatbs(1:size(xmatbs,1)-lag(l),:);
            x2=xmatbs(1+lag(l):end,:);
            x1=bsxfun(@minus,x1,mean(x1,1));
            x2=bsxfun(@minus,x2,mean(x2,1));
            x1std=std(x1,1,1);
            x2std=std(x2,1,1);
            cclagbs(l,:)=sum(x1.*x2,1)./(size(x1,1))./((x1std.*x2std+eps));
        end
        
        figure(30),hold on;ha=gca;
        if isempty(ha.Children)
            %plot([0,(lag(end)+1)*4],zeros(2,1),'-k')
        end
        
        xx=lag*4;
        yy=nanmean(cclag,2);
        ee=nanstd(cclag,1,2)./sum(~isnan(cclag),2).^0.5;
        
        Out.xx=xx;
        Out.yy=yy;
        Out.ee=ee;
        Out.counts=mean(sum(~isnan(cclag),2));
        bsyy=nanmean(cclagbs,2);
        Out.bsxx=xx;
        Out.bsyy=bsyy;
        
        errorbar(xx,yy,ee,'Marker','none','Color',anc,'LineWidth',2),hold on
        plot(xx,bsyy,'s','MarkerSize',10,'LineStyle','none','MarkerFaceColor',anc,'MarkerEdgeColor','none'),hold on
        plot(xx,bsyy,'--','MarkerSize',10,'LineWidth',2,'MarkerFaceColor',anc,'MarkerEdgeColor','none','Color',anc),hold on
        xlim([-0.1 4*(lag(end)+1)]);ylim([-0.5 1.1]);
        ha.XTick = 4*((lag(1)-1):(lag(end)+1));
        xlabel('Lag (days)'),ylabel('Correlation coefficient');
        axis square,box on;
        
        
        
        
    case 'TemporalCorr2'
        
        btnpresent_thr=0.5;
        
        %Correlation of bouton weights in successive sessions resolved by
        %bouton weight and time.
        binedges=[1,2,4,8,16,32];
        bincenters=(binedges(1:end-1)+binedges(2:end))./2;
        binsiz=(binedges(2:end)-binedges(1:end-1));
        
        W=Dat.w;
        P=f.P(W);
        bouton_size=(W(1:end-1,:)+W(2:end,:))./2;
        ispresent_both=(P(1:end-1,:).*P(2:end,:))>btnpresent_thr;
        
        corr_binned=nan(size(W,1)-1,numel(bincenters));
        n_binned=nan(size(W,1)-1,numel(bincenters));
        for t=1:(size(W,1)-1)
            for b=1:numel(bincenters)
                ind=bouton_size(t,:)>=binedges(b) & bouton_size(t,:)<binedges(b+1) & ispresent_both(t,:);
                n_binned(t,b)=sum(ind);
                cc=corrcoef(W(t,ind),W(t+1,ind));
                if n_binned(t,b)>1
                    corr_binned(t,b)=cc(1,2);%.*std(W(t,ind),[],2).*std(W(t+1,ind),[],2);
                else
                    corr_binned(t,b)=nan;
                end
            end
        end
        
        [tt] = analysis_aligntime(D,[],'start');
        figure(2001+find(anind));
        imagesc(corr_binned);
        xlabel('Bouton weight');ylabel('Time (days)');
        title(['Animal ',num2str(Dat.anid)]);
        
        for t=1:(size(W,1)-1)
            for b=1:numel(bincenters)
                text(b,t,num2str(n_binned(t,b)),'FontSize',12,'Color',[1 1 1],'HorizontalAlignment','center')
            end
        end
        
        set(gca,'XTick',0.5:1:numel(binedges)-0.5);set(gca,'XTickLabel',binedges);
        set(gca,'YTick',1:1:size(corr_binned,1));set(gca,'YTickLabel',round(tt(2:end),1));
        colormap('jet');colorbar;caxis([-1 1]);
        axis xy;axis equal;axis tight;box on;drawnow;
        
        %------------------------------------------------------------------
        %Correlation of weight changes resolved by bouton size and time
        boutondef_thr=0.5;
        W=Dat.w;
        P=f.P(W);
        
        bouton_size=W(2:end-1,:);
        dw=diff(W,1,1);
        ispresent_both=(P(1:end-1,:).*P(2:end,:))>boutondef_thr;
        
        dw(~ispresent_both)=nan;
        C=zeros(size(dw(1:end-1,:)));
        C(isnan(dw(1:end-1,:)) | isnan(dw(2:end,:)))=nan;
        
        corr_binned=nan(size(C,1),numel(bincenters));
        n_binned=nan(size(C,1),numel(bincenters));
        for t=1:size(C,1)
            for b=1:numel(bincenters)
                ind=bouton_size(t,:)>=binedges(b) & bouton_size(t,:)<binedges(b+1) & ~isnan(C(t,:));
                n_binned(t,b)=sum(ind);
                cc=corrcoef(dw(t,ind),dw(t+1,ind));
                corr_binned(t,b)=cc(1,2);
            end
        end
        
        figure(3001+find(anind))
        imagesc(corr_binned)
        xlabel('Bouton weight');ylabel('Time (days)');
        
        for t=1:size(C,1)
            for b=1:numel(bincenters)
                text(b,t,num2str(n_binned(t,b)),'FontSize',12,'Color',[1 1 1],'HorizontalAlignment','center')
            end
        end
        
        set(gca,'XTick',0.5:1:numel(binedges));set(gca,'XTickLabel',binedges)
        set(gca,'YTick',1:1:size(C,1));set(gca,'YTickLabel',(1:1:size(C,1))+1)
        title(['Animal ',num2str(Dat.anid)]);
        colorbar;caxis([-0.7 -0.1]);
        axis xy;axis equal;axis tight;box on;drawnow;
        
    case 'Maps'
        %Draws maps to compare circuit at each time point to any later time
        %point
        
        w=Dat.w;
        sig_level=0;
        
        n=size(w,1);
        
        added=nan(n,n);
        S_added=nan(n,n);
        
        elim=nan(n,n);
        S_elim=nan(n,n);
        
        tor=nan(n,n);
        
        pot=nan(n,n);
        S_pot=nan(n,n);
        
        dep=nan(n,n);
        S_dep=nan(n,n);
        
        H=nan(n,n);
        for i=(1:size(w,1))
            for j=1:(size(w,1))
                
                S_change=abs(w(i,:)-w(j,:));
                S_mean=(w(i,:)+w(j,:))./2;
                
                if sig_level==0
                    
                    P=f.P(w);
                    Nb=sum(P(i,:)+P(j,:))./2;
                    
                    added(i,j)=sum(f.P_add(w(i,:),w(j,:)))./Nb;
                    elim(i,j)=sum(f.P_elim(w(i,:),w(j,:)))./Nb;
                    tor(i,j)=added(i,j)+elim(i,j);
                    pot(i,j)=sum(f.P_pot(w(i,:),w(j,:)))./Nb;
                    dep(i,j)=sum(f.P_dep(w(i,:),w(j,:)))./Nb;
                    
                    S_added(i,j)=sum(S_change.*f.P_add(w(i,:),w(j,:)))./sum(f.P_add(w(i,:),w(j,:)));
                    S_elim(i,j)=sum(S_change.*f.P_elim(w(i,:),w(j,:)))./sum(f.P_elim(w(i,:),w(j,:)));
                    S_pot(i,j)=sum(S_change.*f.P_pot(w(i,:),w(j,:)))./sum(f.P_pot(w(i,:),w(j,:)));
                    S_dep(i,j)=sum(S_change.*f.P_dep(w(i,:),w(j,:)))./sum(f.P_dep(w(i,:),w(j,:)));
                    
                    H(i,j)=sum((f.P_change(w(i,:),w(j,:))) - ...
                        (...
                        f.P_change(w(i,:),w(i,:)) + ...
                        f.P_change(w(j,:),w(j,:))...
                        )./2)./Nb;
                    
                else
                    
                    P=f.P(w)>sig_level;
                    Nb=sum(P(i,:)+P(j,:))./2;
                    
                    added(i,j)=sum(f.P_add(w(i,:),w(j,:))>sig_level)./Nb;
                    elim(i,j)=sum(f.P_elim(w(i,:),w(j,:))>sig_level)./Nb;
                    pot(i,j)=sum(f.P_pot(w(i,:),w(j,:))>sig_level)./Nb;
                    dep(i,j)=sum(f.P_dep(w(i,:),w(j,:))>sig_level)./Nb;
                    
                    S_added(i,j)=sum(S_change.*(f.P_add(w(i,:),w(j,:))>sig_level))./sum((f.P_add(w(i,:),w(j,:))>sig_level));
                    S_elim(i,j)=sum(S_change.*(f.P_elim(w(i,:),w(j,:))>sig_level))./sum((f.P_elim(w(i,:),w(j,:))>sig_level));
                    S_pot(i,j)=sum(S_change.*(f.P_pot(w(i,:),w(j,:))>sig_level))./sum((f.P_pot(w(i,:),w(j,:))>sig_level));
                    S_dep(i,j)=sum(S_change.*(f.P_dep(w(i,:),w(j,:))>sig_level))./sum((f.P_dep(w(i,:),w(j,:))>sig_level));
                    
                    H(i,j)=sum((f.P_change(w(i,:),w(j,:))>sig_level) - ...
                        (...
                        (f.P_change(w(i,:),w(i,:))>sig_level) + ...
                        (f.P_change(w(j,:),w(j,:))>sig_level)...
                        )./2)./Nb;
                    %Note: f.P_change(w(i,:),w(i,:)) = f.P_change(w(j,:),w(j,:))
                end
            end
        end
        
        
        Mat.added=added;
        Mat.elim=elim;
        Mat.tor=tor;
        Mat.pot=pot;
        Mat.dep=dep;
        Mat.tor=added+elim;
        
        Mat.S_added=S_added;
        Mat.S_elim=S_elim;
        Mat.S_pot=S_pot;
        Mat.S_dep=S_dep;
        
        Matname={'added','elim','tor','pot','dep','S_added','S_elim','S_pot','S_dep'};
        Maptitle={'Added Fraction','Eliminated Fraction','Turnover Fraction','Potentiated Fraction','Depressed Fraction',...
            'Avg Added Weight','Avg Eliminated Weight','Avg Potentiation','Avg Depression'};
        for k=1:numel(Matname)
            
            M=Mat.(Matname{k});
            M(isnan(M))=0;
            Out.M=M;
            B=zeros(size(M));
            
            %figure(700)
            M=triu(M,1);
            M(M==0)=nan;
            %[~,ind]=min(abs(Dat.L.im_t-Dat.L.t_stimonly));
            %M(1:ind,:)=nan;M(:,1:ind)=nan;
            %plot((1:size(M,1))-ind,nanmean(M,1),'.-','Color',anc);hold on
            
            %{
            for i=1:size(M,1)
                for j=i:size(M,1)
                    B(i,j)=nanmean(diag(M,j-i));
                end
            end
            %}
            
            h=figure;imagesc(M);axis square;colorbar;hold on
            title([Maptitle{k}]);
            h.Name=num2str(Dat.anid);
            
            %[~,ind]=min(abs(Dat.L.im_t-Dat.L.t_stimaudio));
            ind = interp1(Dat.L.im_t,(1:numel(D.L.im_t))',Dat.L.t_stimaudio);
            plot([ind ind],[0,size(w,1)+1],'-','Color',[0.8 0.8 0],'LineWidth',2)
            plot([0,size(w,1)+1],[ind ind],'-','Color',[0.8 0.8 0],'LineWidth',2)
            %[~,ind]=min(abs(Dat.L.im_t-Dat.L.t_stimonly));
            ind = interp1(Dat.L.im_t,(1:numel(D.L.im_t))',Dat.L.t_stimonly);
            plot([ind ind],[0,size(w,1)+1],'-','Color',[0 0.8 0],'LineWidth',2)
            plot([0,size(w,1)+1],[ind ind],'-','Color',[0 0.8 0],'LineWidth',2)
            if ~ismember(Dat.anid,[88,102])
                %[~,ind]=min(abs(Dat.L.im_t-Dat.L.t_inflection));
                ind = interp1(Dat.L.im_t,(1:numel(D.L.im_t))',Dat.L.t_inflection);
                plot([ind ind],[0,size(w,1)+1],'-','Color',[0.4 0.4 0.4],'LineWidth',2)
                plot([0,size(w,1)+1],[ind ind],'-','Color',[0.4 0.4 0.4],'LineWidth',2)
                
                %[~,ind]=min(abs(Dat.L.im_t-Dat.L.t_startlearn));
                ind = interp1(Dat.L.im_t,(1:numel(D.L.im_t))',Dat.L.t_startlearn);
                plot([ind ind],[0,size(w,1)+1],'--','Color',[0.4 0.4 0.4],'LineWidth',2)
                plot([0,size(w,1)+1],[ind ind],'--','Color',[0.4 0.4 0.4],'LineWidth',2)
                
                %[~,ind]=min(abs(Dat.L.im_t-Dat.L.t_endlearn));
                ind = interp1(Dat.L.im_t,(1:numel(D.L.im_t))',Dat.L.t_endlearn);
                plot([ind ind],[0,size(w,1)+1],'--','Color',[0.4 0.4 0.4],'LineWidth',2)
                plot([0,size(w,1)+1],[ind ind],'--','Color',[0.4 0.4 0.4],'LineWidth',2)
            end
            
            set(gca,'XTick',1:size(M,1),'XTickLabel',Dat.L.im_id,...
                'YTick',1:size(M,1),'YTickLabel',Dat.L.im_id)
            ylabel('Initial')
            xlabel('Final')
            
            Malpha=ones(size(M));
            Malpha=triu(Malpha,1);
            set(findobj(gca,'Type','Image'),'AlphaData',Malpha);
            drawnow;
            
            %Following block to only capture image:
            %{
        %axis off,
        axis tight;colorbar off;title ([]);
        
        %delete(findall(gca,'Type','Line'));
        Malpha=ones(size(M));
        Malpha=triu(Malpha,1);
        set(findobj(gca,'Type','Image'),'AlphaData',Malpha);
            
        %set(gca,'Position',[0 0 1 1]);
        
        h=gcf;
        %set(gcf,'Position',[0 0 500 500])
        movegui(h,anpos)
            %}
            
            %colormap(redbluecmap(11));
            
            %{
        pathlist;
        print(gcf,'-dpng',[res_pth,'Map-',,num2str(Dat.anid),'.png']);
        savefig(gcf,[res_pth,'Map-',,num2str(Dat.anid),'.fig'])
            %}
        end
end
end

% % % % % elseif strcmp(analysis_type,'Diffusion')
% % % % %     X=Dat.w;
% % % % %     P=fP(X);
% % % % %     Ppot=f.P_pot(X(1:end-1,:),X(2:end,:));
% % % % %     Pdep=f.P_dep(X(1:end-1,:),X(2:end,:));
% % % % %
% % % % %     Ppp=Ppot(1:end-1,:)+Ppot(2:end,:);
% % % % %     Ppd=Ppot(1:end-1,:)+Pdep(2:end,:);
% % % % %     Pdp=Pdep(1:end-1,:)+Ppot(2:end,:);
% % % % %     Pdd=Pdep(1:end-1,:)+Pdep(2:end,:);
% % % % %
% % % % %     figure;hold on
% % % % %     plot(sum(Ppp,2)./sum(P(2:end-1,:),2))
% % % % %     plot(sum(Ppd,2)./sum(P(2:end-1,:),2))
% % % % %     plot(sum(Pdp,2)./sum(P(2:end-1,:),2))
% % % % %     plot(sum(Pdd,2)./sum(P(2:end-1,:),2))
% % % % %     drawnow;
% % % % %
% % % % % elseif strcmp(analysis_type,'SpaceCorr2')
% % % % %     t=alignedtime(1:end-1);
% % % % %     dw=diff(Dat.w,1,1);
% % % % %     %dw=Dat.w;
% % % % %     P=fP(Dat.w);
% % % % %     siglevel=0.7;
% % % % %     P=P>siglevel;
% % % % %
% % % % %     axonidmat=bsxfun(@eq,Dat.ax_id',Dat.ax_id);
% % % % %     tdbinedges=0:10:50;
% % % % %     cc_time_data=nan(size(dw,1),numel(tdbinedges)-1);
% % % % %     for ttt=1:size(dw,1)
% % % % %         td=abs(bsxfun(@minus,Dat.d',Dat.d));
% % % % %         %Calculate all to all correlation of differences
% % % % %         cc=bsxfun(@times,dw(ttt,:)',dw(ttt,:));
% % % % %         arebtns=bsxfun(@and,P(ttt,:)',P(ttt+1,:)) & bsxfun(@minus,Dat.w(ttt,:)',Dat.w(ttt+1,:))>0;
% % % % %         cc(~arebtns)=nan;
% % % % %         cc(~axonidmat)=nan;
% % % % %         cc(eye(size(cc))>0)=nan;
% % % % %
% % % % %         td(~triu(true(size(td)),1))=nan;
% % % % %         td(~axonidmat)=nan;
% % % % %         td=td(:);
% % % % %
% % % % %         td_bin_id=nan(size(td));
% % % % %         for i=1:(numel(tdbinedges))
% % % % %             td_bin_id(td>tdbinedges(i))=i;
% % % % %         end
% % % % %         meantd=nan(numel(tdbinedges)-1,1);
% % % % %         stdtd=nan(numel(tdbinedges)-1,1);
% % % % %         meancc_td=nan(numel(tdbinedges)-1,1);
% % % % %         stdcc_td=nan(numel(tdbinedges)-1,1);
% % % % %         counts_td=nan(numel(tdbinedges)-1,1);
% % % % %         for i=1:(numel(tdbinedges)-1)
% % % % %             meantd(i)=mean(td(td_bin_id==i));
% % % % %             stdtd(i)=std(td(td_bin_id==i));
% % % % %             meancc_td(i)=nanmean(cc(td_bin_id==i));
% % % % %             stdcc_td(i)=nanstd(cc(td_bin_id==i),1,1)./(sum(td_bin_id==i)).^0.5;
% % % % %             counts_td(i)=sum(td_bin_id==i);
% % % % %         end
% % % % %         cc_time_data(ttt,:)=meancc_td(:);
% % % % %     end
% % % % %     figure(50);movegui(50,'northwest');
% % % % %     plot(t,cc_time_data(:,1),'Color',anc);hold on
% % % % %     xlabel('Time')
% % % % %     ylabel('Avg. correlation')
% % % % %     ylim([-3 3])
% % % % %     box on;drawnow;
% % % % %
% % % % %     figure(51);movegui(51,'north');
% % % % %     plot(t,cc_time_data(:,2),'Color',anc);hold on
% % % % %     xlabel('Time')
% % % % %     ylabel('Avg. correlation')
% % % % %     ylim([-3 3])
% % % % %     box on;drawnow;
% % % % %
% % % % %     figure(52),movegui(52,'northeast');
% % % % %     plot(t,cc_time_data(:,3),'Color',anc);hold on
% % % % %     xlabel('Time')
% % % % %     ylabel('Avg. correlation')
% % % % %     ylim([-3 3])
% % % % %     box on;drawnow;
% % % % %
% % % % %     figure(53),movegui(53,'southeast');
% % % % %     plot(t,cc_time_data(:,4),'Color',anc);hold on
% % % % %     xlabel('Time')
% % % % %     ylabel('Avg. correlation')
% % % % %     ylim([-3 3])
% % % % %     box on;drawnow;
% % % % %
% % % % %     %     figure(53),movegui(53,'south');
% % % % %     %     plot(t,mean(abs(dw),2),'Color',anc);hold on
% % % % %     %     xlabel('Time')
% % % % %     %     ylabel('Avg. dw')
% % % % %     %     ylim([0 1.5])
% % % % %     %     box on;drawnow;
% % % % %
% % % % %     %{
% % % % %         bs_iter=10;
% % % % %         %Initialize bins
% % % % %         bs_sumtd=zeros(numel(tdbinedges),1);
% % % % %         bs_stdtd=nan(numel(tdbinedges),1);
% % % % %         bs_sumcc_td=zeros(numel(tdbinedges),1);
% % % % %         bs_stdcc_td=nan(numel(tdbinedges),1);
% % % % %         bs_counts_td=zeros(numel(tdbinedges),1);
% % % % %         %Shuffling position along axon for bootstrap
% % % % %         for itr=1:bs_iter
% % % % %             bsd=Dat.d;
% % % % %             for ax=1:numel(ax_lbl)
% % % % %                 tempd=Dat.d(Dat.ax_id==ax_lbl(ax));
% % % % %                 bsd(Dat.ax_id==ax_lbl(ax))=tempd(randperm(numel(tempd)));
% % % % %             end
% % % % %             %Bootstrap interbouton distances
% % % % %             bsd_td=abs(bsxfun(@minus,bsd',bsd));
% % % % %             bsd_td(~triu(true(size(bsd_td)),1))=nan;
% % % % %             bsd_td(~axonidmat)=nan;
% % % % %             bsd_td=bsd_td(:);
% % % % %             %Bootstrap binning
% % % % %             bs_td_bin_id=nan(size(bsd_td));
% % % % %             for i=1:(numel(tdbinedges))
% % % % %                 bs_td_bin_id(bsd_td>tdbinedges(i))=i;
% % % % %             end
% % % % %
% % % % %             for i=1:(numel(tdbinedges))
% % % % %                 bs_sumtd(i)=bs_sumtd(i)+sum(bsd_td(bs_td_bin_id==i));
% % % % %                 bs_stdtd(i)=std(bsd_td(bs_td_bin_id==i));
% % % % %                 bs_sumcc_td(i)=bs_sumcc_td(i)+sum(cc(bs_td_bin_id==i));
% % % % %                 bs_stdcc_td(i)=std(cc(bs_td_bin_id==i),1,1)./(sum(bs_td_bin_id==i)).^0.5;
% % % % %                 bs_counts_td(i)=bs_counts_td(i)+sum(bs_td_bin_id==i);
% % % % %             end
% % % % %         end
% % % % %     %}
% % % % %
% % % % %     %{
% % % % %         figure(7000),hold on
% % % % %         if isempty(get(gca,'Children'))
% % % % %             plot([min(tdbinedges),max(tdbinedges)],[0 0],'-k')
% % % % %         end
% % % % %         %Data plot
% % % % %         xx=meantd(:);
% % % % %         yy=meancc_td(:);
% % % % %         ee=stdcc_td(:);
% % % % %         errorbar(xx,yy,ee,'Marker','none','Color',anc+(0.01*ttt));
% % % % %         plot(xx,yy,'.-','MarkerSize',markersiz,'Color',anc+(0.01*ttt));
% % % % %     %{
% % % % %         %Bootstrap plot
% % % % %         xx=[bs_sumtd(1)./bs_counts_td(1), bs_sumtd(end-1)./bs_counts_td(end-1)];
% % % % %         yy=[mean(bs_sumcc_td(1:end-1)./bs_counts_td(1:end-1)),mean(bs_sumcc_td(1:end-1)./bs_counts_td(1:end-1))];
% % % % %         plot(xx,yy,'--','Color',anc+(0.02*ttt));
% % % % %     %}
% % % % %         xlabel('Distance along axon \mu{m}');ylabel('Correlation')
% % % % %         %ylim([-0.2 120]),xlim([min(tdbinedges),max(tdbinedges)])
% % % % %         box on,axis square,
% % % % %         set(gca,'FontName','Calibri','FontSize',15)
% % % % %         drawnow;
% % % % %     %}
% % % % %     %end

% % % % % elseif strcmp(analysis_type,'ShowCorr')
% % % % %     %How correlated are the time series of weights for boutons at a given
% % % % %     %distance from each other (on the same axon)?
% % % % %     X=Dat.w;
% % % % %
% % % % %     X=bsxfun(@minus,X,mean(X,1));
% % % % %     Xstd=std(X,1,1);
% % % % %
% % % % %     %Calculate all to all spatial correlations
% % % % %     cc=(X'*X)./(Xstd'*Xstd)/(size(X,1));
% % % % %     axonidmat=bsxfun(@eq,Dat.ax_id',Dat.ax_id);
% % % % %     keep=true(size(cc));
% % % % %     keep=triu(keep,1);
% % % % %     cc(~keep)=nan;%removes autocorrelations
% % % % %     cc(~axonidmat)=nan;
% % % % %     dd=abs(bsxfun(@minus,Dat.d',Dat.d));
% % % % %
% % % % %     t=1;
% % % % %     dd=(bsxfun(@minus,Dat.rx(t,:)',Dat.rx(t,:))).^2....
% % % % %         +(bsxfun(@minus,Dat.ry(t,:)',Dat.ry(t,:))).^2 ...
% % % % %         +(bsxfun(@minus,Dat.rz(t,:)',Dat.rz(t,:))).^2;
% % % % %     dd=dd.^0.5;
% % % % %     dd(isnan(cc))=nan;
% % % % %
% % % % %     figure(801+anind),histogram(cc(:),[-1:0.05:1],'Normalization','cdf','DisplayStyle','stairs','EdgeColor',[0 0 0]);hold on
% % % % %
% % % % %     bins=5:5:50;
% % % % %     col=parula(numel(bins));
% % % % %     for b=1:numel(bins)
% % % % %         figure(801+anind),histogram(cc(dd<bins(b) & dd>0.5),[-1:0.05:1],'Normalization','cdf','DisplayStyle','stairs','EdgeColor',col(b,:));hold on
% % % % %     end
% % % % %     xlim([-0.95,0.95]);ylim([0 1]);box on;axis square
% % % % %
% % % % %     figure(5),histogram(cc(dd<bins(1) & dd>0.5),[-1:0.05:1],'Normalization','cdf','DisplayStyle','stairs','EdgeColor',anc);hold on
% % % % %     [ii,jj]=find(cc>0.9);
% % % % %     comb=nan(size([ii;jj]));comb(1:2:end)=ii;comb(2:2:end)=jj;
% % % % %     %analysis_plotboutonsontrace(D,Animal,comb,1);
% % % % %     %analysis_plotboutonsonIM(D,Animal,comb);
% % % % %
% % % % %     X=Dat.w(:,randperm(size(Dat.w,2)));
% % % % %     X=bsxfun(@minus,X,mean(X,1));
% % % % %     Xstd=std(X,1,1);
% % % % %     %Calculate all to all spatial correlations
% % % % %     cc=(X'*X)./(Xstd'*Xstd)/(size(X,1));
% % % % %     axonidmat=bsxfun(@eq,Dat.ax_id',Dat.ax_id);
% % % % %     keep=true(size(cc));
% % % % %     keep=triu(keep,1);
% % % % %     cc(~keep)=nan;%removes autocorrelations
% % % % %     cc(~axonidmat)=nan;
% % % % %
% % % % %     figure(801+anind),histogram(cc(dd<bins(1)),[-1:0.05:1],'Normalization','cdf','DisplayStyle','stairs','EdgeColor',[1 0 0]);
% % % % %

% % % % %
% % % % % elseif strcmp('Plasticity',analysis_type)
% % % % %     mrkr={'x','o','^','d','s'};
% % % % %     ismarkepoch=true;
% % % % %     sel_t='inflection';
% % % % %     siglevel=0.95;
% % % % %     %'start'
% % % % %     %'stimonly'
% % % % %     %'inflection'
% % % % %     %'scaled'
% % % % %     %'ind_L'
% % % % %     %'ind_dL'
% % % % %
% % % % %
% % % % %     %Calculate different x-axes (time or learning index related)-----------
% % % % %     %----------------------------------------------------------------------
% % % % %     %Scaled Time:
% % % % %     t.scaled=Dat.L.im_t;
% % % % %     epoch.orig=[Dat.L.t_stimaudio,Dat.L.t_stimonly,Dat.L.fit_f.mu-3*Dat.L.fit_f.t,Dat.L.fit_f.mu,Dat.L.fit_f.mu+3*Dat.L.fit_f.t];
% % % % %     epoch.scaled=epoch.orig;
% % % % %     leftscale=1./(epoch.orig(3)-epoch.orig(2));
% % % % %     rightscale=1./(epoch.orig(5)-epoch.orig(3));
% % % % %     t.scaled(t.scaled<epoch.orig(3))=(t.scaled(t.scaled<epoch.orig(3))-epoch.orig(3)).*leftscale;
% % % % %     t.scaled(t.scaled>epoch.orig(3))=(t.scaled(t.scaled>epoch.orig(3))-epoch.orig(3)).*rightscale;
% % % % %     epoch.scaled(epoch.orig<epoch.orig(3))=(epoch.orig(epoch.orig<epoch.orig(3))-epoch.orig(3)).*leftscale;
% % % % %     epoch.scaled(epoch.orig>epoch.orig(3))=(epoch.orig(epoch.orig>epoch.orig(3))-epoch.orig(3)).*rightscale;
% % % % %     epoch.scaled(3)=0;
% % % % %     axtxt.scaled='Scaled time';
% % % % %
% % % % %     %Time from start
% % % % %     t.start=Dat.L.im_t-Dat.L.im_t(1);
% % % % %     epoch.start=epoch.orig-Dat.L.im_t(1);
% % % % %     axtxt.start='Time from start (days)';
% % % % %
% % % % %     %Time from stimulation-only
% % % % %     t.stimonly=Dat.L.im_t-epoch.orig(2);
% % % % %     epoch.stimonly=epoch.orig-epoch.orig(2);
% % % % %     axtxt.stimonly='Time from stimulation only task (days)';
% % % % %
% % % % %     %Time from inflection point
% % % % %     t.inflection=Dat.L.im_t-epoch.orig(4);
% % % % %     epoch.inflection=epoch.orig-epoch.orig(4);
% % % % %     axtxt.inflection='Time from inflection (days)';
% % % % %
% % % % %     Lbase=0.5;%Baseline of all curves set to 0.5
% % % % %     sep=0.1;%Spacing for indexed points
% % % % %     Lthr_L=Dat.L.fit_f(Dat.L.fit_f.mu-3*Dat.L.fit_f.t)-Dat.L.fit_f.a+Lbase;
% % % % %     t.ind_L=Dat.L.im_Lfit-Dat.L.fit_f.a+Lbase;
% % % % %     t.ind_L(t.ind_L<Lthr_L)=((Lthr_L-sep*sum(t.ind_L<Lthr_L)):sep:(Lthr_L-sep))';
% % % % %     epoch.ind_L=interp1(t.start,t.ind_L,epoch.start);
% % % % %     axtxt.ind_L='Indices+Learning';
% % % % %
% % % % %     Lthr_dL=Dat.L.fit_f(Dat.L.fit_f.mu-3*Dat.L.fit_f.t)-Dat.L.fit_f.a;
% % % % %     sep=0.02;%for index part of the combined index+delta learning axis
% % % % %
% % % % %     t.ind_dL1=diff(Dat.L.im_Lfit);
% % % % %     temp=find(t.ind_dL1>Lthr_dL,1,'first');
% % % % %     t.ind_dL1(1:(temp-1))=((Lthr_dL-sep*(temp-1)):sep:(Lthr_dL-sep))';
% % % % %
% % % % %     meant_start=(t.start(2:end)+t.start(1:end-1))./2;
% % % % %     infind=find(diff(t.ind_dL1)<0,1,'first')+1;
% % % % %     ind_dL1_temp1=interp1(meant_start(1:(infind-1)),t.ind_dL1(1:(infind-1)),epoch.start(epoch.start<=meant_start(infind-1)));
% % % % %     ind_dL1_temp2=interp1(meant_start((infind-1):end),t.ind_dL1((infind-1):end),epoch.start(epoch.start>meant_start(infind-1)));
% % % % %     %epoch.ind_dL1=interp1(meant_start,t.ind_dL1,epoch.start);
% % % % %     epoch.ind_dL1=[ind_dL1_temp1,ind_dL1_temp2];
% % % % %
% % % % %     %For measures that are averaged over 3 sessions
% % % % %     t.ind_dL2=Dat.L.im_Lfit(3:end)-Dat.L.im_Lfit(1:end-2);
% % % % %     temp=find(t.ind_dL2>Lthr_dL,1,'first');
% % % % %     t.ind_dL2(1:(temp-1))=((Lthr_dL-sep*(temp-1)):sep:(Lthr_dL-sep))';
% % % % %     infind=find(diff(t.ind_dL2)<0,1,'first')+1;
% % % % %     ind_dL2_temp1=interp1(t.start(2:infind),t.ind_dL2(1:(infind-1)),epoch.start(epoch.start<=t.start(infind)));
% % % % %     ind_dL2_temp2=interp1(t.start(infind:end-1),t.ind_dL2(infind-1:end),epoch.start(epoch.start>t.start(infind)));
% % % % %     epoch.ind_dL2=[ind_dL2_temp1,ind_dL2_temp2];
% % % % %
% % % % %     axtxt.ind_dL='Indices+\Delta{Learning}';
% % % % %     n_boutons=nansum(fP(Dat.w),2);
% % % % %
% % % % %
% % % % %     %First order number statistics-----------------------------------------
% % % % %     %----------------------------------------------------------------------
% % % % %
% % % % %     %Calculate addition and elimination (numbers)--------------------------
% % % % %     P_added=f.P_add(Dat.w(1:end-1,:),Dat.w(2:end,:));
% % % % %     n_sig_added=sum(P_added>siglevel,2);
% % % % %     frac_sig_added=n_sig_added./mean(n_boutons);
% % % % %     err_frac_sig_added=n_sig_added.^0.5./mean(n_boutons);
% % % % %
% % % % %     P_eliminated=f.P_elim(Dat.w(1:end-1,:),Dat.w(2:end,:));
% % % % %     n_sig_eliminated=sum(P_eliminated>siglevel,2);
% % % % %     frac_sig_eliminated=n_sig_eliminated./mean(n_boutons);
% % % % %     err_frac_sig_eliminated=n_sig_eliminated.^0.5./mean(n_boutons);
% % % % %
% % % % %     %Calculate potentiated and depressed (numbers)-------------------------
% % % % %     P_pot=f.P_pot(Dat.w(1:end-1,:),Dat.w(2:end,:));
% % % % %     n_sig_pot=sum(P_pot>siglevel,2);
% % % % %     frac_sig_pot=n_sig_pot./mean(n_boutons);
% % % % %     err_frac_sig_pot=n_sig_pot.^0.5./mean(n_boutons);
% % % % %
% % % % %     P_dep=f.P_dep(Dat.w(1:end-1,:),Dat.w(2:end,:));
% % % % %     n_sig_dep=sum(P_dep>siglevel,2);
% % % % %     frac_sig_dep=n_sig_dep./mean(n_boutons);
% % % % %     err_frac_sig_dep=n_sig_dep.^0.5./mean(n_boutons);
% % % % %
% % % % %     %Others (numbers)-----------------------------------------------------
% % % % %     thr_large=4;
% % % % %     P_large_stb=fP(Dat.w(1:end-1,:),thr_large).*fP(Dat.w(2:end,:),thr_large);
% % % % %     n_large_stb=sum(P_large_stb>siglevel,2);
% % % % %     frac_large_stb=n_large_stb./mean(n_boutons);
% % % % %     err_large_stb=n_large_stb.^0.5./mean(n_boutons);
% % % % %
% % % % %     %First order strength statistics---------------------------------------
% % % % %     %----------------------------------------------------------------------
% % % % %
% % % % %     S_mean=sum(Dat.w(1:end-1,:)+Dat.w(2:end,:),2);
% % % % %
% % % % %     P_added=f.P_add(Dat.w(1:end-1,:),Dat.w(2:end,:))>siglevel;
% % % % %     S_added=sum(abs(Dat.w(1:end-1,:)-Dat.w(2:end,:)).*P_added,2);
% % % % %     frac_sigStrength_added=S_added./S_mean;
% % % % %
% % % % %     P_eliminated=f.P_elim(Dat.w(1:end-1,:),Dat.w(2:end,:))>siglevel;
% % % % %     S_eliminated=sum(abs(Dat.w(1:end-1,:)-Dat.w(2:end,:)).*P_eliminated,2);
% % % % %     frac_sigStrength_eliminated=S_eliminated./S_mean;
% % % % %
% % % % %     P_pot=f.P_pot(Dat.w(1:end-1,:),Dat.w(2:end,:))>siglevel;
% % % % %     S_pot=sum(abs(Dat.w(1:end-1,:)-Dat.w(2:end,:)).*P_pot,2);
% % % % %     frac_sigStrength_pot=S_pot./S_mean;
% % % % %
% % % % %     P_dep=f.P_dep(Dat.w(1:end-1,:),Dat.w(2:end,:))>siglevel;
% % % % %     S_dep=sum(abs(Dat.w(1:end-1,:)-Dat.w(2:end,:)).*P_dep,2);
% % % % %     frac_sigStrength_dep=S_dep./S_mean;
% % % % %
% % % % %     %Second order strength statistics--------------------------------------
% % % % %     %----------------------------------------------------------------------
% % % % %
% % % % %     %Update 11 measure for oscillation-------------------------------------
% % % % %     S_d2=nan(size(Dat.w(2:end-1,:)));
% % % % %     P_bouton=nan(size(Dat.w(2:end-1,:),1),1);
% % % % %     for i=2:(size(Dat.w,1)-1)
% % % % %         S_d2(i-1,:)=sum(([-0.5;1;-0.5]*ones(1,size(Dat.w,2))).*Dat.w(i-1:i+1,:),1).*(fP(Dat.w(i,:))>siglevel);
% % % % %         P_bouton(i-1)=sum(fP(Dat.w(i,:))>siglevel);
% % % % %     end
% % % % %     frac_d2_strength=sum(abs(S_d2),2)./P_bouton;
% % % % %
% % % % %
% % % % %     %Plots if not ind_dL
% % % % %     %----------------------------------------------------------------------
% % % % %     %----------------------------------------------------------------------
% % % % %     if ~strcmp(sel_t,'ind_dL')
% % % % %
% % % % %         %tt=(t.(sel_t)(1:end-1)+t.(sel_t)(2:end))./2;
% % % % %         startt=find(Dat.L.fit_f(Dat.L.im_t)<Dat.L.fit_f(Dat.L.fit_f.mu),1,'last');
% % % % %         tt=t.(sel_t)(2:end)-t.(sel_t)(startt);
% % % % %         display('All plots aligned by start of learning!')
% % % % %
% % % % %         %------------------------------------------------------------------
% % % % %         %This section contains plots for definitions not involving nans
% % % % %
% % % % %         %Added/Eliminated numbers------------------------------------------
% % % % %         figure(503);movegui(503,'northwest');hold on
% % % % %         %errorbar(tt,frac_sig_added,err_frac_sig_added,'Marker','none','Color',anc);
% % % % %         plot(tt,frac_sig_added,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         %ylim([0 0.25])
% % % % %         xlabel(axtxt.(sel_t));ylabel('Significant added fraction')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         figure(504);movegui(504,'north');hold on
% % % % %         %errorbar(tt,frac_sig_eliminated,err_frac_sig_eliminated,'Marker','none','Color',anc);
% % % % %         plot(tt,frac_sig_eliminated,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         %ylim([0 0.25])
% % % % %         xlabel(axtxt.(sel_t));ylabel('Significant eliminated fraction')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         %Potentiation Depression numbers-----------------------------------
% % % % %         figure(505);movegui(505,'southwest');hold on
% % % % %         %errorbar(tt,frac_sig_pot,err_frac_sig_pot,'Marker','none','Color',anc);
% % % % %         plot(tt,frac_sig_pot,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         %ylim([0.0 .60])
% % % % %         xlabel(axtxt.(sel_t));ylabel('Fraction of significant potentiations')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         figure(506);movegui(506,'south');hold on
% % % % %         %errorbar(tt,frac_sig_dep,err_frac_sig_dep,'Marker','none','Color',anc);
% % % % %         plot(tt,frac_sig_dep,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         %ylim([0.0 .60])
% % % % %         xlabel(axtxt.(sel_t));ylabel('Fraction of significant Depressions')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %
% % % % %         figure(900)
% % % % %         added=sum(f.P_add(Dat.w(1:end-1,:),Dat.w(2:end,:)),2);
% % % % %         added=added./mean(sum(fP(Dat.w),2));
% % % % %         plot(tt,added,'Color',anc);hold on
% % % % %         h=gca;h.YLim(1)=0;h.YLim(2)=max([h.YLim(2),1.1*max(added(:))]);
% % % % %         xlabel(axtxt.(sel_t));ylabel('Added fraction')
% % % % %         axis square;box on;drawnow;
% % % % %
% % % % %
% % % % %         figure(901)
% % % % %         elim=sum(f.P_elim(Dat.w(1:end-1,:),Dat.w(2:end,:)),2);
% % % % %         elim=elim./mean(sum(fP(Dat.w),2));
% % % % %         plot(tt,elim,'Color',anc);hold on
% % % % %         xlabel(axtxt.(sel_t));ylabel('Eliminated fraction')
% % % % %         h=gca;h.YLim(1)=0;h.YLim(2)=max([h.YLim(2),1.1*max(elim(:))]);
% % % % %         axis square;box on;drawnow;%Added/Eliminated strengths----------------------------------------
% % % % %         %{
% % % % %         figure(603);movegui(603,'northwest');hold on
% % % % %         plot(tt,frac_sigStrength_added,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         xlabel(axtxt.(sel_t));ylabel('Strength fraction for significant addition')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         figure(604);movegui(604,'north');hold on
% % % % %         plot(tt,frac_sigStrength_eliminated,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         xlabel(axtxt.(sel_t));ylabel('Strength fraction for significant elimination')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         %Potentiation/Depression strengths---------------------------------
% % % % %         figure(605);movegui(605,'southwest');hold on
% % % % %         plot(tt,frac_sigStrength_pot,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         xlabel(axtxt.(sel_t));ylabel('Strength fraction for significant potentiation')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         figure(606);movegui(606,'south');hold on
% % % % %         plot(tt,frac_sigStrength_dep,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         xlabel(axtxt.(sel_t));ylabel('Strength fraction for significant depression')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %         %}
% % % % %
% % % % %         figure(607);movegui(607,'southeast');hold on
% % % % %         plot(t.(sel_t)(2:end-1),frac_d2_strength,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         xlabel(axtxt.(sel_t));ylabel('Fraction of oscillations')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %         %}
% % % % %         %------------------------------------------------------------------
% % % % %         figure(507);movegui(507,'northeast');hold on
% % % % %         if isempty(get(gca,'Children')) && strcmp(sel_t,'ind_L')
% % % % %             patch([-1;Lthr_L;Lthr_L;-1],[0;0;1;1],[0.5 0.5 0.5],'FaceAlpha',0.25,'EdgeColor','none');hold on
% % % % %         end
% % % % %         yy=frac_sig_added;%frac_sigStrength_added (eliminated,pot,dep)
% % % % %         plot(tt,yy,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         if ismarkepoch
% % % % %             y = interp1(tt,yy,epoch.(sel_t));
% % % % %             for i=1:length(epoch.(sel_t))
% % % % %                 plot(epoch.(sel_t)(i),y(i),'Marker',mrkr{i},'Color',anc,'MarkerSize',markersiz)
% % % % %             end
% % % % %         end
% % % % %         ylim([0.0 1.0])
% % % % %         xlabel(axtxt.(sel_t));ylabel('Fraction of significant added')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         figure(510);movegui(510,'southeast');hold on
% % % % %         if isempty(get(gca,'Children')) && strcmp(sel_t,'ind_L')
% % % % %             patch([-1;Lthr_L;Lthr_L;-1],[0;0;1;1],[0.5 0.5 0.5],'FaceAlpha',0.25,'EdgeColor','none');hold on
% % % % %         end
% % % % %         yy=frac_d2_strength;
% % % % %         plot(t.(sel_t)(2:end-1),yy,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         if ismarkepoch
% % % % %             y = interp1(t.(sel_t)(2:end-1),yy,epoch.(sel_t));
% % % % %             for i=1:length(epoch.(sel_t))
% % % % %                 plot(epoch.(sel_t)(i),y(i),'Marker',mrkr{i},'Color',anc,'MarkerSize',markersiz)
% % % % %             end
% % % % %         end
% % % % %         %ylim([0.15 .25])
% % % % %         xlabel(axtxt.(sel_t));ylabel('Oscillations in strength of existing boutons')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %         %}
% % % % %
% % % % %     else
% % % % %
% % % % %         %Plots with d(Learning)--------------------------------------------
% % % % %         %------------------------------------------------------------------
% % % % %
% % % % %         %Added/Eliminated strengths
% % % % %         figure(700);movegui(700,'southwest');hold on
% % % % %         if isempty(get(gca,'Children'))
% % % % %             patch([-1;Lthr_dL;Lthr_dL;-1],[0;0;1;1],[0.5 0.5 0.5],'FaceAlpha',0.25,'EdgeColor','none');hold on
% % % % %         end
% % % % %         yy=frac_sig_eliminated;%frac_sigStrength_added (eliminated,pot,dep)
% % % % %         plot(t.ind_dL1,yy,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         if ismarkepoch
% % % % %             %y = interp1(t.ind_dL1,abs(depstr),epoch.ind_dL1);
% % % % %             infind=find(diff(t.ind_dL1)<0,1,'first')+1;
% % % % %             y1 = interp1(t.ind_dL1(1:(infind-1)),abs(yy(1:(infind-1))),ind_dL1_temp1);
% % % % %             y2 = interp1(t.ind_dL1(infind-1:end),abs(yy(infind-1:end)),ind_dL1_temp2);
% % % % %             y=[y1,y2];
% % % % %             for i=1:length(epoch.ind_dL1)
% % % % %                 plot(epoch.ind_dL1(i),y(i),'Marker',mrkr{i},'Color',anc,'MarkerSize',markersiz)
% % % % %             end
% % % % %         end
% % % % %         ylim([0 0.07]),xlim([-0.25,0.2])
% % % % %         xlabel(axtxt.ind_dL);ylabel('Fraction of significant eliminated')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         figure(701);movegui(701,'northeast');hold on
% % % % %         if isempty(get(gca,'Children'))
% % % % %             patch([-1;Lthr_dL;Lthr_dL;-1],[0;0;1;1],[0.5 0.5 0.5],'FaceAlpha',0.25,'EdgeColor','none');hold on
% % % % %         end
% % % % %         yy=frac_d2_strength;
% % % % %         plot(t.ind_dL2,yy,'.-','Color',anc,'MarkerSize',markersiz)
% % % % %         if ismarkepoch
% % % % %             infind=find(diff(t.ind_dL2)<0,1,'first')+1;
% % % % %             y1 = interp1(t.ind_dL2(1:(infind-1)),yy(1:(infind-1)),ind_dL2_temp1);
% % % % %             y2 = interp1(t.ind_dL2(infind-1:end),yy(infind-1:end),ind_dL2_temp2);
% % % % %             y=[y1,y2];
% % % % %
% % % % %             for i=1:length(epoch.ind_dL2)
% % % % %                 plot(epoch.ind_dL2(i),y(i),'Marker',mrkr{i},'Color',anc,'MarkerSize',markersiz)
% % % % %             end
% % % % %         end
% % % % %         ylim([0.4 .7]),xlim([-0.2,0.25])
% % % % %         xlabel(axtxt.ind_dL);ylabel('\Delta^{2}Number/(Number of boutons)')
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %         %}
% % % % %     end
% % % % % elseif  strcmp('Distance',analysis_type)
% % % % %     siglevel=0.95;
% % % % %     P=fP(Dat.w);
% % % % %     D=bsxfun(@minus,P(poi(1):end,:),P(poi(1)-1,:));
% % % % %     D=abs(D)>siglevel;
% % % % %     nn=sum(P(poi(1)-1,:)>siglevel,2);
% % % % %     D=(sum((D.^2),2)).^0.5./nn;
% % % % %     figure(800);hold on
% % % % %     plot(1:size(D,1),D,'.-','Color',anc)
% % % % %     axis square;box on;
% % % % %     ylabel('Distance');xlabel('Time')
% % % % %
% % % % % elseif  strcmp('3sessionstates',analysis_type)
% % % % %     siglevel=0.9;
% % % % %
% % % % %     P=fP(Dat.w);
% % % % %     W=Dat.w;
% % % % %     dW=diff(W,1,1);
% % % % %     abs_dW=abs(dW(1:end-1,:))+abs(dW(2:end,:));
% % % % %
% % % % %     p1=P(1:end-2,:);
% % % % %     p2=P(2:end-1,:);
% % % % %     p3=P(3:end,:);
% % % % %
% % % % %     a1=(1-p1);
% % % % %     a2=(1-p2);
% % % % %     a3=(1-p3);
% % % % %
% % % % %     M{1}=a1.*a2.*a3;
% % % % %     M{2}=a1.*a2.*p3;
% % % % %     M{3}=a1.*p2.*a3;
% % % % %     M{4}=a1.*p2.*p3;
% % % % %     M{5}=p1.*a2.*a3;
% % % % %     M{6}=p1.*a2.*p3;
% % % % %     M{7}=p1.*p2.*a3;
% % % % %     M{8}=p1.*p2.*p3;
% % % % %
% % % % %     N=cell(1);
% % % % %     for j=1:numel(M)
% % % % %         N{j}=M{j}.*abs_dW;
% % % % %     end
% % % % %     nboutons=mean(sum(P,2));
% % % % %
% % % % %     T={'aaa','aap','apa','app','paa','pap','ppa','ppp'};
% % % % %     h=figure(900);
% % % % %     set(h,'Units','Normalized','Position',[0 0 0.9 0.9])
% % % % %     mstring={'^','s','*','d'};
% % % % %
% % % % %     for j=1:numel(M)
% % % % %         subplot(2,4,j)
% % % % %         %yy=sum(M{j}>siglevel,2)./nboutons;
% % % % %         yy=sum(N{j},2)./nboutons;
% % % % %         startt=2;
% % % % %         endd=startt+numel(yy)-1;
% % % % %         plot(alignedtime(startt:endd),yy,'.-','Color',anc,'MarkerSize',markersiz);hold on
% % % % %         for k=1:numel(poi)
% % % % %             plot(alignedtime(poi(k)),yy(poi(k)-startt+1),'Marker',mstring{k},'Color',anc,'MarkerSize',markersiz);hold on
% % % % %         end
% % % % %         %errorbar(alignedtime(2:end-1),sum(M{j}>siglevel,2)./nboutons,sum(M{j}>siglevel,2).^0.5./nboutons,'Color',anc,'MarkerSize',markersiz,'LineStyle','none')
% % % % %         ylabel(['Frac. of significant ',T{j}]);
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %     end
% % % % % elseif strcmp('BoutonDefn',analysis_type)
% % % % %     siglevel=0.95;
% % % % %     if animalid==83
% % % % %         learnstart=7;
% % % % %         learnend=12;
% % % % %     elseif animalid==85
% % % % %         learnstart=11;
% % % % %         learnend=15;
% % % % %     elseif animalid==101
% % % % %         learnstart=11;
% % % % %         learnend=17;
% % % % %     elseif animalid==102
% % % % %         learnstart=7;
% % % % %         learnend=17;
% % % % %     elseif animalid==88
% % % % %         learnstart=9;
% % % % %         learnend=17;
% % % % %     end
% % % % %
% % % % %     startt=find(Dat.L.im_t-Dat.L.t_stimaudio>0,1,'first');
% % % % %     %alignedtime=alignedtime-alignedtime(learnstart);
% % % % %     %alignedtime=alignedtime-alignedtime(learnend);
% % % % %
% % % % %     nsessions=6;keepsessions=learnstart-nsessions+1:1:learnstart+nsessions;
% % % % %     %keepsessions=(startt):size(Dat.w,1);
% % % % %     t_aligned=alignedtime(keepsessions);%-alignedtime(keepsessions(1));
% % % % %     W=Dat.w(keepsessions,:);
% % % % %     P=fP(W);
% % % % %
% % % % %     %One session based definitions-----------------------------------------
% % % % %     p1=P(1,:);
% % % % %     a1=(1-P(1,:)).*(1-P(2,:));
% % % % %
% % % % %     OneN{1}=bsxfun(@times,a1,1-P(2:end,:));
% % % % %     OneN{2}=bsxfun(@times,a1,P(2:end,:));
% % % % %     OneN{3}=bsxfun(@times,p1,1-P(2:end,:));
% % % % %     OneN{4}=bsxfun(@times,p1,P(2:end,:));
% % % % %     YLabels={'aa','ap','pa','pp'};
% % % % %     h=figure(905);set(h,'Units','Normalized','Position',[0 0.6 0.9 0.4])
% % % % %     h.Name='One session measures';
% % % % %     %nboutons=mean(sum(P,2));
% % % % %     nboutons=sum(P(2:end,:),2);
% % % % %     tt=t_aligned(2:end);
% % % % %     for j=1:numel(OneN)
% % % % %         subplot(1,4,j)
% % % % %         yy=sum(OneN{j}>siglevel,2)./nboutons;
% % % % %         %yy=sum(OneN{j},2)./nboutons;
% % % % %         plot(tt,yy,'.-','Color',anc,'MarkerSize',markersiz);hold on
% % % % %         xlabel('Time');ylabel(['Frac. of significant ',YLabels{j}]);
% % % % %         axis square;box on;drawnow;
% % % % %     end
% % % % %
% % % % %     %Two session based definitions-----------------------------------------
% % % % %     p12=prod(P(1:2,:),1);
% % % % %     a12=prod(1-P(1:2,:),1);
% % % % %
% % % % %     A1=(1-P(2:end-1,:));
% % % % %     A2=(1-P(3:end,:));
% % % % %     P1=P(2:end-1,:);
% % % % %     P2=P(3:end,:);
% % % % %
% % % % %     TwoN{1}=bsxfun(@times,a12,A1.*A2);
% % % % %     TwoN{2}=bsxfun(@times,a12,A1.*P2);
% % % % %     TwoN{3}=bsxfun(@times,p12,P1.*A2);
% % % % %     TwoN{4}=bsxfun(@times,p12,P1.*P2);
% % % % %
% % % % %     YLabels={'aa-aa','aa-pp','pp-aa','pp-pp'};
% % % % %     h=figure(906);set(h,'Units','Normalized','Position',[0 0.3 0.9 0.4])
% % % % %     h.Name='Two session measures';
% % % % %
% % % % %     nboutons=mean([sum(P1,2),sum(P2,2)],2);
% % % % %     tt=(t_aligned(2:end-1)+t_aligned(3:end))./2;
% % % % %     for j=1:numel(TwoN)
% % % % %         subplot(1,4,j)
% % % % %         yy=sum(TwoN{j}>siglevel,2)./nboutons;
% % % % %         %yy=sum(TwoN{j},2)./nboutons;
% % % % %         plot(tt,yy,'.-','Color',anc,'MarkerSize',markersiz);hold on
% % % % %         xlabel('Time');ylabel(['Frac. of significant ',YLabels{j}]);
% % % % %         axis square;box on;drawnow;
% % % % %     end
% % % % %
% % % % %     %Three session based definitions---------------------------------------
% % % % %     p123=prod(P(1:3,:),1)>siglevel;
% % % % %     a123=prod(1-P(1:3,:),1)>siglevel;
% % % % %     A=1-P;
% % % % %     A1=A(2:end-2,:);
% % % % %     A2=A(3:end-1,:);
% % % % %     A3=A(4:end,:);
% % % % %     P1=P(2:end-2,:);
% % % % %     P2=P(3:end-1,:);
% % % % %     P3=P(4:end,:);
% % % % %
% % % % %     ThreeN{1}=bsxfun(@times,a123,(A1.*A2.*A3)>siglevel);
% % % % %     ThreeN{2}=bsxfun(@times,a123,(P1.*P2.*P3)>siglevel);
% % % % %     ThreeN{3}=bsxfun(@times,p123,(A1.*A2.*A3)>siglevel);
% % % % %     ThreeN{4}=bsxfun(@times,p123,(P1.*P2.*P3)>siglevel);
% % % % %     YLabels={'aaa-aaa','aaa-ppp','ppp-aaa','ppp-ppp'};
% % % % %     h=figure(907);set(h,'Units','Normalized','Position',[0 0 0.9 0.4])
% % % % %     h.Name='Three session measures';
% % % % %
% % % % %     nboutons=sum(p1,2);
% % % % %     tt=t_aligned(3:end-1);
% % % % %     for j=1:numel(OneN)
% % % % %         subplot(1,4,j)
% % % % %         yy=sum(ThreeN{j}>siglevel,2)./nboutons;
% % % % %         %yy=sum(ThreeN{j},2)./nboutons;
% % % % %         plot(tt,yy,'.-','Color',anc,'MarkerSize',markersiz);hold on
% % % % %         xlabel('Time');ylabel(['Frac. of significant ',YLabels{j}]);
% % % % %         axis square;box on;drawnow;
% % % % %     end
% % % % %     %----------------------------------------------------------------------
% % % % %
% % % % %     p1=P(1:end-2,:);
% % % % %     p2=P(2:end-1,:);
% % % % %     p3=P(3:end,:);
% % % % %
% % % % %     a1=(1-p1);
% % % % %     a2=(1-p2);
% % % % %     a3=(1-p3);
% % % % %
% % % % %     ThreeN=cell(8,1);
% % % % %     %ThreeN{1}=a1.*a2.*a3;
% % % % %     ThreeN{2}=a1.*a2.*p3;
% % % % %     ThreeN{3}=a1.*p2.*a3;
% % % % %     ThreeN{4}=a1.*p2.*p3;
% % % % %     ThreeN{5}=p1.*a2.*a3;
% % % % %     ThreeN{6}=p1.*a2.*p3;
% % % % %     ThreeN{7}=p1.*p2.*a3;
% % % % %     ThreeN{8}=p1.*p2.*p3;
% % % % %
% % % % %     for k=2:numel(ThreeN)
% % % % %         ThreeN{k}=mean(ThreeN{k},2);
% % % % %     end
% % % % %
% % % % %     n=ThreeN{2}+ThreeN{3}+ThreeN{4}+...
% % % % %         ThreeN{5}+ThreeN{6}+ThreeN{7}+ThreeN{8};
% % % % %     for k=2:numel(ThreeN)
% % % % %         ThreeN{k}=ThreeN{k}./n;
% % % % %     end
% % % % %
% % % % %     E=zeros(size(ThreeN{k}));
% % % % %     for k=2:numel(ThreeN)
% % % % %         E=E-ThreeN{k}.*log(ThreeN{k});
% % % % %     end
% % % % %
% % % % %     figure(401)
% % % % %     plot(1:numel(E),E,'.-','Color',anc),hold on
% % % % %     axis square;box on;
% % % % %     drawnow;
% % % % %     %}
% % % % %
% % % % %     %------------------------------------------------------
% % % % %     %{
% % % % %     TwoS{1}=f.P_pot(W(1:end-1,:),W(2:end,:));
% % % % %     TwoS{2}=f.P_dep(W(1:end-1,:),W(2:end,:));
% % % % %     TwoS{3}=1-TwoS{1}-TwoS{2};
% % % % %
% % % % %     %Two session measure plots
% % % % %     T={'aa','ap','pa','pp'};TS={'pot','dep','unchanged'};
% % % % %     h=figure(901);
% % % % %     set(h,'Units','Normalized','Position',[0 0 0.9 0.9])
% % % % %     h.Name='2 session number and strength measures';
% % % % %     nboutons=mean(sum(P,2));
% % % % %     %tt=alignedtime(keepsessions);
% % % % %     tt=(t_aligned(1:end-1)+t_aligned(2:end))./2;
% % % % %     for j=1:numel(TwoN)
% % % % %         subplot(2,3,j)
% % % % %         %yy=sum(TwoN{j}>siglevel,2)./nboutons;
% % % % %         yy=sum(TwoN{j},2)./nboutons;
% % % % %         plot(tt,yy,'.-','Color',anc,'MarkerSize',markersiz);hold on
% % % % %         ylabel(['Frac. of significant ',T{j}]);
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %
% % % % %         subplot(2,3,j+3)
% % % % %         %yy=sum(TwoS{j}>siglevel,2)./nboutons;
% % % % %         yy=sum(TwoS{j},2)./nboutons;
% % % % %         plot(tt,yy,'.-','Color',anc,'MarkerSize',markersiz);hold on
% % % % %         ylabel(['Frac. of significant ',TS{j}]);
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %     end
% % % % %
% % % % %     p1=P(1:end-2,:);
% % % % %     p2=P(2:end-1,:);
% % % % %     p3=P(3:end,:);
% % % % %
% % % % %     a1=(1-p1);
% % % % %     a2=(1-p2);
% % % % %     a3=(1-p3);
% % % % %
% % % % %     ThreeN{1}=a1.*a2.*a3;
% % % % %     ThreeN{2}=a1.*a2.*p3;
% % % % %     ThreeN{3}=a1.*p2.*a3;
% % % % %     ThreeN{4}=a1.*p2.*p3;
% % % % %     ThreeN{5}=p1.*a2.*a3;
% % % % %     ThreeN{6}=p1.*a2.*p3;
% % % % %     ThreeN{7}=p1.*p2.*a3;
% % % % %     ThreeN{8}=p1.*p2.*p3;
% % % % %     %Three session measures
% % % % %     T={'aaa','aap','apa','app','paa','pap','ppa','ppp'};
% % % % %     h=figure(902);
% % % % %     h.Name='3 session number measures';
% % % % %     set(h,'Units','Normalized','Position',[0 0 0.9 0.9])
% % % % %     tt=t_aligned(2:end-1);
% % % % %     for j=1:numel(ThreeN)
% % % % %         subplot(2,4,j)
% % % % %         yy=sum(ThreeN{j}>siglevel,2)./nboutons;
% % % % %         plot(tt,yy,'.-','Color',anc,'MarkerSize',markersiz);hold on
% % % % %         ylabel(['Frac. of significant ',T{j}]);
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %     end
% % % % %
% % % % %     dep1=f.P_dep(W(1:end-2,:),W(2:end-1,:));
% % % % %     pot1=f.P_pot(W(1:end-2,:),W(2:end-1,:));
% % % % %     dep2=f.P_dep(W(2:end-1,:),W(3:end,:));
% % % % %     pot2=f.P_pot(W(2:end-1,:),W(3:end,:));
% % % % %
% % % % %     abs_dW1=abs(diff(W,1));abs_dW1=abs_dW1(1:end-1,:);
% % % % %     abs_dW2=abs(diff(W,1));abs_dW2=abs_dW2(2:end,:);
% % % % %
% % % % %     ThreeS=cell(1);
% % % % %
% % % % %     %{
% % % % %     ThreeS{1}=dep1.*dep2;
% % % % %     ThreeS{2}=dep1.*pot2;
% % % % %     ThreeS{3}=pot1.*dep2;
% % % % %     ThreeS{4}=pot1.*pot2;
% % % % %     %}
% % % % %
% % % % %     ThreeS{1}=dep1.*dep2.*(abs_dW1+abs_dW2);
% % % % %     ThreeS{2}=dep1.*pot2.*(abs_dW1+abs_dW2);
% % % % %     ThreeS{3}=pot1.*dep2.*(abs_dW1+abs_dW2);
% % % % %     ThreeS{4}=pot1.*pot2.*(abs_dW1+abs_dW2);
% % % % %
% % % % %     T={'dep-pot','dep-dep','pot-dep','pot-pot'};
% % % % %
% % % % %     h=figure(903);
% % % % %     h.Name='3 session strength measures';
% % % % %     set(h,'Units','Normalized','Position',[0 0 0.9 0.9])
% % % % %     for j=1:numel(ThreeS)
% % % % %         subplot(1,4,j)
% % % % %         %yy=sum(ThreeS{j}>siglevel,2)./nboutons;
% % % % %         yy=sum(ThreeS{j},2)./nboutons;
% % % % %         plot(tt,yy,'.-','Color',anc,'MarkerSize',markersiz);hold on
% % % % %         ylabel(['Frac. of significant ',T{j}]);
% % % % %         axis square;box on;
% % % % %         drawnow;
% % % % %     end
% % % % %     %}
% % % % %
% % % % % elseif strcmp('Entropy',analysis_type)
% % % % %     siglevel=0.95;
% % % % %     if animalid==83
% % % % %         learnstart=7;
% % % % %         learnend=12;
% % % % %     elseif animalid==85
% % % % %         learnstart=11;
% % % % %         learnend=15;
% % % % %     elseif animalid==101
% % % % %         learnstart=11;
% % % % %         learnend=17;
% % % % %     elseif animalid==102
% % % % %         learnstart=7;
% % % % %         learnend=17;
% % % % %     elseif animalid==88
% % % % %         learnstart=9;
% % % % %         learnend=17;
% % % % %     elseif animalid==108
% % % % %         learnstart=7;
% % % % %         learnend=12;
% % % % %     end
% % % % %
% % % % %     startt=find(Dat.L.im_t-Dat.L.t_stimaudio>0,1,'first');
% % % % %     startt=1;keepsessions=(startt):size(Dat.w,1);
% % % % %     %alignedtime=alignedtime-alignedtime(learnstart);
% % % % %     %alignedtime=alignedtime-alignedtime(learnend);
% % % % %
% % % % %     %nsessions=6;keepsessions=learnstart-nsessions+1:1:learnstart+nsessions;
% % % % %     %t_aligned=alignedtime(keepsessions)-alignedtime(keepsessions(1));
% % % % %     t_aligned=alignedtime(keepsessions);
% % % % %     W=Dat.w(keepsessions,:);
% % % % %     P=fP(W);
% % % % %
% % % % %     keepboutons=any(P>0,1);
% % % % %     P=P(:,keepboutons);
% % % % %     W=W(:,keepboutons);
% % % % %
% % % % %     %Two session based entropy measures------------------------------------
% % % % %     %Number: 4 possible states - aa,ap,pa,pp.
% % % % %     p1=P(1:end-1,:);
% % % % %     p2=P(2:end,:);
% % % % %     a1=1-p1;
% % % % %     a2=1-p2;
% % % % %
% % % % %     TwoP=cell(4,1);
% % % % %     TwoP{1}=mean(a1.*a2,2);
% % % % %     TwoP{2}=mean(a1.*p2,2);
% % % % %     TwoP{3}=mean(p1.*a2,2);
% % % % %     TwoP{4}=mean(p1.*p2,2);
% % % % %
% % % % %     states=[2,3,4];
% % % % %     %Calculate normalization if only a subset of states is considered
% % % % %     n=zeros(size(TwoP{states(1)}));
% % % % %     for s=1:numel(states)
% % % % %         n=n+TwoP{states(s)};
% % % % %     end
% % % % %
% % % % %     %Normalize data
% % % % %     for s=1:numel(states)
% % % % %         TwoP{states(s)}=TwoP{states(s)}./n;
% % % % %     end
% % % % %
% % % % %     %Calculate entropy
% % % % %     TwoE=0;
% % % % %     for s=1:numel(states)
% % % % %         TwoE=TwoE-TwoP{states(s)}.*log(TwoP{states(s)});
% % % % %     end
% % % % %
% % % % %     %Plot
% % % % %     figure(400);movegui(400,'northwest');
% % % % %     tt=(t_aligned(1:end-1)+t_aligned(2:end))./2;
% % % % %     plot(tt,TwoE,'.-','Color',anc,'MarkerSize',markersiz),hold on
% % % % %     ylabel('Two state entropy');xlabel('Time');
% % % % %     axis square;box on;drawnow;
% % % % %
% % % % %     %Three session based entropy measures------------------------------------
% % % % %     %Number: 8 possible states - aaa,aap, ...,ppa,ppp.
% % % % %     p1=P(1:end-2,:);
% % % % %     p2=P(2:end-1,:);
% % % % %     p3=P(3:end,:);
% % % % %     a1=(1-p1);
% % % % %     a2=(1-p2);
% % % % %     a3=(1-p3);
% % % % %
% % % % %     ThreeP=cell(8,1);
% % % % %     ThreeP{1}=mean(a1.*a2.*a3,2);
% % % % %     ThreeP{2}=mean(a1.*a2.*p3,2);
% % % % %     ThreeP{3}=mean(a1.*p2.*a3,2);
% % % % %     ThreeP{4}=mean(a1.*p2.*p3,2);
% % % % %     ThreeP{5}=mean(p1.*a2.*a3,2);
% % % % %     ThreeP{6}=mean(p1.*a2.*p3,2);
% % % % %     ThreeP{7}=mean(p1.*p2.*a3,2);
% % % % %     ThreeP{8}=mean(p1.*p2.*p3,2);
% % % % %
% % % % %     states=[2,3,4,5,6,7,8];
% % % % %
% % % % %     figure,
% % % % %     for s=2:7
% % % % %         plot(ThreeP{s}),hold on
% % % % %     end
% % % % %     drawnow;
% % % % %
% % % % %
% % % % %     %Calculate normalization if only a subset of states is considered
% % % % %     n=zeros(size(ThreeP{states(1)}));
% % % % %     for s=1:numel(states)
% % % % %         n=n+ThreeP{states(s)};
% % % % %     end
% % % % %
% % % % %     %Normalize data
% % % % %     for s=1:numel(states)
% % % % %         ThreeP{states(s)}=ThreeP{states(s)}./n;
% % % % %     end
% % % % %
% % % % %     %Calculate entropy
% % % % %     ThreeE=0;
% % % % %     for s=1:numel(states)
% % % % %         ThreeE=ThreeE-ThreeP{states(s)}.*log(ThreeP{states(s)});
% % % % %     end
% % % % %
% % % % %     %Plot
% % % % %     figure(401);movegui(401,'north');
% % % % %     tt=t_aligned(2:end-1);
% % % % %     plot(tt,ThreeE,'.-','Color',anc,'MarkerSize',markersiz),hold on
% % % % %     ylabel('Three state entropy');xlabel('Time');
% % % % %     axis square;box on;drawnow;
% % % % %
% % % % %     %Four session based entropy measures-----------------------------------
% % % % %     %Number: 16 possible states - aaaa,aaap, ..., pppa,pppp.
% % % % %     p1=P(1:end-3,:);
% % % % %     p2=P(2:end-2,:);
% % % % %     p3=P(3:end-1,:);
% % % % %     p4=P(4:end,:);
% % % % %     a1=(1-p1);
% % % % %     a2=(1-p2);
% % % % %     a3=(1-p3);
% % % % %     a4=(1-p4);
% % % % %
% % % % %     FourP{1}=mean(a1.*a2.*a3.*a4,2);
% % % % %     FourP{2}=mean(a1.*a2.*a3.*p4,2);
% % % % %     FourP{3}=mean(a1.*a2.*p3.*a4,2);
% % % % %     FourP{4}=mean(a1.*a2.*p3.*p4,2);
% % % % %     FourP{5}=mean(a1.*p2.*a3.*a4,2);
% % % % %     FourP{6}=mean(a1.*p2.*a3.*p4,2);
% % % % %     FourP{7}=mean(a1.*p2.*p3.*a4,2);
% % % % %     FourP{8}=mean(a1.*p2.*p3.*p4,2);
% % % % %     FourP{9}=mean(p1.*a2.*a3.*a4,2);
% % % % %     FourP{10}=mean(p1.*a2.*a3.*p4,2);
% % % % %     FourP{11}=mean(p1.*a2.*p3.*a4,2);
% % % % %     FourP{12}=mean(p1.*a2.*p3.*p4,2);
% % % % %     FourP{13}=mean(p1.*p2.*a3.*a4,2);
% % % % %     FourP{14}=mean(p1.*p2.*a3.*p4,2);
% % % % %     FourP{15}=mean(p1.*p2.*p3.*a4,2);
% % % % %     FourP{16}=mean(p1.*p2.*p3.*p4,2);
% % % % %
% % % % %     states=[2:16];
% % % % %     %Calculate normalization if only a subset of states is considered
% % % % %     n=zeros(size(FourP{states(1)}));
% % % % %     for s=1:numel(states)
% % % % %         n=n+FourP{states(s)};
% % % % %     end
% % % % %
% % % % %     %Normalize data
% % % % %     for s=1:numel(states)
% % % % %         FourP{states(s)}=FourP{states(s)}./n;
% % % % %     end
% % % % %
% % % % %     %Calculate entropy
% % % % %     FourE=0;
% % % % %     for s=1:numel(states)
% % % % %         FourE=FourE-FourP{states(s)}.*log(FourP{states(s)});
% % % % %     end
% % % % %
% % % % %     %Plot
% % % % %     figure(402);movegui(402,'northeast');
% % % % %     tt=(t_aligned(2:end-2)+t_aligned(3:end-1))./2;
% % % % %     plot(tt,FourE,'.-','Color',anc,'MarkerSize',markersiz),hold on
% % % % %     ylabel('Four state entropy');xlabel('Time');
% % % % %     axis square;box on;drawnow;
% % % % %
% % % % %     %----------------------------------------------------------------------
% % % % %     %----------------------------------------------------------------------
% % % % %     %Weight based measures:
% % % % %     %One session
% % % % %     %Boutons are distributed in differet weight bins (states)
% % % % %
% % % % %     %Two session
% % % % %     %The 'state' is defined as a transition from one weight bin to another.
% % % % %     %This is akin to calculating the entropy based on the transition matrix
% % % % %     binedges=[-inf,[2:6:20],inf];
% % % % %     TE=zeros(size(W,1)-1,1);
% % % % %     for j=1:(size(W,1)-1)
% % % % %         T=fT(W(j,:),W(j+1,:),binedges,D.anid);
% % % % %         Tkept=T(2:end,:);%subset of states
% % % % %         %Tkept=T(2:end);
% % % % %         Tkept=Tkept./sum(Tkept(:));%normalization
% % % % %         TE(j)=sum(-Tkept(:).*log(Tkept(:)));
% % % % %     end
% % % % %
% % % % %     %Plot
% % % % %     figure(403);movegui(403,'south');
% % % % %     tt=(t_aligned(1:end-1)+t_aligned(2:end))./2;
% % % % %     plot(tt,TE,'.-','Color',anc,'MarkerSize',markersiz),hold on
% % % % %     ylabel('Transition matrix entropy');xlabel('Time');
% % % % %     axis square;box on;drawnow;
% % % % % elseif strcmp('Hamming',analysis_type)
% % % % %
% % % % %     W=Dat.w;
% % % % %     P=fP(W);
% % % % %     siglevel=0.95;
% % % % %     reftimes=(poi(3)+2):size(W,1);
% % % % %     refckt=all(P(reftimes,:)>siglevel,1);
% % % % %     sum(refckt)
% % % % %     %H=mean(W(:,refckt),2)-mean(mean(W(reftimes,refckt),1));
% % % % %     H=mean(P(:,refckt),2);
% % % % %
% % % % %     %{
% % % % %     Hmat=nan(size(P,1),size(P,1));
% % % % %     for ref=1:size(P,1)
% % % % %         for j=1:size(P,1)
% % % % %             Hmat(ref,j)=sum((P(ref,:).*(1-P(j,:)))>siglevel,2) + sum(((1-P(ref,:)).*P(j,:))>siglevel,2);
% % % % %             Hmat(ref,j)=Hmat(ref,j)./sum((P(ref,:).*P(j,:))>siglevel,2);
% % % % %         end
% % % % %     end
% % % % %     Hmat2=nan(size(P,1),2*size(P,1)-1);
% % % % %     for i=1:size(P,1)
% % % % %        Hmat2(i,size(P,1)-i+1:(2*size(P,1)-i))=Hmat(i,:);
% % % % %     end
% % % % %
% % % % %     Hmat2(:,size(P,1):end)=flipud(Hmat2(:,size(P,1):end));
% % % % %     figure(603+anind),movegui(603+anind,anpos)
% % % % %     h=imagesc(Hmat2);colorbar;axis equal;box on;axis tight
% % % % %     h.AlphaData=~isnan(Hmat2)+0.2;
% % % % %     title(animalid);drawnow;hold on
% % % % %
% % % % %     [~,ind]=min(abs(Dat.L.im_t-Dat.L.t_stimaudio));
% % % % %     plot([0,size(Hmat2,2)+1],[ind ind],'-','Color',[0.8 0.8 0],'LineWidth',2)
% % % % %     [~,ind]=min(abs(Dat.L.im_t-Dat.L.t_stimonly));
% % % % %     plot([0,size(Hmat2,2)+1],[ind ind],'-','Color',[0 0.8 0],'LineWidth',2)
% % % % %     if ~ismember(animalid,[88,102])
% % % % %         [~,ind]=min(abs(Dat.L.im_t-Dat.L.t_inflection));
% % % % %         plot([0,size(Hmat2,2)+1],[ind ind],'-','Color',[0.4 0.4 0.4],'LineWidth',2)
% % % % %
% % % % %         [~,ind]=min(abs(Dat.L.im_t-Dat.L.t_startlearn));
% % % % %         plot([0,size(Hmat2,2)+1],[ind ind],'--','Color',[0.4 0.4 0.4],'LineWidth',2)
% % % % %
% % % % %         [~,ind]=min(abs(Dat.L.im_t-Dat.L.t_endlearn));
% % % % %         plot([0,size(Hmat2,2)+1],[ind ind],'--','Color',[0.4 0.4 0.4],'LineWidth',2)
% % % % %     end
% % % % %     set(h.Parent,'XTick',[],'YTick',[]);
% % % % %     %}
% % % % % elseif strcmp('all2all',analysis_type)

% % % % % elseif strcmp('NewMeasure',analysis_type)
% % % % %
% % % % %     W=Dat.w(poi(4)-5:poi(4)+3,:);
% % % % %     P=fP(W);
% % % % %     added=nan(size(P,1),1);
% % % % %     elim=nan(size(P,1),1);
% % % % %
% % % % %     ind=(1-P(1,:)).*P(end,:)>0.9;
% % % % %     sum(ind)
% % % % %     ind2=P(1,:).*(1-P(end,:))>0.9;
% % % % %     sum(ind2)
% % % % %     for i=2:(size(P,1)-1)
% % % % %         added(i)=sum(prod(1-P(1:i-1,ind),1).*prod(P(i:end,ind),1))./sum((1-P(1,ind)).*P(end,ind));
% % % % %         elim(i)=sum(prod(P(1:i-1,ind2),1).*prod(1-P(i:end,ind2),1))./sum((P(1,ind2)).*(1-P(end,ind2)));
% % % % %     end
% % % % %     figure,
% % % % %     plot(alignedtime(poi(4)-5:poi(4)+3),added,'.-','Color',anc),hold on
% % % % %     plot(alignedtime(poi(4)-5:poi(4)+3),elim,'x--','Color',anc)
% % % % %
% % % % % end
% % % % % end
%--------------------------------------------------------------------------

function T=fT(wi,wf,binedges,anid)
%This function returns the transition matrix

f=fP_handles;
T=nan(numel(binedges)-1);
for frm=1:numel(binedges)-1
    for to=1:numel(binedges)-1
        P_initstate=f.P(wi,binedges(frm))-f.P(wi,binedges(frm+1));
        P_finstate=f.P(wf,binedges(to))-f.P(wf,binedges(to+1));
        T(frm,to)=nansum(P_initstate(:).*P_finstate(:));
    end
end

lbl=cell(1,numel(binedges));
for i=1:numel(binedges)
    lbl{i}=num2str(binedges(i));
end
T=bsxfun(@rdivide,T,sum(T,2));

%
%Plot the transition matrix
figure,imagesc(T)
axis square, box on
h=gca;
set(h,'XTick',0.5:1:numel(binedges)-0.5,'YTick',0.5:1:numel(binedges)-0.5,'XTickLabel',lbl,'YTickLabel',lbl)
h.XLabel.String='To';
h.YLabel.String='From';
h.Title.String=['Animal ',num2str(anid)];
h.CLim=[0 0.6];
colorbar;
drawnow;
%}
end

%--------------------------------------------------------------------------
function [rot]=analysis_bouton_rotation(ax_id,w,rx,ry,rz,nx,ny,nz,anid)
%This function estimates rotation of the z-axis in the image stacks.
%Optimal linear transform is calculated for boutons registered in a pair of
%images. The transformation is used to find rotation measure for each bouton.

sec=unique(round(ax_id/1000));
rot.diff=nan(size(w));
rot.net=nan(size(w));
%rot.net2=nan(size(w));
Ithr=2;
for s=1:numel(sec)
    sec_id=(round(ax_id/1000)==sec(s));
    for t2=2:(size(w,1))
        %Relative to previous session
        t1=t2-1;
        keep=w(t1,:)>Ithr & w(t2,:)>Ithr;
        keep=keep & sec_id;
        
        r_t1=[rx(t1,keep)',ry(t1,keep)',rz(t1,keep)'];
        r_t2=[rx(t2,keep)',ry(t2,keep)',rz(t2,keep)'];
        [~,T,~]=optimal_linear_transform(r_t2,r_t1);
        
        old_nvec=[nx(t2,sec_id)',ny(t2,sec_id)',nz(t2,sec_id)'];
        new_nvec=(T*old_nvec')';
        new_nvec=(bsxfun(@times,new_nvec,1./(sum(new_nvec.^2,2)).^0.5));
        
        %Only the z-component matters. Rotation in xy is ignored.
        rot.diff(t2,sec_id)=abs(acosd(new_nvec(:,3))-acosd(old_nvec(:,3)));
        
        %r_t2reg=bsxfun(@plus,T*r_t2',L)';
        
        %Relative to first session-----------------------------------------
        t1=1;
        keep=w(t1,:)>Ithr & w(t2,:)>Ithr;
        keep=keep & sec_id;
        
        r_t1=[rx(t1,keep)',ry(t1,keep)',rz(t1,keep)'];
        r_t2=[rx(t2,keep)',ry(t2,keep)',rz(t2,keep)'];
        [~,T,~]=optimal_linear_transform(r_t2,r_t1);
        
        old_nvec=[nx(t2,sec_id)',ny(t2,sec_id)',nz(t2,sec_id)'];
        new_nvec=(T*old_nvec')';
        new_nvec=bsxfun(@times,new_nvec,1./(sum(new_nvec.^2,2)).^0.5);
        
        rot.net(t2,sec_id)=abs(acosd(new_nvec(:,3))-acosd(old_nvec(:,3)));
        %rot.net2(t2,sec_id)=abs(acosd(nz(t2,sec_id))-acosd(nz(t1,sec_id)));
        
        %r_t2reg=bsxfun(@plus,T*r_t2',L)';
        %{
        figure,plot3(r_t1(:,1),r_t1(:,2),r_t1(:,3),'ob');hold on
        plot3(r_t2(:,1),r_t2(:,2),r_t2(:,3),'xr');
        plot3(r_t2reg(:,1),r_t2reg(:,2),r_t2reg(:,3),'xg');
        axis equal;axis square;box on;drawnow
        %rmsd=sum((r_t2reg-r_t1).^2,1).^0.5;
        %}
    end
    
    %{
    figure,
    sec_id=find(sec_id);
    keep=nanmax(rot.net(:,sec_id),[],1)>0;
    scatter3(rx(1,sec_id)',ry(2,sec_id)',rz(3,sec_id)',5*nanmax(rot.net(:,sec_id),[],1)','filled'),hold on
    quiver3(rx(1,sec_id(keep))',ry(2,sec_id(keep))',rz(3,sec_id(keep))',nx(1,sec_id(keep))',ny(2,sec_id(keep))',nz(3,sec_id(keep))',0.5),hold on
    xlim([0 1024]);ylim([0 1024]);zlim([0 312]);
    view([90,0]);axis equal;drawnow;
    %}
end
end

%--------------------------------------------------------------------------
function analysis_maindataset_MakePretty(h)
h=h(isgraphics(h));
for i=1:numel(h)
    if strcmp(h(i).Type,'figure')
        h(i).Position=[h(i).Position(1) h(i).Position(2) 668 636];
        hax=findobj(h(i).Children,'flat','Type','Axes');
        if ~isempty(hax)
            axis(hax,'square');
            hax.Box='on';
            hax.YLim(1)=0;
            ylimval=0;
            hl=findobj(hax.Children,'Type','Line');
            for ii=1:numel(hl)
                ylimval=max([ylimval,max(hl(ii).YData(:))]);
            end
            ylimval(ylimval==0)=0.1;
            hax.YLim(2)=ylimval*1.1;
            grid(hax,'on');
        end
    end
end
end

%--------------------------------------------------------------------------
function X=shufflerowswithincols(X)
%shuffle each column independently, without replacement
nrows=size(X,1);
for c=1:size(X,2)
    X(:,c)=X(randperm(nrows),c);
end
end