function analysis_explorelines(src,~)
%This function is a callback for line plots where there are multiple lines,
%and the user wishes to see a single line highlighted. The function
hax=src.Parent;
set(findobj(hax.Children,'flat','Type','Line'),'LineWidth',0.5);
src.LineWidth=4;
disp(src.UserData);
end