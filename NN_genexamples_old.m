%This piece is for groups of boutons e.g. triplets or doublets 
%Generate examples from distinct axons
rng('shuffle');
dthr=20;
%{
%For learners
Ld_mat=abs(bsxfun(@minus,Ld,Ld'));
Lax_mat=bsxfun(@eq,Lax,Lax');
Ld_mat(~Lax_mat)=inf;
%Impose distance threshold and whether only consecutive boutons are allowed
Ld_mat(Ld_mat>dthr)=inf;%Distance threshold
Ld_mat=diag(diag(Ld_mat)) + diag(diag(Ld_mat,1),1) + diag(diag(Ld_mat,-1),-1);
Ld_mat(Ld_mat==0)=inf;
[Li,Lj]=find(Ld_mat<inf);

%For nonlearners
NLd_mat=abs(bsxfun(@minus,NLd,NLd'));
NLax_mat=bsxfun(@eq,NLax,NLax');
NLd_mat(~NLax_mat)=inf;
%Impose distance threshold and whether only consecutive boutons are allowed
NLd_mat(NLd_mat>dthr)=inf;%Distance threshold
NLd_mat=diag(diag(NLd_mat)) + diag(diag(NLd_mat,1),1) + diag(diag(NLd_mat,-1),-1);
NLd_mat(NLd_mat==0)=inf;
[NLi,NLj]=find(NLd_mat<inf);
%}
nbtns=2; %number of consecutive boutons to consider
nsets=100;
k=nbtns*nsets;
n_examples=10000;
IM=zeros(10,k,1,n_examples);
lbl=nan(n_examples,1);

Lind=[];
NLind=[];
temp=(1:size(L,2))';
for i=1:nbtns
    Lind=[Lind,temp+i-1];
    NLind=[NLind,temp+i-1];
end
Lind(any(Lind>size(L,2),2),:)=[];
NLind(any(NLind>size(NL,2),2),:)=[];

Ltempax=reshape(Lax(Lind(:)),size(Lind));
NLtempax=reshape(NLax(NLind(:)),size(NLind));

%Remove if not from same axon
Lrem=false(size(Ltempax,1),1);
NLrem=false(size(NLtempax,1),1);
for i=2:nbtns
    Lrem=Lrem | Ltempax(:,i-1)~=Ltempax(:,i);
    NLrem=NLrem | NLtempax(:,i-1)~=NLtempax(:,i);
end
Lind(Lrem,:)=[];
NLind(NLrem,:)=[];

%Remove if far
Lrem=sum(abs(diff(Ld(Lind),1,2)),2)>dthr;
NLrem=sum(abs(diff(NLd(NLind),1,2)),2)>dthr;
Lind(Lrem,:)=[];
NLind(NLrem,:)=[];

for i=1:n_examples
    if mod(i,2)==0
        L_allowed=Lind(randperm(size(Lind,1),nsets),:);
        flipthis=randperm(size(L_allowed,1),round(size(L_allowed,1)/2));%Randomly flip half of the bouton sets
        L_allowed(flipthis(:),:)=flip(L_allowed(flipthis(:),:),2);
        L_allowed=L_allowed';
        
        IM(:,:,1,i)=L(:,L_allowed(:));
        lbl(i)=1;
    else
        NL_allowed=NLind(randperm(size(NLind,1),nsets),:);
        flipthis=randperm(size(NL_allowed,1),round(size(NL_allowed,1)/2));
        NL_allowed(flipthis(:),:)=flip(NL_allowed(flipthis(:),:),2);
        NL_allowed=NL_allowed';
        
        IM(:,:,1,i)=NL(:,NL_allowed(:));
        lbl(i)=0;
    end
end
lbl=categorical(lbl);

n_train=round(n_examples/2);
IMtrain=IM(:,:,1,1:n_train);
lbltrain=lbl(1:n_train);
IMtest=IM(:,:,1,n_train+1:end);
lbltest=lbl(n_train+1:end);
clear IM;lbl;