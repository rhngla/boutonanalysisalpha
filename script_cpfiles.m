%This code copies axon .swc files from one folder to another

%sourcedir='C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\ManualTraces\';
%destdir='C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\ManualTraces_v2\';

sourcedir='C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\Controls\Conditions\Profiles_reference\';
destdir='C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\Controls\Conditions\Profiles_test4\';

animal={'DL001'};
timepoint={'A','B','D','E','F','H','I'};%cellstr((char(double('B'):double('Q')))');
section={'002'};
axon={[9,10,11,12,13,15]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';

for an=1:numel(animal)
    for se=1:numel(section)
        for ti=1:numel(timepoint)
            stackid=[animal{an},timepoint{ti},section{se}];
            for ax=1:numel(axon{se})
                AM=[]; r=[]; R=[];
                for tr=1:numel(tracer)
                    axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
                    
                    sourcefname=isunixispc([sourcedir,stackid,'/',axonstr,'.mat']);
                    destfname=isunixispc([destdir,stackid,'/',axonstr,'.mat']);
                    if exist(sourcefname,'file')
                        if ~exist(isunixispc([destdir,stackid]),'dir')
                            mkdir(isunixispc(destdir),stackid)
                            display(['Creating new directory',isunixispc([destdir,stackid])])
                        end
                        copyfile(sourcefname,destfname)
                        display(['Saved ',isunixispc([destdir,stackid,'/',axonstr])]);
                    else
                        display([sourcefname,' not found!'])
                    end
                end
            end
        end
    end
end