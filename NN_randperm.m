load('/Users/Fruity/Dropbox/Lab/Plasticity/BoutonAnalysis/dat/NN_Dat.mat','O*');
L_ind=nan(3,20,50);
for i=1:20
    %anid,iter,indices
    for ani=1:3
        [L,~,~,NL] = NN_eqhist(O83,O85,O101,O102,O88,ani);
        L_ind(ani,i,:)=randperm(size(L,2),50);
    end
end
save('/Users/Fruity/Dropbox/Lab/Plasticity/BoutonAnalysis/dat/LOO_perms.mat','L_ind');