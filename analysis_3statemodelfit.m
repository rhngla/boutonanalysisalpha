function [] = analysis_3statemodelfit(anid,gridrangeind)
%This function loads data created and saved by theanalysis_3StateModelcalc code. Fitting parameters are now
%p_l<-m, p_m<-l, p_0<-l.
%P(i,j) = P(i<-j)
%N(i,j) = N(i<-j)

pathlist;
%Load data from file-------------------------------------------------------


for a=1:numel(anid)
    M=load([path_3state,'M',num2str(anid(a))]);
    avgflag=M.avgflag;
    allowedtimes=M.allowedtimes;
    fitind=M.fitind;
    
    Ncreated_data=M.Ncreated_data(:);
    Nrecentlycreated_data=M.Nrecentlycreated_data(:);
    Nrecentlyelim_data=M.Nrecentlyelim_data(:);
    Nelim_data=M.Nelim_data(:);
    
    Ecreated_data=M.Ecreated_data(:);
    Erecentlycreated_data=M.Erecentlycreated_data(:);
    Erecentlyelim_data=M.Erecentlyelim_data(:);
    Eelim_data=M.Eelim_data(:);
    
    n00=M.n00;
    n01=M.n01;
    n10=M.n10;
    n11=M.n11;
    
    %Set parameter ranges------------------------------------------------------
    %Values that are set within loop are set to 1 here
    
    %{
    %----With p01,p10 and p12 on the grid-------
    p01range=0.5:0.01:0.9;
    p10range=0.1:0.01:0.9;
    p12range=0.005:0.01:0.5;
    p21range=1;
    frange=1;
    flmrange=1;
    %}
    %-------------------------------------------
    
    
    %----With p10,p21,flm on the grid-----------
    p01range=1;
    p10range=0.005:0.005:0.5;
    p12range=1;
    p21range=0.001:0.005:0.5;
    frange=1;
    flmrange=0.2:0.005:0.7;
    %-------------------------------------------
    %}
    
    %temp1=temp2 if homeostasis is imposed, since n10=n01 (imposed)
    temp1=n10/(n11+n01);
    temp2=n01/(n11+n01);
    
    %{
    P =
    [(1-p10)        p01         0
       p10       (1-p01-p21)   p12
        0           p21      (1-p12)]
    %}
    
    Z=eye(3);Z(1,1)=0;
    np01range=numel(p01range);np10range=numel(p10range);np12range=numel(p12range);np21range=numel(p21range);nfrange=numel(frange);nflmrange=numel(flmrange);
    siz=[np01range,np10range,np12range,np21range,nfrange,nflmrange];
    cumprodsiz = [1 cumprod(siz(1:end-1))];
    nparams=ones(numel(siz),1);
    errorsq=inf;
    bestind=1;
    
    tic();
    paramgridsiz=numel(p01range)*numel(p10range)*numel(p12range)*numel(p21range)*numel(frange)*numel(flmrange);
    if isempty(gridrangeind)
        gridrangeind=[1,paramgridsiz];
    end
    posongrid=gridrangeind(1);
    chisqgrid=nan(paramgridsiz,1);
    errind=1;
    while posongrid<=(gridrangeind(2))
        ndx=posongrid;
        for iii = numel(nparams):-1:1,
            vi = rem(ndx-1, cumprodsiz(iii)) + 1;
            vj = (ndx - vi)/cumprodsiz(iii) + 1;
            nparams(iii) = double(vj);
            ndx = vi;
        end
        %f=frange(nparams(5));
        
        %{
        %----With p01,p10 and p12 on the grid:------
        p01=p01range(nparams(1));
        p10=p10range(nparams(2));
        p12=p12range(nparams(3));

        %From constraints
        f=1./((1./p10).*temp1+1);
        flm=(1./p01).*temp2;
        p21=p12.*(p01.*(1./temp2)-1);
        %-------------------------------------------
        %}
        
        
        %----With p10,p21,flm on the grid-------
        p10=p10range(nparams(2));
        p21=p21range(nparams(4));
        flm=flmrange(nparams(6));
        
        %From constraints
        f=1./((1./p10).*temp1+1);
        p01=(1./flm).*temp2;
        p12=p21./(p01.*(1./temp2)-1);
        %-------------------------------------------
        %}
        
        P=[1-p10,p01,0;...
            p10,1-p01-p21,p12;...
            0,p21,1-p12];
        
        if p01+p21<=1 && flm<=1 && sum(P(:)>1)==0 && sum(P(:)<0)==0
            %if sum(eig(P)<0)==0
            %N1=Learning,N2=Memory
            N1=flm;N2=1-flm;
            Pn=eye(3);
            PZ=(P*Z);
            PZn=eye(3);
            Ncreated_exact=nan(numel(M.allowedtimes),1);
            Nelim_exact=nan(numel(M.allowedtimes),1);
            Nrecentlyelim_exact=nan(numel(M.allowedtimes),1);
            
            Nrecentlycreated_exact=((1-f)/f)*((1-P(1,1))/P(1,1))*(P(1,1).^(0:(numel(M.allowedtimes)-1))');
            for n=0:(numel(M.allowedtimes)-1)
                Ncreated_exact(n+1)=((1-f)/f)*(Pn(2,1)+Pn(3,1));
                Nelim_exact(n+1)=Pn(1,2)*N1+Pn(1,3)*N2;
                Nrecentlyelim_exact(n+1)=PZn(1,2)*N1+PZn(1,3)*N2;
                Pn=Pn*P;
                PZn=PZn*(PZ);
            end
            
            chisqgrid(errind)=sum(((Ncreated_data(fitind)-Ncreated_exact(fitind)).^2)./Ecreated_data(fitind).^2)+...
                sum(((Nrecentlycreated_data(fitind)-Nrecentlycreated_exact(fitind)).^2)./Erecentlycreated_data(fitind).^2);
            %sum(((Nelim_data(fitind)-Nelim_exact(fitind)).^2)./Eelim_data(fitind).^2)+...
            %sum(((Nrecentlyelim_data(fitind)-Nrecentlyelim_exact(fitind)).^2)./Erecentlyelim_data(fitind).^2);
            
            %Change degrees of independence if this is changed.
            if errorsq>chisqgrid(errind)
                errorsq=chisqgrid(errind);
                bestind=posongrid;
                bestP=P;
                bestflm=flm;
                bestf=f;
            end
            %end
        end
        posongrid=posongrid+1;
        errind=errind+1;
    end
    
    toc()
    savedir=(['Fits',filesep]);
    if ~exist([path_3state,savedir],'dir')
        mkdir(path_3state,savedir)
    end
    save([path_3state,savedir,num2str(anid(a)),'-',num2str(round(gridrangeind(2)*100/paramgridsiz))],...
        'bestP','bestf','bestflm','errorsq','chisqgrid','gridrangeind','bestind',...
        'p01range','p10range','p12range','p21range','flmrange','frange','M')
end
end