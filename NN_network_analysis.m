%Tests to check what features are learnt for different learners vs.
%combined non-learners

anid=1;
if ismac
    path_exp='/Users/Fruity/Dropbox/Lab/Plasticity/dat/NN/L-NL-k50-AvgPool-1feature-MB1-200ep/';
else
    path_exp='E:\Rohan\Dropbox\Lab\Plasticity\dat\NN\L-NL-k50-AvgPool-1feature-MB1-200ep\';
end

%Load files
fnames=dir([path_exp,num2str(anid),'L-NL','*.mat']);
fnames={fnames.name}';
accuracy=nan(numel(fnames),1);
clear N
for f=1:numel(fnames)
    N{f}=load([path_exp,fnames{f}]);
end

%Generate testing examples
[L,Lax,Lind,NL] = NN_eqhist(O83,O85,O101,O102,O88,anid);
[IM,lbl,layers,options]=NN_genex(L,NL);
for f=1:numel(fnames)
    predictedLabels = classify(N{f}.net,IM);
    accuracy(f)=sum(predictedLabels == lbl)/numel(lbl);    
end

%Find eventual loss estimate
figure,
finalloss=nan(numel(fnames),1);
finalacc=nan(numel(fnames),1);
for f=1:numel(fnames)
    if numel(N{f}.traininfo.TrainingLoss)>1
        temp=movmean(N{f}.traininfo.TrainingLoss,[10000,0]);
        temp_plot=temp(1:1000:end);
        plot(temp_plot,'-','Color',[rand(1) rand(1) rand(1) 0.5]);hold on
        finalloss(f)=temp_plot(end);
        finalacc(f)=mean(N{f}.traininfo.TrainingAccuracy(end-1000:end));
    end
end
[~,sortind]=sort(finalloss);
axis square;box on;drawnow


W=[];
figure,
temp=sort(finalloss);
temp=temp(4);
for f=1:numel(fnames)
    %if accuracy(f)==max()
    if finalloss(f)<=temp
        w1=N{f}.net.Layers(2).Weights(:,1,1,1);
        W=[W,w1(:)];
        plot(w1);hold on
    end
end

W=median(W,2);