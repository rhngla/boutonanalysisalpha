function [btnind,btndist] = analysis_nldistortion(D)
%Axon in all possible time pairs (t1,t2) are aligned using matched peaks
%present in (t1,t2).
%Euclidian distance between boutons in each (t1,t2) are calculated
%after alignment.
%btndist: Maximum distance for a bouton across any (t1,t2) is given as output
%btnind: Index of the bouton in the original matrix

ispeak=~isnan(D.G.LoGxy.amp);
axid=unique(D.G.LoGxy.ax_id);
valfull=[];indsfid=[];valfids=[];indfull=[];
for t1=1:(size(D.G.LoGxy.Inorm,1)-1)
    for t2=t1+1
        peaksinboth=(ispeak(t1,:) & ispeak(t2,:));
        %peaks_t1only=(ispeak(t1,:) & ~ispeak(t2,:));
        for ax=1:numel(axid)
            %Ss=round(axid(ax)./1000);
            %Sax=find(axid(round(axid/1000)==Ss)==axid(ax));
            %Ss=find(unique(round(axid./1000))==Ss);
            thisaxon=(D.G.LoGxy.ax_id==axid(ax));
            fidpts=find(peaksinboth & thisaxon);
            %tomatch=find(peaks_t1only & thisaxon);
            tomatch=find(thisaxon);
            
            %Find the best linear transformation based on peaks present in
            %both times
            T_t1=[D.G.LoGxy.rx(t1,fidpts);D.G.LoGxy.ry(t1,fidpts)]';%Target
            S_t2=[D.G.LoGxy.rx(t2,fidpts);D.G.LoGxy.ry(t2,fidpts)]';%Source
            [S_t2_trans,L,b]=optimal_linear_transform(S_t2,T_t1);
            S_t2_trans=S_t2_trans';
            temp=sum(([T_t1-S_t2_trans]).^2,2).^0.5;
            valfids=[valfids;temp(:)];
            indsfid=[indsfid;fidpts(:)];
            
            %Apply the transformation to all the peaks and find the
            %distribution of distances between peaks for a pair of times
            rxy_tomatch_t1=[D.G.LoGxy.rx(t1,tomatch);D.G.LoGxy.ry(t1,tomatch)]';
            rxy_tomatch_t2=[D.G.LoGxy.rx(t2,tomatch);D.G.LoGxy.ry(t2,tomatch)]';
            rxy_tomatch_trans_t2=bsxfun(@plus,rxy_tomatch_t2*L',b');
            temp=sum((rxy_tomatch_t1-rxy_tomatch_trans_t2).^2,2).^0.5;
            valfull=[valfull;temp(:)];
            indfull=[indfull;tomatch(:)];
            
            %{
            %Tests
            figure(2);clf(2);
            plot(T_t1(:,2),T_t1(:,1),'*b');hold on
            plot(S_t2(:,2),S_t2(:,1),'*r');hold on
            plot(S_t2_trans(:,2),S_t2_trans(:,1),'*g');hold on
            
            plot(rxy_t1(:,2),rxy_t1(:,1),'-b');hold on
            plot(rxy_t1(ig_t1,2),rxy_t1(ig_t1,1),'x','Color',[0.5 0.5 0.5]);hold on
            plot(rxy_t2(:,2),rxy_t2(:,1),'-r');hold on
            plot(rxy_t2_trans(:,2),rxy_t2_trans(:,1),'-g');hold on
            axis equal;drawnow;box on;axis ij
            plot(rxy_t2(T(i,2),2),rxy_t2(T(i,2),1),'*k');hold on
            plot(rxy_t2(T(i,3),2),rxy_t2(T(i,3),1),'ok');hold on
            %}
        end
    end
end

%{
valfull=sort(valfull);
valfull(round(.95*numel(valfull)))
figure,histogram(valfids,[0:.25:10],'Normalization','PDF');hold on,
histogram(valfull,[0:.25:10],'Normalization','PDF','DisplayStyle','stairs');hold on
axis square;
%}

btnind=nan(numel(unique(indfull)),1);
btndist=nan(numel(unique(indfull)),1);

inds=unique(indfull);
for i=1:numel(inds)
    btnind(i)=inds(i);
    btndist(i)=max(valfull(indfull==inds(i)));
end
end