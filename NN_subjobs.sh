declare -a animal=(1 2 3)
declare -a shuffleid=($(seq 1 1 1))
declare -a iter=($(seq 1 1 1))
#declare -a shuffleid=($(seq 1 1 20))
#printf "%s\n" "${shuffleid[@]}"
fname="NN_batchjob.bash"
for anid in "${animal[@]}"
do
	for sid in "${shuffleid[@]}"
	do
		for ii in "${iter[@]}"
		do
      echo '#!/bin/bash'>$fname
      echo "#SBATCH --job-name="$anid-$sid-$iter >>$fname
      echo "#SBATCH --output=out">>$fname
      echo "#SBATCH --error=err">>$fname
      echo "#SBATCH --exclusive">>$fname
      echo "#SBATCH --partition=ser-par-10g-4">>$fname
      echo "#SBATCH -N 1">>$fname
      echo "/home/gala.r/MATLAB/R2017b/bin/matlab -logfile ./matlabout.txt -nosplash -nodesktop -nodisplay -r \"NN_script($sid,$anid)\"">>$fname
      echo "work=/gss_gpfs_scratch/gala.r/BoutonAnalysis/code/" >> $fname
      echo "cd \$work">> $fname
      sbatch $fname
      echo "Job: " $anid-$sid-$iter " submitted"; 			
  	done
  done
done