function [] = NN_createdatastore(Boutons,Label)
%This function chooses multiple random subset of boutons and creates a
%datastore object. These data will be used to train the neural network.
%'Boutons' is the full set of bouton weights in a matrix of dim (t x n).

%Parameters:
k=100;
n=size(Boutons,2);
if ismac
destdir=['/Users/Fruity/Desktop/NNDatastore/',num2str(Label),'/'];
else
destdir=['F:\Rohan\NNDatastore\',num2str(Label),'\'];
end

for i=1:10000
    fname=[destdir,num2str(i),'.mat'];
    ind=randperm(n,k);
    IM=Boutons(:,ind);
    save(fname,'IM');
end
end