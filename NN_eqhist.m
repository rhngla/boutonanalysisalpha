function [L,Lax,Lind,NL] = NN_eqhist(O83,O85,O101,O102,O88,animalind)
%animalind=1 refers to 83
%animalind=2 refers to 85
%animalind=3 refers to 101

%{
p = @(n,m,k) ...
    1 - (((n-k)*(n-m)./n./(n-m-k)).^0.5).*...
    (...
    ((1-(m./n)).^(n-m)).*...
    ((1-(k./n)).^(n-k))./...
    ((1-(m+k)./n).^(n-(m+k)))...
    );

n=500;  % dataset from which k samples are drawn
m=1:n;   % number of outliers
k=50;   % number of samples per example

figure,
s=p(n,m,k);
inrange=s>0 & s<1;
plot(m(inrange),s(inrange),'.','MarkerSize',10)
title(sprintf('n = %d ,k = %d',n,k))
ylabel('Fraction of examples with atleast one outlier')
xlabel('# of outliers')
ylim([0 1])
grid on
axis square
drawnow;
%}

M=cell(4,1);
Max=cell(4,1);

M{1}=O83.w;
M{2}=O85.w;
M{3}=O101.w;
M{4}=O102.w;
M{5}=O88.w;

Max{1}=O83.ax_id+83*10^4;
Max{2}=O85.ax_id+85*10^4;
Max{3}=O101.ax_id+101*10^4;
Max{4}=O102.ax_id+102*10^4;
Max{5}=O88.ax_id+88*10^4;

Mind{1}=O83.ind(end,:);
Mind{2}=O85.ind(end,:);
Mind{3}=O101.ind(end,:);
Mind{4}=O102.ind(end,:);
Mind{5}=O88.ind(end,:);

for i=1:numel(M)
    keep=any(M{i}>2);
    Max{i}=Max{i}(:,keep);
    M{i}=M{i}(:,keep);
    Mind{i}=Mind{i}(:,keep);
    
    %Count number of boutons on unique axons
    %sort(histcounts(categorical(Max{i}))');
    
    %Keep equal number (250) randomly boutons from each animal
    %ind=sort(randperm(size(M{i},2),250));
    
    ind=1:size(M{i},2);
    M{i}=M{i}(:,ind);
    Max{i}=Max{i}(:,ind);
    Mind{i}=Mind{i}(:,ind);
end

%{
%Histogram equalization 1st way:
dist=[M{1},M{2},M{3},M{4},M{5}];
dist=dist(:);
dist=sort(dist);
eqM=M;
for i=1:numel(M)
    temp=M{i};
    for t=1:size(M{i},1)
        [~,ind]=sort(M{i}(t,:));
        vals=sort(dist(randperm(numel(dist),numel(ind))));
        eqM{i}(t,ind)=vals;
    end
    figure(100),plot(eqM{i}(:),M{i}(:),'.');hold on
end
%}

%Histogram equalization
distrib=[M{1},M{2},M{3},M{4},M{5}];
distrib=distrib(:);
distrib=sort(distrib);
distrib(distrib<0)=0;
eqM=M;
for i=1:numel(M)
    for t=1:size(M{i},1)
        [~,ind]=sort(M{i}(t,:));
        val=nan(size(M{i},2),1);
        nbtns=numel(distrib)./numel(M{i}(t,:));
        for b=1:size(M{i},2)
            val(b)=mean(distrib(round((b-1)*nbtns:b*nbtns-1)+1));
        end
        eqM{i}(t,ind)=val;
    end
    %figure(100),plot(eqM{i}(:),M{i}(:),'.');hold on
end
L=[];Lax=[];Lind=[];
for i=1:numel(animalind)
    L=[L,eqM{animalind(i)}];
    Lax=[Lax,Max{animalind(i)}];
    Lind=[Lind,Mind{animalind(i)}];
end
NL=[eqM{4},eqM{5}];
end