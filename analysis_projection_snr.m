function snr=analysis_projection_snr(D,A)
% This function calculates a signal to noise measure based on intensity
% projections of axons.
% D is obtained from analysis_getmat.m 
% A is obtained from analysis_loaddat.m. 
% A should contain all axons in D.

pathlist;

%--------------------------------------------------------------------------
%Assumption: atleast one axon from each section in A is present in D.
%--------------------------------------------------------------------------

%Find section number in structure A that corresponds to the axon_id
%assigned in D. 
t=1;
ax_lbl=unique(D.G.LoGxy.ax_id);
ss=unique(round(ax_lbl./1000));
sA=round(ax_lbl./1000);
ax_id=mod(ax_lbl,1000);
axA=nan(size(ax_lbl));
for i=1:numel(ss)
    sA(sA==ss(i))=i;
end

%Finding axon number in structure A that corresponds to the axon_id 
%assigned in D
for a=1:numel(ax_lbl)
    for temp_a=1:numel(A(1).Section{sA(a)}.Time{t}.Axon)
        if isequal(str2double(A(1).Section{sA(a)}.Time{t}.Axon{temp_a}.id(end-2:end)),ax_id(a))
            axA(a)=temp_a;
        end
    end
end

%Calculate signal to noise based on the intensity projection
snr=nan(size(D.G.LoGxy.ind,1),numel(ax_lbl));
for t=1:size(D.G.LoGxy.ind,1)
    for a=1:numel(ax_lbl)
        A(1).Section{sA(a)}.Time{t}.Axon{axA(a)}.id
        temp=strsplit(A(1).Section{sA(a)}.Time{t}.Axon{axA(a)}.id,'-');
        tempIM=load([profile_pth,temp{1},filesep,temp{2},'.mat'],'proj');
        IM=tempIM.proj.G.xy.ax;
        snr(t,a)=mean(IM(IM>0))./std(IM(IM>0));
    end
end

end
