function [ind_tor_num,ind_tor_wt,ind_osc]=analysis_locateboutons(O)

w=O.w;
f=fP_handles;
P=f.P(w);
top_frac=5/100;
nexamples=round(size(w,2)*(top_frac));

%Oscillatig boutons--------------------------------------------------------
S_d2=nan(size(w(2:end-1,:)));
P_bouton=nan(size(w(2:end-1,:),1),1);
for i=2:(size(w,1)-1)
    S_d2(i-1,:)=sum(([-0.5;1;-0.5]*ones(1,size(w,2))).*w(i-1:i+1,:),1).*(P(i,:));
    P_bouton(i-1)=sum(P(i,:));
end

%Show top nexample contributions
sorted=sort(abs(S_d2(:)));
thr=sorted(end-nexamples);
ind_osc=any(S_d2>=thr,1);

%--------------------------------------------------------------------------
%Number turnover contributions---------------------------------------------
P_add=f.P_add(w(1:end-1,:),w(2:end,:));
P_elim=f.P_elim(w(1:end-1,:),w(2:end,:));
meas=max(P_add,P_elim);
%Show top nexample contributions
sorted=sort(abs(meas(:)));
thr=sorted(end-nexamples);
ind_tor_num=any(meas>=thr,1);

%--------------------------------------------------------------------------
%Weight turnover contributions---------------------------------------------
P_pot=f.P_pot(w(1:end-1,:),w(2:end,:));
P_dep=f.P_dep(w(1:end-1,:),w(2:end,:));
meas=max(P_pot,P_dep).*abs(w(2:end,:)-w(1:end-1,:));

%Show top nexample contributions
sorted=sort(abs(meas(:)));
thr=sorted(end-nexamples);
ind_tor_wt=any(meas>=thr,1);
end