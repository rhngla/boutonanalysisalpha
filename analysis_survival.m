function []=analysis_survival(Dat)
%{
%Run this section in command line to generate input variable Dat
Dat.anid, Dat.P and Dat,W are used within this function
Dat=cell(8,1);
Dat{1}=analysis_editmatrix(d83);
Dat{2}=analysis_editmatrix(d85);
Dat{3}=analysis_editmatrix(d101);
Dat{4}=analysis_editmatrix(d102);
Dat{5}=analysis_editmatrix(d88);
Dat{6}=analysis_editmatrix(d68);
Dat{7}=analysis_editmatrix(d89);
Dat{8}=analysis_editmatrix(d108);
%}
issavefig=true;
%anind=[1,3,4,5];%Include only 2 learners and non-learners
anind=[1,3,4,5,6,7,8];%Exclude 2 to exclude 85
allan=[83,85,101,102,88,108,68,89];
anc=lines(numel(allan));anc(end,:)=[0.3 0.3 0.3];
NL_P_isbouton=[];
L_P_isbouton=[];
k=1;
anntxt=cell(1);
for i=1:numel(anind)
    Dat{anind(i)}.anid
    cc=anc(anind(i),:);
    X=Dat{anind(i)}.P;
    P_isbouton=nan(size(X,1),size(X,2)*(size(X,1)-1));
    for ti=1:(size(X,1)-1)
        colstart=size(X,2)*(ti-1)+1;
        colend=size(X,2)*(ti-1)+size(X,2);
        rowend=size(X,1)+1-ti;
        P_isbouton(1:rowend,colstart:colend)=X(ti:end,:);
    end
    
    if Dat{anind(i)}.anid==102 || Dat{anind(i)}.anid==88
        NL_P_isbouton=catnanpad2(NL_P_isbouton,P_isbouton,2);
    else
        L_P_isbouton=catnanpad2(L_P_isbouton,P_isbouton,2);
    end
    
    [tt,sfrac,serr]=calc_dat(P_isbouton);
    
    if Dat{anind(i)}.anid==68
        startpt=[0.8,4,100];
    else
        startpt=[0.2,4,80];
    end
    
    s=fittype('a*exp(-x/t1)+(1-a)*exp(-x/t2)');
    [fitsfrac,goodness]=fit(tt,sfrac,s,'StartPoint',startpt,'Weights',1./(serr+eps));
    
    %Generate string to add to plot
    %Fvars=coeffnames(fitsfrac);
    Fvars={'a','\tau_{fast}','\tau_{slow}'};
    Fvals=coeffvalues(fitsfrac);
    Fints=confint(fitsfrac);
    %fprintf('\n');
    %disp(['Fitting survival curve independently for',num2str(Dat{i}.anid)])
    %Dat{i}.anid
    k=k+1;anntxt{k}=sprintf(['Animal ',num2str(Dat{anind(i)}.anid),' ::: Adj. R sq: ',num2str(goodness.adjrsquare)]);
    for j=1:numel(Fvals)
        k=k+1;anntxt{k}=sprintf('%5s %5.2f (%5.2f,%5.2f)',Fvars{j},Fvals(j),Fints(1,j),Fints(2,j)); 
    end
    k=k+1;anntxt{k}='';
    
    hf=figure(300);
    errorbar(tt,sfrac,serr,'.','Color',cc,'LineStyle','none');hold on
    plot(tt,fitsfrac(tt),'-','Color',cc);
    xlim([0 70]);ylim([-0.05 1.05]);axis square
    ylabel('Survival Fraction');
    xlabel('Time in days');
    grid on
    set(findobj(hf,'Type','ErrorBar'),'LineWidth',2);
    set(findobj(hf,'Type','Line'),'LineWidth',2);
end
annotation('textbox','String',anntxt,'FontSize',13,'Position',[0.5943 0.1405 0.2996 0.7538]...
    ,'HorizontalAlignment','right','LineStyle','none');
hf=gcf;hf.Position=[211 84 909 864];
if issavefig
pathlist;
print(gcf,'-dpng',[res_pth,'Survival fraction-All animals.png']);
savefig(gcf,[res_pth,'Survival fraction-All animals.fig']);
end

%Combined data for learners and non-learners-------------------------------
[L_tt,L_sfrac,L_serr]=calc_dat(L_P_isbouton);
[L_fitsfrac,~]=fit(L_tt,L_sfrac,s,'StartPoint',[0.2,4,80],'Weights',1./(L_serr+eps));
display(L_fitsfrac);

[NL_tt,NL_sfrac,NL_serr]=calc_dat(NL_P_isbouton);
[NL_fitsfrac,~]=fit(NL_tt,NL_sfrac,s,'StartPoint',[0.2,4,80],'Weights',1./(NL_serr+eps));
display(NL_fitsfrac);

%Plot data for separate fits on learners and non-learners------------------
hf=figure(500);

errorbar(L_tt,L_sfrac,L_serr,'.','Color',[0.8 0 0],'LineStyle','none');hold on
plot(L_tt,L_fitsfrac(L_tt),'-','Color',[0.8 0 0]);
k=0;
Fvars={'a','\tau_{fast}','\tau_{slow}'};
Fvals=coeffvalues(L_fitsfrac);
Fints=confint(L_fitsfrac);
k=k+1;anntxt1{k}=sprintf(['Learners ::: ',' Adj. R sq: ',num2str(goodness.adjrsquare)]);
for j=1:numel(Fvals)
    k=k+1;anntxt1{k}=sprintf('%5s %5.2f (%5.2f,%5.2f)',Fvars{j},Fvals(j),Fints(1,j),Fints(2,j));
end
k=k+1;anntxt1{k}='';

errorbar(NL_tt,NL_sfrac,NL_serr,'.','Color',[0 0 0],'LineStyle','none');hold on
plot(NL_tt,NL_fitsfrac(NL_tt),'-','Color',[0 0 0]);
Fvars={'a','\tau_{fast}','\tau_{slow}'};
Fvals=coeffvalues(NL_fitsfrac);
Fints=confint(NL_fitsfrac);
k=k+1;anntxt1{k}=sprintf(['Non-learners ::: ',' Adj. R sq: ',num2str(goodness.adjrsquare)]);
for j=1:numel(Fvals)
    k=k+1;anntxt1{k}=sprintf('%5s %5.2f (%5.2f,%5.2f)',Fvars{j},Fvals(j),Fints(1,j),Fints(2,j));
end
k=k+1;anntxt1{k}='';
annotation('textbox','String',anntxt1,'FontSize',13,'Position',[0.5943 0.1405 0.2996 0.7538]...
    ,'HorizontalAlignment','right','LineStyle','none');

xlim([0 70]);ylim([-0.05 1.05]);axis square
ylabel('Survival Fraction');
xlabel('Time in days');
grid on;drawnow;hf=gcf;
hf.Position=[211 84 909 864];
set(findobj(hf,'Type','ErrorBar'),'LineWidth',2);
set(findobj(hf,'Type','Line'),'LineWidth',2);
if issavefig
    pathlist;
    print(gcf,'-dpng',[res_pth,'Survival fraction-learners and nonlearners-different a t1 t2.png']);
    savefig(gcf,[res_pth,'Survival fraction-learners and nonlearners-different a t1 t2.fig']);
end

%Plot data for fits on learners and non-learners with reduced paramters----
%--------------------------------------------------------------------------

%Exludes points not used for fitting
y_fit=[L_sfrac;NL_sfrac];
e_fit=[L_serr;NL_serr];
t_orig_fit=[L_tt;NL_tt];
t_temp_fit=[L_tt;NL_tt+100];
id_fit=[1*ones(size(L_sfrac));2*ones(size(NL_sfrac))];

%For plotting only
y_plot=[L_sfrac;NL_sfrac];
e_plot=[L_serr;NL_serr];
t_orig_plot=[L_tt;NL_tt];
t_temp_plot=[L_tt;NL_tt+100];
id_plot=[1*ones(size(L_sfrac));2*ones(size(NL_sfrac))];

%--------------------------------------------------------------------------
%Simultaneous fitting for all animals with different a and same t1,t2------
%--------------------------------------------------------------------------
F1=@(a1,ts,tl,x) (a1*exp(-x/ts)+(1-a1)*exp(-x/tl));
F2=@(a2,ts,tl,x) (a2*exp(-(x-100)/ts)+(1-a2)*exp(-(x-100)/tl));

F=@(a1,a2,ts,tl,x) F1(a1,ts,tl,x).*(x<100 & x>=0)+...
    F2(a2,ts,tl,x).*(x<200 & x>=100);

[fitsfrac,goodness,~]=fit(t_temp_fit,y_fit,F,'StartPoint',[0.2,0.2,5,100],'Weights',1./e_fit);
%Fvars=coeffnames(fitsfrac);
Fvars={'a_{learner}','a_{non-learner}','\tau_{fast}','\tau_{slow}'};
Fvals=coeffvalues(fitsfrac);
Fints=confint(fitsfrac);
anntxt=cell(1);
k=1;
anntxt{k}=['Adj. R sq: ',num2str(goodness.adjrsquare)];
for j=1:numel(Fvals)
    k=k+1;anntxt{k}=sprintf('%5s %5.2f (%5.2f,%5.2f)',Fvars{j},Fvals(j),Fints(1,j),Fints(2,j));
end

figure(5023),
cc=[[0.8 0 0];[0 0 0]];
for i=[1,2]
    errorbar(t_orig_plot(id_plot==i),y_plot(id_plot==i),e_plot(id_plot==i),'.','Color',cc(i,:),'MarkerEdgeColor','none','Marker','none'),hold on
    plot(t_orig_plot(id_plot==i),y_plot(id_plot==i),'s','Color',cc(i,:),'MarkerSize',5,'MarkerFaceColor',cc(i,:)),hold on
    plot(t_orig_plot(id_plot==i),fitsfrac(t_temp_plot(id_plot==i)),'-','Color',cc(i,:)),hold on
end
annotation('textbox','String',anntxt,'FontSize',13,'Position',[0.5943 0.1405 0.2996 0.7538]...
    ,'HorizontalAlignment','right','LineStyle','none');
hf=gcf;hf.Position=[211 84 909 864];

xlim([0 70]);ylim([-0.05 1.05]);axis square
ylabel('Survival Fraction');
xlabel('Time in days');
grid on
set(findobj(hf,'Type','ErrorBar'),'LineWidth',2);
set(findobj(hf,'Type','Line'),'LineWidth',2);
title('Fits have same \tau_{fast} and \tau_{slow}')
if issavefig
    pathlist;
    print(gcf,'-dpng',[res_pth,'Survival fraction-learners and nonlearners-same t1 t2.png']);
    savefig(gcf,[res_pth,'Survival fraction-learners and nonlearners-same t1 t2.fig']);
end

%--------------------------------------------------------------------------
%Simultaneous fitting for all animals with different a, t1,different t2----
%--------------------------------------------------------------------------

F1=@(a1,t1,tl,x) (a1*exp(-x/t1)+(1-a1)*exp(-x/tl));
F2=@(a2,t2,tl,x) (a2*exp(-(x-100)/t2)+(1-a2)*exp(-(x-100)/tl));

F=@(a1,a2,t1,t2,tl,x) F1(a1,t1,tl,x).*(x<100 & x>=0)+...
    F2(a2,t2,tl,x).*(x<200 & x>=100);

[fitsfrac,goodness,~]=fit(t_temp_fit,y_fit,F,'StartPoint',[0.2,0.2,5,5,100],'Weights',1./e_fit);
%Fvars=coeffnames(fitsfrac);
Fvars={'a_{learner}','a_{non-learner}','\tau_{fast, learner}','\tau_{fast, nonlearner}','\tau_{slow}'};
Fvals=coeffvalues(fitsfrac);
Fints=confint(fitsfrac);
anntxt=cell(1);
k=1;
anntxt{k}=['Adj. R sq: ',num2str(goodness.adjrsquare)];
for j=1:numel(Fvals)
    k=k+1;anntxt{k}=sprintf('%5s %5.2f (%5.2f,%5.2f)',Fvars{j},Fvals(j),Fints(1,j),Fints(2,j));
end

figure(5024),
cc=[[0.8 0 0];[0 0 0]];
for i=[1,2]
    errorbar(t_orig_plot(id_plot==i),y_plot(id_plot==i),e_plot(id_plot==i),'.','Color',cc(i,:),'MarkerEdgeColor','none','Marker','none'),hold on
    plot(t_orig_plot(id_plot==i),y_plot(id_plot==i),'s','Color',cc(i,:),'MarkerSize',5,'MarkerFaceColor',cc(i,:)),hold on
    plot(t_orig_plot(id_plot==i),fitsfrac(t_temp_plot(id_plot==i)),'-','Color',cc(i,:)),hold on
end
annotation('textbox','String',anntxt,'FontSize',13,'Position',[0.5943 0.1405 0.2996 0.7538]...
    ,'HorizontalAlignment','right','LineStyle','none');
hf=gcf;hf.Position=[211 84 909 864];

xlim([0 70]);ylim([-0.05 1.05]);axis square
ylabel('Survival Fraction');
xlabel('Time in days');
grid on
set(findobj(hf,'Type','ErrorBar'),'LineWidth',2);
set(findobj(hf,'Type','Line'),'LineWidth',2);
title('Fits have same \tau_{slow}')
if issavefig
    pathlist;
    print(gcf,'-dpng',[res_pth,'Survival fraction-learners and nonlearners-same t2 only.png']);
    savefig(gcf,[res_pth,'Survival fraction-learners and nonlearners-same t2 only.fig']);
end
%--------------------------------------------------------------------------


end

function [X,Y,E]=calc_dat(P_isbouton)
P_survived=cumprod(P_isbouton,1);
initnum=nan(size(P_survived,1),1);
finnum=nan(size(P_survived,1),1);
varnum=nan(size(P_survived,1),1);
for ti=1:size(P_survived,1)
    measured_in_fin=~isnan(P_isbouton(ti,:));
    initnum(ti)=sum(P_isbouton(1,measured_in_fin));
    finnum(ti)=sum(P_survived(ti,measured_in_fin));
    varnum(ti)=sum(P_survived(ti,measured_in_fin).*(1-P_survived(ti,measured_in_fin)));
end

Y=finnum./initnum;
E=varnum.^0.5./initnum;
X=(0:(numel(Y)-1))'*4;
end