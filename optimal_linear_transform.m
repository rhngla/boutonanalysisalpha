function [X_affine,L,b]=optimal_linear_transform(X,Y)
% This function finds the optimal linear (affine) transformation (L,b) for 
% assignment Y=L*X+b. X and Y are Nx3

X=X';Y=Y';
X_cm=X-mean(X,2)*ones(1,size(X,2));
Y_cm=Y-mean(Y,2)*ones(1,size(Y,2));
Cov_XX=X_cm*X_cm';
Cov_YX=Y_cm*X_cm';
L=Cov_YX/(Cov_XX+1*diag(10^-8.*ones(1,size(X,1))));
b=mean(Y,2)-mean(L*X,2);

X_affine=L*X+b*ones(1,size(X,2));

%{
%Checks
figure(10)
subplot(1,2,1)
plot3(X(1,:),X(2,:),X(3,:),'r*')
axis equal, hold on, box on
plot3(Y(1,:),Y(2,:),Y(3,:),'k*')
subplot(1,2,2)
plot3(X_affine(1,:),X_affine(2,:),X_affine(3,:),'r*')
axis equal, hold on, box on
plot3(Y(1,:),Y(2,:),Y(3,:),'k*')
%}