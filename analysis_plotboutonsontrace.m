function analysis_plotboutonsontrace(D,A,ind,t)

ind=ind(:)';
if ~islogical(ind)
   temp=false(size(D.ax_id));
   temp(ind)=true;
   ind=temp;clear temp;
end

ax_lbl=unique(D.ax_id);
ss=unique(round(ax_lbl./1000));
sA=round(ax_lbl./1000);
ax_id=mod(ax_lbl,1000);
axA=nan(size(ax_lbl));

for i=1:numel(ss)
    sA(sA==ss(i))=i;
end

%Finding the correct indices for axons in A that correspond to boutons in D
for a=1:numel(ax_lbl)
    for temp_a=1:numel(A(1).Section{sA(a)}.Time{t}.Axon)
        if isequal(str2double(A(1).Section{sA(a)}.Time{t}.Axon{temp_a}.id(end-2:end)),ax_id(a))
            axA(a)=temp_a;
        end
    end
end

cols=lines(numel(ax_lbl));
for a=1:numel(ax_lbl)
    thisax=D.ax_id==ax_lbl(a);
    btn_blen=D.blen(t,thisax);
    btn_ind=D.ind(t,thisax);
    btn_special=D.ind(t,(thisax & ind));
    
    ax_d=A(1).Section{sA(a)}.Time{t}.Axon{axA(a)}.fit.G.LoGxy.d.man;
    ax_ig=true(size(ax_d));
    btn_d=ax_d(btn_ind);
    btn_d_left=btn_d(:)-btn_blen(:)./2;
    btn_d_right=btn_d(:)+btn_blen(:)./2;
    
    for b=1:numel(btn_d)
        ax_ig(ax_d>=btn_d_left(b) & ax_d<=btn_d_right(b))=false;
    end
    
    r=A(1).Section{sA(a)}.Time{t}.Axon{axA(a)}.r.optim;
    %figure(sA(a));
    figure(1);
    %ax_col=[0.3 0.3 0.3]+0.7.*rand(1,3);
    ax_col=cols(a,:);
    plot3(r(:,2),r(:,1),r(:,3),'-','Color',[0.4 0.4 0.4]),hold on
    plot3(r(ax_ig,2),r(ax_ig,1),r(ax_ig,3),'.','Color',[0.8 0.8 0.8],'Markersize',1),hold on
    
    plot3(r(btn_ind,2),r(btn_ind,1),r(btn_ind,3)+1,'o','Markersize',7,'MarkerFaceColor',[0.8 0.8 0.8],'MarkerEdgeColor',[0.5 0.5 0.5],'LineStyle','none'),hold on
    %plot3(r(btn_special,2),r(btn_special,1),r(btn_special,3),'o','Color',ax_col,'LineWidth',2,'MarkerSize',10),hold on
    plot3(r(btn_special,2),r(btn_special,1),312*ones(size(r(btn_special,3))),'o','MarkerFaceColor',[0.8 0 0],'LineStyle','none','MarkerSize',7,'MarkerEdgeColor',[0.8 0 0]),hold on
    xlim([0 1024]);ylim([0 1024]);
    axis square;box on;view([0 90])
    drawnow
end
end