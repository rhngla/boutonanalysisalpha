%Custom figure plots were added to
%/Applications/MATLAB_R2017b.app/toolbox/nnet/cnn/+nnet/+internal/+cnn/trainer.m
%original copy of the file was saved as .../trainer_orig.m

%{
%Generate training examples:
k=100; %number of boutons per example
nbtns=1;
n_examples=10000;
IM=zeros(10,k,1,n_examples);
lbl=nan(n_examples,1);
for i=1:n_examples
    if mod(i,2)==0
        ind=randperm(size(L,2),k);
        IM(:,:,1,i)=L(:,ind);
        lbl(i)=1;
    else
        ind=randperm(size(NL,2),k);
        IM(:,:,1,i)=NL(:,ind);
        lbl(i)=0;
    end
end
lbl=categorical(lbl);
%}

%{
    n_train=round(n_examples/2);
    IMtrain=IM(:,:,1,1:n_train);
    lbltrain=lbl(1:n_train);
    IMtest=IM(:,:,1,n_train+1:end);
    lbltest=lbl(n_train+1:end);
    clear IM;lbl;
}

%Define the network:
%This network was used to get robust features when single boutons are
chosen randomly from the datasets
mylayers = [
    imageInputLayer([10 k 1])
    convolution2dLayer([10,1],2,'Stride',1);
    reluLayer
    maxPooling2dLayer([1 k],'Stride',1)
    fullyConnectedLayer(2)
    softmaxLayer()
    classificationLayer];
%}

%maxPooling2dLayer([1 nsets],'Stride',1)
%averagePooling2dLayer([1 nsets],'Stride',1)
%Network to look at consecutive boutons
mylayers = [
    imageInputLayer([10 k 1])
    convolution2dLayer([10,nbtns],1,'Stride',[1,nbtns]);
    reluLayer
    fullyConnectedLayer(2)
    softmaxLayer()
    classificationLayer];
%}

%Train the network:
%'Plots','training-progress',...
options = trainingOptions('sgdm',...
    'Momentum',0,...
    'MaxEpochs',60, ...
    'Verbose',true,...
    'LearnRateSchedule','piecewise',...
    'LearnRateDropFactor',1,...
    'ExecutionEnvironment','auto',...
    'Plots','none',...
    'MiniBatchSize',10);

net = trainNetwork(IMtrain,lbltrain,mylayers,options);

%Generate examples for validation
predictedLabels = classify(net,IMtest);
accuracy = sum(predictedLabels == lbltest)/numel(lbltest);
