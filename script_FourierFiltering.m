I_axon=A.I.G.Gauss2.raw;
d_axon=A.d.optim;
min_bouton_size=2;
max_bouton_size=10;
n_stds=3;

%--------------------------------------------------------------------------
cutoff_freqmin=2*pi./(2*(2*min_bouton_size));
cutoff_freqmax=2*pi./(2*(2*max_bouton_size));
V=I_axon;
X=d_axon;
dL=0.02;
Fs=1./dL;%sampling frequency
Xq=min(X):dL:max(X);
if mod(numel(Xq),2)==1
    Xq=Xq(1:end-1);
end
Vq = interp1(X,V,Xq,'linear');
L=numel(Xq);

F=fft(Vq);
f = Fs*(0:(L/2))/L;
fsym=[f(1:end-1),f(end-1:-1:1)];
F(fsym >cutoff_freqmin)=0;
F(fsym <cutoff_freqmax)=0;
Vfilt=real(ifft(F));
noise_level=n_stds.*std(Vfilt-Vq);
figure,plot(Xq,Vq,'-b'),hold on,plot(Xq,Vfilt,'-r'),hold on,plot(Xq,Vfilt-Vq,'-k')