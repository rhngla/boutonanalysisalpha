pth=pwd;
[projectdir,currdir]=fileparts(pth);
datdir=isunixispc([projectdir,filesep,'dat',filesep]);

im_pth=isunixispc([datdir,'Images/']);
man_pth=isunixispc([datdir,'Traces/']);
profile_pth=isunixispc([datdir,'Profiles/']);
excel_pth=isunixispc([datdir,'ExcelData/']);
err_pth=isunixispc([datdir,'errorlogs/']);